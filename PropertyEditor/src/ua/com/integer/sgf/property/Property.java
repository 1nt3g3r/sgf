package ua.com.integer.sgf.property;

import java.lang.reflect.Method;

import javax.swing.JOptionPane;

public class Property {
	private PropertyType type;
	private Object value = "";
	private String name;
	private Object object;
	
	public Property(String name, PropertyType type) {
		this.type = type;
		this.name = name;
		
		switch(type) {
		case FLOAT: setFloat(0.0f);
			break;
		case INTEGER: setInt(1);
			break;
		case STRING: setString("");
			break;
		default:
			break;
		
		}
	}
	
	public void setObject(Object object) {
		this.object = object;
	}
	
	public void applyProperty() {
		Method setMethod = null;
		Class<? extends Object> objectClass = object.getClass();
		try {
			switch(type) {
			case FLOAT: setMethod = objectClass.getMethod("set"+getName(), float.class);
			break;
			case INTEGER: setMethod = objectClass.getMethod("set"+getName(), int.class);
				break;
			case STRING: setMethod = objectClass.getMethod("set"+getName(), String.class);
				break;
			default:
				break;
			}
			setMethod.invoke(object, getValue());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Can't set " + getName() + " property!");
		}
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
	
	public PropertyType getType() {
		return type;
	}
	
	public Object getValue() {
		return value;
	}
	
	public String getName() {
		return name;
	}
	
	public int asInt() {
		return Integer.parseInt(value+"");
	}
	
	public float asFloat() {
		return Float.parseFloat(value+"");
	}
	
	public String asString() {
		return value+"";
	}
	
	public void setInt(int value) {
		this.value = new Integer(value);
	}
	
	public void setFloat(float value) {
		this.value = new Float(value);
	}
	
	public void setString(String value) {
		this.value = value;
	}
}
