package ua.com.integer.sgf.property;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.badlogic.gdx.scenes.scene2d.Actor;

public class PropertyEditor extends JPanel {
	private static final long serialVersionUID = -994383060592403206L;
	private JTable table;

	public PropertyEditor() {
		this(null);
	}
	/**
	 * Create the panel.
	 */
	public PropertyEditor(Object object) {
		setLayout(new GridLayout(0, 1, 0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		if (object != null) {
			table.setModel(new PropertyTableModel(PropertyMaker.getPropertiesForObject(object)));
		}
	}
	
	public void setObject(Object object) {
		table.setModel(new PropertyTableModel(PropertyMaker.getPropertiesForObject(object)));
	}
	
	public static void main(String[] args) {
		JFrame frm = new JFrame();
		frm.setSize(400, 400);
		frm.getContentPane().add(new PropertyEditor(new Actor()));
		frm.setVisible(true);
	}

}
