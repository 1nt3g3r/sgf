package ua.com.integer.sgf.property;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class PropertyMaker {
	public static List<Property> getPropertiesForObject(Object object) {
		List<Property> props = new ArrayList<Property>();
		
		Class<? extends Object> objectClass = object.getClass();
		for(Method m : objectClass.getMethods()) {
			if (m.getName().startsWith("set") && m.getParameterTypes().length == 1) {
				String paramName = m.getParameterTypes()[0].getName();
				String propertyName = m.getName().substring(3, m.getName().length());
				Property p = null;
				if (paramName.equals("float")) {
					p = new Property(propertyName, PropertyType.FLOAT);
				} else if (paramName.equals("java.lang.String")) {
					p = new Property(propertyName, PropertyType.STRING);
				} else if (paramName.equals("int")) {
					p = new Property(propertyName, PropertyType.INTEGER);
				}
				if (p != null) {
					p.setObject(object);
					props.add(p);
				}
			}
		}
		return props;
	}
}
