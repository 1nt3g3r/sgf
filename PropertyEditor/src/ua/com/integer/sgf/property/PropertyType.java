package ua.com.integer.sgf.property;

/**
 * Property types which we can edit
 * @author integer
 */
public enum PropertyType {
	FLOAT,
	INTEGER,
	STRING;
}
