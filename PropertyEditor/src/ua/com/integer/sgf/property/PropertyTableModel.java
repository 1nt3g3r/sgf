package ua.com.integer.sgf.property;

import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

public class PropertyTableModel extends AbstractTableModel {
	private static final long serialVersionUID = -4522199011391539898L;
	private List<Property> properties;
	
	public PropertyTableModel(List<Property> properties) {
		this.properties = properties;
	}
	
	@Override
	public String getColumnName(int column) {
		switch(column) {
		case 0 : return "Type";
		case 1 : return "Name";
		case 2 : return "Value";
		}
		return "";
	}
	
	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public int getRowCount() {
		return properties.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		switch(column) {
		case 0 : return properties.get(row).getType();
		case 1 : return properties.get(row).getName();
		case 2 : {
			switch(properties.get(row).getType()) {
			case FLOAT: return properties.get(row).asFloat();
			case INTEGER: return properties.get(row).asInt();
			case STRING: return properties.get(row).asString();
			}
		}
		}
		return "";
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columnIndex == 2;
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		Property p = properties.get(rowIndex);
		try {
			switch(p.getType()) {
			case FLOAT: p.setFloat(Float.parseFloat(aValue+""));
			break;
			case INTEGER: p.setInt(Integer.parseInt(aValue+""));
			break;
			case STRING: p.setString(aValue+"");
			break;
			default:
				break;
			}
			p.applyProperty();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Input correct value!");
		}
	}

}
