package ua.com.integer.sgf.screen.manager;
import ua.com.integer.sgf.actor.simple.SimpleActor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class AbstractScreen implements Screen {
	private Stage stage;
	private static SpriteBatch batch = new SpriteBatch();
	private ScreenConfig config;
	private boolean needUpdate;
	
	public class BackPressListener extends InputListener {
		@Override
		public boolean keyDown(InputEvent event, int keycode) {
			if (keycode == Keys.BACK || keycode == Keys.ESCAPE) {
				onBackOrEscapePressed();
			}
			return super.keyDown(event, keycode);
		}
	}
	
	public AbstractScreen(ScreenConfig config) {
		this.config = config;
		initialize();
	}
	
	public AbstractScreen(String name) {
		config = new ScreenConfig();
		config.screenName = name;
		initialize();
	}
	
	private void initialize() {
		stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false, batch);
		stage.addListener(new BackPressListener());
	}
	
	public ScreenConfig getConfig() {
		return config;
	}
	
	public void onBackOrEscapePressed() {
	}

	public void setBackground(TextureRegion background) {
		config.background = background;
	}

	public String getScreenName() {
		return config.screenName;
	}

	public Stage getStage() {
		return stage;
	}
	
	/**
	 * Add an actor to this screen
	 */
	public void addActor(Actor actor) {
		stage.addActor(actor);
		if (actor instanceof SimpleActor) {
			((SimpleActor) actor).updatePosition();
		}
	}
	
	/**
	 * Add an actor to this screen with Gdx.graphics.getWidth()*percentX, 
	 * 	Gdx.graphics.getHeight()*percentY coords.
	 */
	public void addActor(Actor actor, float percentX, float percentY) {
		float actorX = Gdx.graphics.getWidth() * percentX;
		float actorY = Gdx.graphics.getHeight() * percentY;
		actor.setPosition(actorX, actorY);
		addActor(actor);
	}
	
	/**
	 * Add an actor to center of this screen.
	 */
	public void addActorToCenter(Actor actor) {
		float centerX = (Gdx.graphics.getWidth() - actor.getWidth())/2;
		float centerY = (Gdx.graphics.getHeight() - actor.getHeight())/2;
		actor.setPosition(centerX, centerY);
		addActor(actor);
	}
	
	/**
	 * Finds and returns an actor with selected name. If no actor found null will be returned. 
	 * Method searches in root group.
	 */
	public Actor findByName(String name) {
		return stage.getRoot().findActor(name);
	}

	/**
	 * Draws background image or fills screen background color if it need. After it renders stage. 
	 * You can override it and disable draw background if you want increase performance.
	 * @param delta
	 */
	@Override
	public void render(float delta) {
		if (config.needDrawBackgroundImage) {
			drawBackgroundImage();
		} else {
			clearScreen();
		}
		renderStage(delta);
	}
	
	protected void drawBackgroundImage() {
		batch.begin();
		batch.draw(config.background, 0, 0, stage.getWidth(), stage.getHeight());
		batch.end();
	}
	
	protected void clearScreen() {
		GL10 gl = Gdx.gl10;
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		gl.glClearColor(config.backgroundColor.r, 
				config.backgroundColor.g, 
				config.backgroundColor.b, 
				config.backgroundColor.a);
	}
	
	protected void renderStage(float delta) {
		stage.draw();
		stage.act(delta);
	}

	@Override
	public void resize(int width, int height) {
		stage.setViewport(width, height, false);
	}

	@Override
	public void show() {
		config.active = true;
		Gdx.input.setInputProcessor(stage);
		if (isNeedUpdate()) {
			performUpdate();
		}
		needUpdate = true;
	}
	
	/**
	 * Override this method to update your screen. Update will be 
	 * performed only if needUpdate() returns true. After update 
	 * needUpdate will be reset to "true" value - it means, you should 
	 * set needUpdate(false) if you want to save state of your screen
	 */
	public void performUpdate() {
	}
	
	public void setNeedUpdate(boolean needUpdate) {
		this.needUpdate = needUpdate;
	}
	
	public boolean isNeedUpdate() {
		return needUpdate;
	}
	
	public boolean isActive() {
		return config.active;
	}

	@Override
	public void hide() {
		config.active = false;
	}

	@Override
	public void pause() {
		config.active = false;
	}

	@Override
	public void resume() {
		config.active = true;
	}

	@Override
	public void dispose() {
		stage.dispose();
	}
}
