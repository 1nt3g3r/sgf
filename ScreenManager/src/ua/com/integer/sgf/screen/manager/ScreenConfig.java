package ua.com.integer.sgf.screen.manager;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class ScreenConfig {
	public String screenName;
	public Color backgroundColor = Color.BLACK;
	public boolean needDrawBackgroundImage;
	public TextureRegion background;
	public boolean active;
}
