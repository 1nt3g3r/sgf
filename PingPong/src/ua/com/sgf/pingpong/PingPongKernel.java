package ua.com.sgf.pingpong;

import ua.com.integer.sgf.kernel.GameKernel;
import ua.com.integer.sgf.kernel.LoadingActor;
import ua.com.integer.sgf.kernel.screen.loading.LoadingScreen;
import ua.com.sgf.pingpong.screen.GameScreen;

public class PingPongKernel extends GameKernel {
	private static PingPongKernel instance = new PingPongKernel();
	
	private PingPongKernel() {
		super("game-data");
	}
	
	public static PingPongKernel getInstance() {
		return instance;
	}
	
	@Override
	public void created() {
		res().screens().addScreen(new GameScreen());
		
		loadResources();
	}
	
	private void loadResources() {
		rm().loadAll();
		
		LoadingActor loadActor = new PingPongLoadingActor();
		LoadingScreen loadScreen = new LoadingScreen(rm(), loadActor);
		loadScreen.addActor(loadActor);
		
		res().screens().addScreen(loadScreen);
		res().screens().showScreen("loading-screen");
	}
}
