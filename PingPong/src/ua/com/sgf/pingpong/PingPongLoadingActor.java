package ua.com.sgf.pingpong;

import java.util.Date;

import ua.com.integer.sgf.kernel.LoadingActor;
import ua.com.sgf.pingpong.screen.GameScreen;

public class PingPongLoadingActor extends LoadingActor {
	boolean end = false;
	@Override
	public void loadFinished() {
		if (!end) {
			end = true;
			PingPongKernel.getInstance().res().music().getMusic("test.mp3").play();
		}
	}
}
