package ua.com.sgf.pingpong.screen;

import ua.com.integer.sgf.action.ActionManager;
import ua.com.integer.sgf.screen.manager.AbstractScreen;
import ua.com.integer.sgf.sound.manager.MusicManager;
import ua.com.sgf.pingpong.PingPongKernel;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class GameScreen extends AbstractScreen {

	public GameScreen() {
		super("game-screen");
		getConfig().needDrawBackgroundImage = false;
		
		getStage().addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				MusicManager music = PingPongKernel.getInstance().res().music();
				music.getMusic(music.getLoadedMusic().peek()).play();
			}
		});

		//sm.getSound(sm.getLoadedSounds().peek()).play();
		//PingPongKernel.getInstance().res().sounds().g
		
	//	quad.addListener(new QuadListener());
	//	addActor(quad);
	}
	
	class QuadListener extends InputListener {
		private ActionManager am = PingPongKernel.getInstance().res().actions();
		private Action increase = am.getAction("inc-1-percent");
		private Action decrease = am.getAction("dec-1-percent");
		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			resetActions();
			return true;
		}
		
		@Override
		public void touchUp(InputEvent event, float x, float y, int pointer,
				int button) {
			super.touchUp(event, x, y, pointer, button);
		}
		
		private void resetActions() {
			increase = am.getAction("inc-1-percent");
			decrease = am.getAction("dec-1-percent");
		}
	}
}
