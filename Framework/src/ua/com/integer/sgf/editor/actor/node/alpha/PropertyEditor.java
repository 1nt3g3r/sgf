package ua.com.integer.sgf.editor.actor.node.alpha;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import ua.com.integer.sgf.editor.util.SwingUtils;
import java.awt.Dimension;

public class PropertyEditor extends JDialog {
	private static final long serialVersionUID = 4313365339449026871L;
	private final JPanel contentPanel = new JPanel();
	private JComboBox alphaBox;
	private JComboBox timeBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			PropertyEditor dialog = new PropertyEditor(2, 4);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public PropertyEditor(float alpha, float time) {
		getContentPane().setPreferredSize(new Dimension(300, 150));
		setTitle("Alpha action editor");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "What alpha value is?", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			contentPanel.add(panel);
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			{
				alphaBox = new JComboBox();
				alphaBox.setModel(new DefaultComboBoxModel(new String[] {"0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1"}));
				alphaBox.setEditable(true);
				panel.add(alphaBox);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "What is the time of this alpha?", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
			contentPanel.add(panel);
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			{
				timeBox = new JComboBox();
				timeBox.setModel(new DefaultComboBoxModel(new String[] {"0.1", "0.2", "0.3", "0.5", "0.7", "0.9", "1", "2", "3", "5", "10", "20", "30", "60"}));
				timeBox.setEditable(true);
				panel.add(timeBox);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						okClicked();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		
		alphaBox.setSelectedItem(alpha);
		timeBox.setSelectedItem(time);
		
		pack();
		SwingUtils.showModalInCenter(this);
	}
	
	public void okClicked() {
	}
	
	public float getAlpha() {
		float alpha = 0;
		try {
			alpha = Float.parseFloat(alphaBox.getSelectedItem().toString());
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "You input wrong alpha value! Current alpha is 0");
		}
		return alpha;
	}
	
	public float getTime() {
		float time = 0;
		try {
			time = Float.parseFloat(timeBox.getSelectedItem().toString());
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "You input wrong time value! Current time is 0");
		}
		return time;
	}

	public JComboBox getAlphaBox() {
		return alphaBox;
	}
	public JComboBox getTimeBox() {
		return timeBox;
	}
}
