package ua.com.integer.sgf.editor.actor.node.moveby;

import javax.swing.JDialog;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.action.descriptor.MoveByActionDescriptor;
import ua.com.integer.sgf.dimension.Dimension;
import ua.com.integer.sgf.editor.actor.node.ActionNode;
import ua.com.integer.sgf.editor.util.SwingUtils;

public class MoveByNode extends ActionNode {
	private static final long serialVersionUID = 6625787140348016509L;
	
	private Dimension dx, dy;
	private float time;
	
	public MoveByNode() {
		this(Dimension.getDimension(0), Dimension.getDimension(0), 0);
	}
	
	public MoveByNode(Dimension dx, Dimension dy, float time) {
		this.dx = dx;
		this.dy = dy;
		this.time = time;
	}
	
	@Override
	public String toString() {
		return "move_by: " + "dx="+dx + ", dy=" + dy + ", time=" + time;
	}
	

	@Override
	public ActionDescriptor getDescriptor() {
		return new MoveByActionDescriptor(dx, dy, time);
	}
	
	@Override
	public void editClicked() {
		JDialog dialog = new MoveByEditor(dx, dy, time) {
			private static final long serialVersionUID = 3810944988250876814L;
			@Override
			public void okClicked() {
				dx = getDx().getDimension();
				dy = getDy().getDimension();
				time = Float.parseFloat(getTime().getText());
				dispose();
			}
		};
		SwingUtils.showModalInCenter(dialog);
	}
	
	@Override
	public boolean isLeaf() {
		return true;
	}

	@Override
	public ActionNode copy() {
		return new MoveByNode(dx, dy, time);
	}
}
