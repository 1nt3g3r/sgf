package ua.com.integer.sgf.editor.actor.node.sizeto;

import ua.com.integer.sgf.dimension.Dimension;
import ua.com.integer.sgf.editor.actor.node.sizeby.SizeByPropertyEditor;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class SizeToPropertyEditor extends SizeByPropertyEditor {
	private static final long serialVersionUID = -7637639591739442112L;
	public SizeToPropertyEditor(Dimension oldDx, Dimension oldDy, float oldTime) {
		super(oldDx, oldDy, oldTime);
		getDy().setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "HEIGHT", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		getDx().setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "WIDTH", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		setTitle("SizeToAction editor");
	}
}
