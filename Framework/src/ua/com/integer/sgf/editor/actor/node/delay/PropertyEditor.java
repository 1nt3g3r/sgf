package ua.com.integer.sgf.editor.actor.node.delay;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import ua.com.integer.sgf.editor.util.SwingUtils;

public class PropertyEditor extends JDialog {
	private static final long serialVersionUID = -8072323010136431519L;
	private final JPanel contentPanel = new JPanel();
	private JComboBox comboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			PropertyEditor dialog = new PropertyEditor(3);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public PropertyEditor(float time) {
		setTitle("DelayAction editor");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setPreferredSize(new Dimension(300, 50));
		contentPanel.setBorder(new TitledBorder(null, "How long this delay will be in seconds?", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.X_AXIS));
		{
			comboBox = new JComboBox();
			comboBox.setEditable(true);
			comboBox.setModel(new DefaultComboBoxModel(new String[] {"0.1", "0.2", "0.3", "0.4", "0.5", "0.8", "1", "2", "3", "5", "10", "20", "30", "60"}));
			contentPanel.add(comboBox);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						okClicked();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		
		pack();
		comboBox.setSelectedItem(time);
		SwingUtils.showModalInCenter(this);
	}
	
	public void okClicked() {
		
	}
	
	public float getTime() {
		float time = 0;
		try {
			time = Float.parseFloat(comboBox.getSelectedItem().toString());
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "You input wrong time! Current time is 0");
		}
		return time;
	}
}
