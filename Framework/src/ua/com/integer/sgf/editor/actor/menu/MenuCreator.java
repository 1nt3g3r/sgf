package ua.com.integer.sgf.editor.actor.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import ua.com.integer.sgf.actor.GameActor;
import ua.com.integer.sgf.actor.simple.SimpleActor;
import ua.com.integer.sgf.editor.EditListener;
import ua.com.integer.sgf.editor.EditorFrame;
import ua.com.integer.sgf.editor.EditorGame;
import ua.com.integer.sgf.editor.EditorScreen;
import ua.com.integer.sgf.screen.manager.ScreenConfig;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class MenuCreator {
	private Preferences prefs = Preferences.userNodeForPackage(EditorFrame.class);
	
	private JCheckBoxMenuItem showMenuBackground;
	
	public JMenu getAdditionalMenu() {
		JMenu toReturn = new JMenu("Screen");
			JMenuItem backgroundItem = new JMenuItem("Set background image...");
			backgroundItem.addActionListener(new RootBackgroundMenuListener());
			toReturn.add(backgroundItem);
			
			showMenuBackground = new JCheckBoxMenuItem("Show background image");
			showMenuBackground.addActionListener(new ShowMenuBackgroundListener());
			toReturn.add(showMenuBackground);
		return toReturn;
	}
	
	class RootBackgroundMenuListener implements ActionListener {
		private Texture texture;
		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(new File(prefs.get("root-actor-background", "./")));
			if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				savePref("root-actor-background", fileChooser.getSelectedFile().getAbsolutePath());
				Texture oldTexture = texture;
				
				Texture texture = new Texture(Gdx.files.absolute(fileChooser.getSelectedFile().getAbsolutePath()));
				TextureRegion rootRegion = new TextureRegion(texture);
				getEditorConfig().background = rootRegion;
				getEditorConfig().needDrawBackgroundImage = true;
				showMenuBackground.setSelected(true);
				
				if (oldTexture != null) {
					oldTexture.dispose();
				}
			}
		}
	}
	
	private ScreenConfig getEditorConfig() {
		ScreenConfig config = EditorGame.getInstance().res().screens().getScreen(EditorScreen.class).getConfig();
		return config;
	}
	
	class ShowMenuBackgroundListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (showMenuBackground.isSelected() && getEditorConfig().background != null) {
				getEditorConfig().needDrawBackgroundImage = true;
			} else {
				getEditorConfig().needDrawBackgroundImage = false;
				showMenuBackground.setSelected(false);
			}
		}
		
	};
	
	public JMenu getSimpleActorMenu() {
		JMenu toReturn = new JMenu("Actor");
			JMenuItem insertNewItem = new JMenuItem("Insert actor to active");
			insertNewItem.addActionListener(new InsertNewItemMenuListener());
			toReturn.add(insertNewItem);
			
			JMenuItem newActorItem = new JMenuItem("New");
			newActorItem.addActionListener(new NewSimpleActorMenuListener());
			toReturn.add(newActorItem);
			
			JMenuItem saveAsItem = new JMenuItem("Save to file");
			saveAsItem.addActionListener(new SaveActorMenuListener());
			toReturn.add(saveAsItem);
			
			JMenuItem loadItem  = new JMenuItem("Load from file");
			loadItem.addActionListener(new LoadActorMenuListener());
			toReturn.add(loadItem);
		return toReturn;
	}
	
	class InsertNewItemMenuListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			GameActor activeActor = EditorGame.getInstance().res().screens().getScreen(EditorScreen.class).getActiveActor();
			GameActor childActor = new SimpleActor();
			childActor.addListener(new EditListener(childActor));
			childActor.debug();
			childActor.setSize(activeActor.getWidth()/2, activeActor.getHeight()/2);
			childActor.setPosition(activeActor.getWidth()/4, activeActor.getHeight()/4);
			activeActor.addActor(childActor);
		}
	}
	
	class NewSimpleActorMenuListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (JOptionPane.showConfirmDialog(null, "Current actor will be lost. Do you want to continue?") == JOptionPane.YES_OPTION) {
				EditorGame.getInstance().setActionActor(new SimpleActor());
			}
		}
	}
	
	class LoadActorMenuListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			JFileChooser fileChooser = getActorFileChooser();
			if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				savePref("actor-dir", fileChooser.getSelectedFile()
						.getAbsolutePath());
				
				SimpleActor actor = SimpleActor.loadActorFromFile(fileChooser.getSelectedFile(), true);
				addEditListener(actor);
				EditorGame.getInstance().setActionActor(actor);
			}
		}
	}
	
	private static void addEditListener(GameActor actor) {
		actor.addListener(new EditListener(actor));
		for(Actor child : actor.getChildren()) {
			if (child instanceof GameActor) {
				addEditListener((GameActor) child);
			}
		}
	}
	
	class SaveActorMenuListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			JFileChooser fileChooser = getActorFileChooser();
			if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				savePref("actor-dir", fileChooser.getSelectedFile().getAbsolutePath());
				EditorGame.getInstance().getRootActor().saveToFile(fileChooser.getSelectedFile());
				JOptionPane.showMessageDialog(null, "Actor saved!");
			}
		}
	}
	
	private JFileChooser getActorFileChooser() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileFilter() {
			@Override
			public String getDescription() {
				return "SGF actor description files";
			}
			@Override
			public boolean accept(File file) {
				return file.getName().endsWith(".actor");
			}
		});
		fileChooser.setCurrentDirectory(new File(prefs.get("actor-dir", "./")));
		return fileChooser;
	}
	
	private void savePref(String name, String value) {
		prefs.put(name, value);
		try {
			prefs.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}
}
