package ua.com.integer.sgf.editor.actor.node.scaleto;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.action.descriptor.ScaleToDescriptor;
import ua.com.integer.sgf.editor.actor.node.ActionNode;

public class ScaleToNode extends ActionNode {
	private static final long serialVersionUID = -648665242079286281L;
	private float scaleX, scaleY;
	private float time;
	
	public ScaleToNode(float scaleX, float scaleY, float time) {
		this.scaleX = scaleX;
		this.scaleY = scaleY;
		this.time = time;
	}
	
	public ScaleToNode() {
	}
	
	@Override
	public String toString() {
		return "scale to " + scaleX + ", " + scaleY + ", time=" + time;
	}
	
	@Override
	public ActionDescriptor getDescriptor() {
		return new ScaleToDescriptor(scaleX, scaleY, time);
	}
	
	@Override
	public void editClicked() {
		new ScaleToPropertyEditor(scaleX, scaleY, time) {
			private static final long serialVersionUID = 3029627628556820467L;

			@Override
			public void okClicked() {
				scaleX = getScaleX();
				scaleY = getScaleY();
				time = getTime();
				dispose();
			}
		};
	}
	
	@Override
	public boolean isLeaf() {
		return true;
	}

	@Override
	public ActionNode copy() {
		return new ScaleToNode(scaleX, scaleY, time);
	}

}
