package ua.com.integer.sgf.editor.actor.node.remove;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.action.descriptor.RemoveActorDescriptor;
import ua.com.integer.sgf.editor.actor.node.ActionNode;

public class RemoveNode extends ActionNode {
	private static final long serialVersionUID = 7799672375253806965L;
	
	@Override
	public String toString() {
		return "remove actor";
	}

	@Override
	public ActionDescriptor getDescriptor() {
		return new RemoveActorDescriptor();
	}

	@Override
	public boolean isLeaf() {
		return true;
	}

	@Override
	public ActionNode copy() {
		return new RemoveNode();
	}
}
