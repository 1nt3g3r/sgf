package ua.com.integer.sgf.editor.actor.node.moveto;

import ua.com.integer.sgf.dimension.Dimension;
import ua.com.integer.sgf.editor.actor.node.moveby.MoveByEditor;

import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class MoveToEditor extends MoveByEditor {

	public MoveToEditor(Dimension oldDx, Dimension oldDy, float oldTime) {
		super(oldDx, oldDy, oldTime);
		setTitle("MoveBy action editor");
		getTime().setText("1.0");
		getDy().setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Destination Y", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		getDx().setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Destination X", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1317250922548927727L;

}
