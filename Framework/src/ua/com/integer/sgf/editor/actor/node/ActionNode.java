package ua.com.integer.sgf.editor.actor.node;

import javax.swing.tree.DefaultMutableTreeNode;

import ua.com.integer.sgf.action.ActionType;
import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.editor.actor.node.alpha.AlphaNode;
import ua.com.integer.sgf.editor.actor.node.delay.DelayNode;
import ua.com.integer.sgf.editor.actor.node.moveby.MoveByNode;
import ua.com.integer.sgf.editor.actor.node.moveto.MoveToNode;
import ua.com.integer.sgf.editor.actor.node.parallel.ParallelNode;
import ua.com.integer.sgf.editor.actor.node.remove.RemoveNode;
import ua.com.integer.sgf.editor.actor.node.repeat.RepeatNode;
import ua.com.integer.sgf.editor.actor.node.rotateby.RotateByNode;
import ua.com.integer.sgf.editor.actor.node.rotateto.RotateToNode;
import ua.com.integer.sgf.editor.actor.node.scaleby.ScaleByNode;
import ua.com.integer.sgf.editor.actor.node.scaleto.ScaleToNode;
import ua.com.integer.sgf.editor.actor.node.sequence.SequenceNode;
import ua.com.integer.sgf.editor.actor.node.sizeby.SizeByNode;
import ua.com.integer.sgf.editor.actor.node.sizeto.SizeToNode;

/**
 * Tree node for present some action. Node has 
 * some associated ActionDescritor object. Also you 
 * should implement editClicked() method if ActionDescriptor 
 * class which you use has some params (as example, time, width, etc.).
 * 
 * @author 1nt3g3r
 */
public abstract class ActionNode extends DefaultMutableTreeNode {
	private static final long serialVersionUID = 7115786578558702632L;
	
	public abstract ActionDescriptor getDescriptor();
	public void editClicked(){};
	
	public abstract ActionNode copy();
	
	public static ActionNode getByDescriptor(ActionDescriptor d) {
		switch(d.type) {
		case ALPHA: return new AlphaNode(d.fParam(0), d.fParam(1));
		case DELAY: return new DelayNode(d.fParam(0));
		case MOVE_BY: return new MoveByNode(d.dParam(0), d.dParam(1), d.fParam(2));
		case MOVE_TO: return new MoveToNode(d.dParam(0), d.dParam(1), d.fParam(2));
		case PARALLEL: {
			ParallelNode node = new ParallelNode();
			for(ActionDescriptor descriptor : d.children) {
				node.add(ActionNode.getByDescriptor(descriptor));
			}
			return node;
		}
		case REMOVE_ACTOR: return new RemoveNode();
		case REPEAT: {
			RepeatNode toReturn = new RepeatNode(d.iParam(0));
			if (d.children.size > 0) {
				toReturn.add(ActionNode.getByDescriptor(d.children.get(0)));
			}
			return toReturn;
		}
		case ROTATE_BY: return new RotateByNode(d.fParam(0), d.fParam(1));
		case ROTATE_TO: return new RotateToNode(d.fParam(0), d.fParam(1));
		case SCALE_BY: return new ScaleByNode(d.fParam(0), d.fParam(1), d.fParam(2));
		case SCALE_TO: return new ScaleToNode(d.fParam(0), d.fParam(1), d.fParam(2));
		case SEQUENCE: {
			SequenceNode toReturn = new SequenceNode();
			for(ActionDescriptor toAdd : d.children) {
				toReturn.add(ActionNode.getByDescriptor(toAdd));
			}
			return toReturn;
		}
		case SIZE_BY: return new SizeByNode(d.dParam(0), d.dParam(1), d.fParam(2));
		case SIZE_TO: return new SizeToNode(d.dParam(0), d.dParam(1), d.fParam(2));
		}
		throw new IllegalArgumentException("Can't create node for " + d + " descriptor!");
	}
	
	public static ActionNode getByActionType(ActionType type) {
		switch(type) {
		case ALPHA: return new AlphaNode();
		case DELAY: return new DelayNode();
		case MOVE_BY: return new MoveByNode();
		case MOVE_TO: return new MoveToNode();
		case PARALLEL: return new ParallelNode();
		case REMOVE_ACTOR: return new RemoveNode();
		case REPEAT: return new RepeatNode();
		case ROTATE_BY: return new RotateByNode();
		case ROTATE_TO: return new RotateToNode();
		case SCALE_BY: return new ScaleByNode();
		case SCALE_TO: return new ScaleToNode();
		case SEQUENCE: return new SequenceNode();
		case SIZE_BY: return new SizeByNode();
		case SIZE_TO: return new SizeToNode();
		default: throw new IllegalArgumentException("Can't create node for " + type + " ActionType!");
		}
	}
}
