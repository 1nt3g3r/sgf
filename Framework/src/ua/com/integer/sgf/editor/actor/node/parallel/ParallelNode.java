package ua.com.integer.sgf.editor.actor.node.parallel;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.action.descriptor.ParallelActionDescriptor;
import ua.com.integer.sgf.editor.actor.node.ActionNode;

public class ParallelNode extends ActionNode {
	private static final long serialVersionUID = -814554708422661798L;

	@Override
	public String toString() {
		return "parallel";
	}
	
	@Override
	public boolean isLeaf() {
		return false;
	}
	
	@Override
	public ActionDescriptor getDescriptor() {
		ParallelActionDescriptor descriptor = new ParallelActionDescriptor();
		for(int i = 0; i < getChildCount(); i++) {
			descriptor.addChild(((ActionNode) getChildAt(i)).getDescriptor());
		}
		return descriptor;
	}

	@Override
	public ActionNode copy() {
		return new ParallelNode();
	}
}
