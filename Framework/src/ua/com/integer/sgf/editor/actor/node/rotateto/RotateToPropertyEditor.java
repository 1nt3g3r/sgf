package ua.com.integer.sgf.editor.actor.node.rotateto;

import ua.com.integer.sgf.editor.actor.node.rotateby.PropertyEditor;

public class RotateToPropertyEditor extends PropertyEditor {
	private static final long serialVersionUID = 8980321201871661137L;
	public RotateToPropertyEditor(float rotation, float time) {
		super(rotation, time);
		setTitle("RotateToAction editor");
	}
}
