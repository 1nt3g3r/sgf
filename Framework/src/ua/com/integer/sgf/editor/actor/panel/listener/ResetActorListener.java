package ua.com.integer.sgf.editor.actor.panel.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ua.com.integer.sgf.editor.EditorGame;

public class ResetActorListener implements ActionListener {
	@Override
	public void actionPerformed(ActionEvent arg0) {
		EditorGame.getInstance().getRootActor().reset();
		EditorGame.getInstance().centerActionActor();
	}
}
