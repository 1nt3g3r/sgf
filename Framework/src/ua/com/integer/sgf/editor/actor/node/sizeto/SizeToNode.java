package ua.com.integer.sgf.editor.actor.node.sizeto;

import javax.swing.JDialog;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.action.descriptor.SizeToDescriptor;
import ua.com.integer.sgf.dimension.Dimension;
import ua.com.integer.sgf.editor.actor.node.ActionNode;
import ua.com.integer.sgf.editor.util.SwingUtils;

public class SizeToNode extends ActionNode {
	private static final long serialVersionUID = 7023036951344946376L;
	private Dimension width, height;
	private float time;
	
	public SizeToNode(Dimension width, Dimension height, float time) {
		this.width = width;
		this.height = height;
		this.time = time;
	}
	
	public SizeToNode() {
		this(Dimension.getDimension(1), Dimension.getDimension(1), 1);
	}
	
	@Override
	public String toString() {
		return "sizeTo " + width.getValue() +", " + height.getValue() +", time=" + time;
	}

	@Override
	public boolean isLeaf() {
		return true;
	}
	
	@Override
	public ActionDescriptor getDescriptor() {
		return new SizeToDescriptor(width, height, time);
	}
	
	@Override
	public void editClicked() {
		JDialog dialog = new SizeToPropertyEditor(width, height, time) {
			private static final long serialVersionUID = -7870738388518941369L;
			@Override
			public void okClicked() {
				width = getDx().getDimension();
				height = getDy().getDimension();
				time = getFloatTime();
			}
		};
		SwingUtils.showModalInCenter(dialog);
	}

	@Override
	public ActionNode copy() {
		return new SizeToNode(width, height, time);
	}

}
