package ua.com.integer.sgf.editor.actor.node.scaleto;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ua.com.integer.sgf.editor.util.SwingUtils;

public class ScaleToPropertyEditor extends JDialog {
	private static final long serialVersionUID = -2375823862323848889L;
	private final JPanel contentPanel = new JPanel();
	private JComboBox scaleYBox;
	private JComboBox scaleXBox;
	private JComboBox timeBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ScaleToPropertyEditor dialog = new ScaleToPropertyEditor(0, 1, 2);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ScaleToPropertyEditor(float scaleX, float scaleY, float time) {
		setTitle("ScaleToAction editor");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "ScaleX", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			contentPanel.add(panel);
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			{
				scaleXBox = new JComboBox();
				scaleXBox.setModel(new DefaultComboBoxModel(new String[] {"0", "0.1", "0.2", "0.5", "0.9", "1", "2", "3"}));
				scaleXBox.setEditable(true);
				panel.add(scaleXBox);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "ScaleY", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			contentPanel.add(panel);
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			{
				scaleYBox = new JComboBox();
				scaleYBox.setModel(new DefaultComboBoxModel(new String[] {"0", "0.1", "0.2", "0.5", "0.9", "1", "2", "3"}));
				scaleYBox.setEditable(true);
				panel.add(scaleYBox);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "Time", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			contentPanel.add(panel);
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			{
				timeBox = new JComboBox();
				timeBox.setModel(new DefaultComboBoxModel(new String[] {"0.5", "1", "2", "3", "5", "10", "15"}));
				timeBox.setEditable(true);
				panel.add(timeBox);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						okClicked();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		
		scaleXBox.setSelectedItem(scaleX);
		scaleYBox.setSelectedItem(scaleY);
		timeBox.setSelectedItem(time);
		
		pack();
		SwingUtils.showModalInCenter(this);
	}

	public float getScaleX() {
		float toReturn = 0;
		try {
			toReturn = Float.parseFloat(scaleXBox.getSelectedItem().toString());
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "You input wrong scaleX value! Current value is 0");
		}
		return toReturn;
	}
	
	public float getScaleY() {
		float toReturn = 0;
		try {
			toReturn = Float.parseFloat(scaleYBox.getSelectedItem().toString());
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "You input wrong scaleY value! Current value is 0");
		}
		return toReturn;
	}
	
	public float getTime() {
		float toReturn = 0;
		try {
			toReturn = Float.parseFloat(timeBox.getSelectedItem().toString());
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "You input wrong time value! Current value is 0");
		}
		return toReturn;
	}
	
	
	public void okClicked() {
		
	}
	public JComboBox getScaleYBox() {
		return scaleYBox;
	}
	public JComboBox getScaleXBox() {
		return scaleXBox;
	}
	public JComboBox getTimeBox() {
		return timeBox;
	}
}
