package ua.com.integer.sgf.editor.actor.node.sequence;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.action.descriptor.SequenceDescriptor;
import ua.com.integer.sgf.editor.actor.node.ActionNode;

public class SequenceNode extends ActionNode {
	private static final long serialVersionUID = -3689579993414938609L;
	
	@Override
	public String toString() {
		return "sequence";
	}
	
	@Override
	public boolean isLeaf() {
		return false;
	}

	@Override
	public ActionDescriptor getDescriptor() {
		SequenceDescriptor toReturn = new SequenceDescriptor();
		for(int i = 0; i < getChildCount(); i++) {
			ActionNode child = (ActionNode) getChildAt(i);
			toReturn.addChild(child.getDescriptor());
		}
		return toReturn;
	}

	@Override
	public ActionNode copy() {
		return new SequenceNode();
	}

}
