package ua.com.integer.sgf.editor.actor.node.delay;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.action.descriptor.DelayDescriptor;
import ua.com.integer.sgf.editor.actor.node.ActionNode;

public class DelayNode extends ActionNode {
	private static final long serialVersionUID = -7116077688130980796L;
	private float delayInSeconds = 0;
	
	public DelayNode() {
	}
	
	public DelayNode(float delayInSeconds) {
		this.delayInSeconds = delayInSeconds;
	}
	
	@Override
	public ActionDescriptor getDescriptor() {
		return new DelayDescriptor(delayInSeconds);
	}
	
	@Override
	public boolean isLeaf() {
		return true;
	}
	
	@Override
	public String toString() {
		return "delay " + delayInSeconds;
	}
	
	@Override
	public void editClicked() {
		new PropertyEditor(delayInSeconds) {
			private static final long serialVersionUID = -3457981843028351668L;
			@Override
			public void okClicked() {
				delayInSeconds = getTime();
				dispose();
			}
		};
	}

	@Override
	public ActionNode copy() {
		return new DelayNode(delayInSeconds);
	}
}
