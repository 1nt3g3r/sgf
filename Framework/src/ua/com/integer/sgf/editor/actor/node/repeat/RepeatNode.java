package ua.com.integer.sgf.editor.actor.node.repeat;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.action.descriptor.RepeatDescriptor;
import ua.com.integer.sgf.editor.actor.node.ActionNode;

import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;

public class RepeatNode extends ActionNode {
	private static final long serialVersionUID = 2207757587804202293L;
	private int count;
	
	public RepeatNode() {
		this(RepeatAction.FOREVER);
	}
	
	public RepeatNode(int count) {
		this.count = count;
	}
	
	@Override
	public boolean isLeaf() {
		return false;
	}
	
	@Override
	public String toString() {
		if (count == RepeatAction.FOREVER) {
			return "forever";
		} else {
			return "repeat " + count + " count";
		}
	}

	@Override
	public ActionDescriptor getDescriptor() {
		ActionNode child = null;
		if (getChildCount() > 0) {
			child = (ActionNode) getChildAt(0);
			return new RepeatDescriptor(child.getDescriptor(), count);
		} else {
			return new RepeatDescriptor();
		}
	}
	
	@Override
	public void editClicked() {
		new PropertyEditor(count) {
			private static final long serialVersionUID = 6564971541345280420L;
			@Override
			public void okClicked() {
				count = getCount();
				dispose();
			}
		};
	}

	@Override
	public ActionNode copy() {
		return new RepeatNode();
	}
}
