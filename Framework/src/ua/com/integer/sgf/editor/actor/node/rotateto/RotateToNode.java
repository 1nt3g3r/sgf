package ua.com.integer.sgf.editor.actor.node.rotateto;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.action.descriptor.RotateToDescriptor;
import ua.com.integer.sgf.editor.actor.node.ActionNode;

public class RotateToNode extends ActionNode {
	private static final long serialVersionUID = 7434970074186024299L;
	private float angle;
	private float time;
	
	public RotateToNode(float angle, float time) {
		this.angle = angle;
		this.time = time;
	}
	
	public RotateToNode() {
	}
	
	@Override
	public String toString() {
		return "rotate to " + angle + ", time=" + time;
	}
	
	@Override
	public boolean isLeaf() {
		return true;
	}
	
	@Override
	public ActionDescriptor getDescriptor() {
		return new RotateToDescriptor(angle, time);
	}
	
	@Override
	public void editClicked() {
		new RotateToPropertyEditor(angle, time) {
			private static final long serialVersionUID = -3984347846092671225L;
			@Override
			public void okClicked() {
				angle = getRotation();
				time = getTime();
				dispose();
			}
		};
	}

	@Override
	public ActionNode copy() {
		return new RotateToNode(angle, time);
	}

}
