package ua.com.integer.sgf.editor.actor.node.rotateby;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ua.com.integer.sgf.editor.util.SwingUtils;

public class PropertyEditor extends JDialog {
	private static final long serialVersionUID = 5064260421826544404L;
	private final JPanel contentPanel = new JPanel();
	private JSpinner spinner;
	private JComboBox comboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			PropertyEditor dialog = new PropertyEditor(100, 3.1f);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public PropertyEditor(float rotation, float time) {
		setTitle("RotateByAction editor");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setPreferredSize(new Dimension(300, 100));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		{
			JPanel degreesPanel = new JPanel();
			degreesPanel.setBorder(new TitledBorder(null, "How many degrees rotation is?", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			contentPanel.add(degreesPanel);
			degreesPanel.setLayout(new BoxLayout(degreesPanel, BoxLayout.X_AXIS));
			{
				spinner = new JSpinner();
				spinner.setModel(new SpinnerNumberModel(new Integer(0), null, null, new Integer(10)));
				degreesPanel.add(spinner);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "What time of this rotation?", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			contentPanel.add(panel);
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			{
				comboBox = new JComboBox();
				comboBox.setModel(new DefaultComboBoxModel(new String[] {"0.1", "0.2", "0.5", "1", "2", "3", "5", "7", "10", "20", "30", "60"}));
				comboBox.setEditable(true);
				panel.add(comboBox);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						okClicked();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		
		spinner.setValue(rotation);
		comboBox.setSelectedItem(time);
		
		pack();
		SwingUtils.showModalInCenter(this);
	}
	
	public int getRotation() {
		int rotation = 0;
		try {
			rotation = Integer.parseInt(spinner.getValue().toString());
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "You input wrong rotation! Current rotation is 0");
		}
		return rotation;
	}
	
	public float getTime() {
		float time = 0;
		try {
			time = Float.parseFloat(comboBox.getSelectedItem().toString());
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "You input wrong time value! Current time is 0.0f");
		}
		return time;
	}
	
	public void okClicked() {
		
	}

	public JSpinner getSpinner() {
		return spinner;
	}
	public JComboBox getComboBox() {
		return comboBox;
	}
}
