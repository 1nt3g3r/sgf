package ua.com.integer.sgf.editor.actor.node.sizeby;

import javax.swing.JDialog;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.action.descriptor.SizeByDescriptor;
import ua.com.integer.sgf.dimension.Dimension;
import ua.com.integer.sgf.editor.actor.node.ActionNode;
import ua.com.integer.sgf.editor.util.SwingUtils;

public class SizeByNode extends ActionNode {
	private static final long serialVersionUID = 6814611996522798453L;
	private Dimension dx, dy;
	private float time;
	
	public SizeByNode(Dimension dx, Dimension dy, float time) {
		this.dx = dx;
		this.dy = dy;
		this.time = time;
	}
	
	public SizeByNode() {
		this(Dimension.getDimension(0), Dimension.getDimension(0), 0);
	}
	
	@Override
	public String toString() {
		return "size by " + dx.getValue() + ", " + dy.getValue() + ", time=" + time;
	}
	
	@Override
	public boolean isLeaf() {
		return true;
	}
	
	@Override
	public ActionDescriptor getDescriptor() {
		return new SizeByDescriptor(dx, dy, time);
	}

	@Override
	public void editClicked() {
		JDialog dialog = new SizeByPropertyEditor(dx, dy, time) {
			private static final long serialVersionUID = 8467491174673909342L;
			@Override
			public void okClicked() {
				dx = getDx().getDimension();
				dy = getDy().getDimension();
				time = getFloatTime();
				dispose();
			}
		};
		SwingUtils.showModalInCenter(dialog);
	}

	@Override
	public ActionNode copy() {
		return new SizeByNode(dx, dy, time);
	}
}
