package ua.com.integer.sgf.editor.actor.node.alpha;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.action.descriptor.AlphaDescriptor;
import ua.com.integer.sgf.editor.actor.node.ActionNode;

public class AlphaNode extends ActionNode {
	private static final long serialVersionUID = -5864123618936507280L;
	private float alpha;
	private float time;
	
	public AlphaNode(float alpha, float time) {
		this.alpha = alpha;
		this.time = time;
	}
	
	public AlphaNode() {
	}

	@Override
	public boolean isLeaf() {
		return true;
	}
	
	@Override
	public ActionDescriptor getDescriptor() {
		return new AlphaDescriptor(alpha, time);
	}

	@Override
	public void editClicked() {
		new PropertyEditor(alpha, time) {
			private static final long serialVersionUID = 1454716011578250446L;
			@Override
			public void okClicked() {
				alpha = getAlpha();
				time = getTime();
				dispose();
			}
		};
	}

	@Override
	public ActionNode copy() {
		return new AlphaNode(alpha, time);
	}
	
	@Override
	public String toString() {
		return "alpha " + alpha + ", time=" + time;
	}
}
