package ua.com.integer.sgf.editor.actor.node.repeat;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ua.com.integer.sgf.editor.util.SwingUtils;

public class PropertyEditor extends JDialog {
	private static final long serialVersionUID = -8358025028803536709L;
	private final JPanel contentPanel = new JPanel();
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JPanel comboboxPanel;
	private JComboBox comboBox;
	private JRadioButton rdbtnRepeatForever;
	private JRadioButton rdbtnRepeatCountTimes;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			PropertyEditor dialog = new PropertyEditor(1);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public PropertyEditor(int count) {
		setTitle("Repeat action editor");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JPanel radiobuttonPanel = new JPanel();
			contentPanel.add(radiobuttonPanel, BorderLayout.NORTH);
			{
				rdbtnRepeatForever = new JRadioButton("Repeat forever");
				rdbtnRepeatForever.addActionListener(new UpdatePanelVisibleListener());
				rdbtnRepeatForever.setSelected(true);
				buttonGroup.add(rdbtnRepeatForever);
				radiobuttonPanel.add(rdbtnRepeatForever);
			}
			{
				rdbtnRepeatCountTimes = new JRadioButton("Repeat count times");
				rdbtnRepeatCountTimes.addActionListener(new UpdatePanelVisibleListener());
				buttonGroup.add(rdbtnRepeatCountTimes);
				radiobuttonPanel.add(rdbtnRepeatCountTimes);
			}
		}
		{
			comboboxPanel = new JPanel();
			comboboxPanel.setBorder(new TitledBorder(null, "How many times do you want repeat?", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			contentPanel.add(comboboxPanel, BorderLayout.CENTER);
			comboboxPanel.setLayout(new BoxLayout(comboboxPanel, BoxLayout.X_AXIS));
			{
				comboBox = new JComboBox();
				comboBox.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "100", "1000"}));
				comboBox.setEditable(true);
				comboboxPanel.add(comboBox);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						okClicked();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		
		rdbtnRepeatForever.setSelected(count == -1);
		rdbtnRepeatCountTimes.setSelected(count != -1);
		comboboxPanel.setVisible(count != -1);
		comboBox.setSelectedItem(count);
		
		pack();
		SwingUtils.showModalInCenter(this);
	}
	
	public void okClicked() {
	}
	
	public int getCount() {
		if (rdbtnRepeatCountTimes.isSelected()) {
			int count = 0;
			try {
				count = Integer.parseInt(comboBox.getSelectedItem().toString());
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "You input wrong value! Current value is 0");
			}
			return count;
		} else {
			return -1;
		}
	}
	
	class UpdatePanelVisibleListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			comboboxPanel.setVisible(rdbtnRepeatCountTimes.isSelected());
			pack();
		}
	}
}
