package ua.com.integer.sgf.editor.actor.node.moveto;


import javax.swing.JDialog;
import javax.swing.JOptionPane;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.action.descriptor.MoveToDescriptor;
import ua.com.integer.sgf.dimension.Dimension;
import ua.com.integer.sgf.editor.actor.node.ActionNode;
import ua.com.integer.sgf.editor.util.SwingUtils;

public class MoveToNode extends ActionNode {
	private static final long serialVersionUID = -166820760397759901L;
	private Dimension destinationX, destinationY;
	private float time;
	
	public MoveToNode(Dimension destinationX, Dimension destinationY, float time) {
		this.destinationX = destinationX;
		this.destinationY = destinationY;
		this.time = time;
	}
	
	public MoveToNode() {
		this(Dimension.getDimension(0), Dimension.getDimension(0), 0);
	}
	
	@Override
	public String toString() {
		return "move to " + destinationX.getValue() + ", " + destinationY.getValue() + ", time=" + time;
	}
	
	@Override
	public void editClicked() {
		JDialog dialog = new MoveToEditor(destinationX, destinationY, time) {
			private static final long serialVersionUID = 8479284452872163156L;

			@Override
			public void okClicked() {
				destinationX = getDx().getDimension();
				destinationY = getDy().getDimension();
				try {
					time = Float.parseFloat(getTime().getText());
				} catch (NumberFormatException e) {
					time = 0;
					JOptionPane.showMessageDialog(null, "Wrong time entered! Current time is 0");
				}
				dispose();
			}
		};
		SwingUtils.showModalInCenter(dialog);
	}
	
	@Override
	public ActionDescriptor getDescriptor() {
		return new MoveToDescriptor(destinationX, destinationY, time);
	}
	
	@Override
	public boolean isLeaf() {
		return true;
	}

	@Override
	public ActionNode copy() {
		return new MoveToNode(destinationX, destinationY, time);
	}
}
