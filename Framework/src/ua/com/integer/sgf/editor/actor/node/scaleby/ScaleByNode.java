package ua.com.integer.sgf.editor.actor.node.scaleby;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.action.descriptor.ScaleByDescriptor;
import ua.com.integer.sgf.editor.actor.node.ActionNode;
import ua.com.integer.sgf.editor.actor.node.scaleto.ScaleToPropertyEditor;

public class ScaleByNode extends ActionNode {
	private static final long serialVersionUID = 2051597354156694851L;
	
	private float scaleX, scaleY;
	private float time;

	
	public ScaleByNode(float scaleX, float scaleY, float time) {
		this.scaleX = scaleX;
		this.scaleY = scaleY;
		this.time = time;
	}
	
	public ScaleByNode() {
	}
	
	@Override
	public String toString() {
		return "scale by " + scaleX + ", " + scaleY + ", time=" + time;
	}
	
	@Override
	public ActionDescriptor getDescriptor() {
		return new ScaleByDescriptor(scaleX, scaleY, time);
	}
	
	@Override
	public void editClicked() {
		new ScaleToPropertyEditor(scaleX, scaleY, time) {
			private static final long serialVersionUID = 3029627628556820467L;

			@Override
			public void okClicked() {
				scaleX = getScaleX();
				scaleY = getScaleY();
				time = getTime();
				dispose();
			}
		};
	}
	
	@Override
	public boolean isLeaf() {
		return true;
	}

	@Override
	public ActionNode copy() {
		return new ScaleByNode(scaleX, scaleY, time);
	}
}
