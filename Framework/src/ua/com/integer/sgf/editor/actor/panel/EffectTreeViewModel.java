package ua.com.integer.sgf.editor.actor.panel;

import javax.swing.tree.DefaultTreeModel;

import ua.com.integer.sgf.editor.actor.node.ActionNode;
import ua.com.integer.sgf.editor.actor.node.sequence.SequenceNode;

import com.badlogic.gdx.scenes.scene2d.Action;

public class EffectTreeViewModel extends DefaultTreeModel {
	private static final long serialVersionUID = -8810180838273642294L;
	
	public EffectTreeViewModel(ActionNode node) {
		super(node);
	}
	
	public EffectTreeViewModel() {
		super(new SequenceNode());
	}
	
	public Action getResultAction() {
		ActionNode root = (ActionNode) getRoot();
		return root.getDescriptor().getAction();
	}

}
