package ua.com.integer.sgf.editor.actor.node.moveby;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ua.com.integer.sgf.dimension.Dimension;
import ua.com.integer.sgf.editor.DimensionPanel;

public class MoveByEditor extends JDialog {
	private static final long serialVersionUID = -2410658865791714612L;
	private final JPanel contentPanel = new JPanel();
	private JTextField time;
	private JButton okButton;
	private DimensionPanel dx;
	private DimensionPanel dy;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			MoveByEditor dialog = new MoveByEditor(null, null, 0);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public MoveByEditor(Dimension oldDx, Dimension oldDy, float oldTime) {
		
		setTitle("MoveByEditor");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		{
			dx = new DimensionPanel("DX");
			dx.setDimension(oldDx);
			contentPanel.add(dx);
		}
		{
			dy = new DimensionPanel("DY");
			dy.setDimension(oldDy);
			contentPanel.add(dy);
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "TIME", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			contentPanel.add(panel);
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			{
				time = new JTextField(oldTime + "");
				panel.add(time);
				time.setColumns(10);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						okClicked();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		
		pack();
	}
	
	public void okClicked() {}
	
	public DimensionPanel getDx() {
		return dx;
	}
	public DimensionPanel getDy() {
		return dy;
	}
	
	public JTextField getTime() {
		return time;
	}
	
	public float getFloatTime() {
		float toReturn = 0;
		try {
			toReturn = Float.parseFloat(getTime().getText());
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "You input wrong time value. Current value is 0");
		}
		return toReturn;
	}
}
