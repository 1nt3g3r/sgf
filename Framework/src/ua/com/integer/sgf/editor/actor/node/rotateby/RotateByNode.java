package ua.com.integer.sgf.editor.actor.node.rotateby;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.action.descriptor.RotateByActionDescriptor;
import ua.com.integer.sgf.editor.actor.node.ActionNode;

public class RotateByNode extends ActionNode {
	private static final long serialVersionUID = 828663923709109940L;
	private float degrees;
	private float time;
	
	public RotateByNode(float degrees, float time) {
		this.degrees = degrees;
		this.time = time;
	}
	
	public RotateByNode() {
		degrees = 360;
		time = 1.0f;
	}
	
	@Override
	public String toString() {
		return "rotate_by: " + " degrees=" + degrees + ", duration="+time;
	}
	
	@Override
	public ActionDescriptor getDescriptor() {
		return new RotateByActionDescriptor(degrees, time);
	}
	
	@Override
	public void editClicked() {
		new PropertyEditor(degrees, time) {
			private static final long serialVersionUID = -7076796270497059358L;
			@Override
			public void okClicked() {
				degrees = getRotation();
				time = getTime();
				dispose();
			}
		};
	}
	
	@Override
	public boolean isLeaf() {
		return true;
	}

	@Override
	public ActionNode copy() {
		return new RotateByNode(degrees, time);
	}
}
