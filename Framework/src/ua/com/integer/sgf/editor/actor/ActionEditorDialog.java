package ua.com.integer.sgf.editor.actor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.Box;
import javax.swing.DropMode;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTree;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreeSelectionModel;

import ua.com.integer.sgf.action.ActionType;
import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.actor.GameActor;
import ua.com.integer.sgf.editor.actor.node.ActionNode;
import ua.com.integer.sgf.editor.actor.panel.EffectTreeViewModel;
import ua.com.integer.sgf.editor.actor.panel.actiontree.TreeTransferHandler;

public class ActionEditorDialog extends JDialog {
	private static final long serialVersionUID = -1873360222614446751L;
	private JTree effectTree;
	private GameActor actor;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ActionEditorDialog dialog = new ActionEditorDialog(null);
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the dialog.
	 */
	public ActionEditorDialog(GameActor gameActor) {
		this.actor = gameActor;
		setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
		setTitle("Action editor");
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmSaveAction = new JMenuItem("Save");
		mntmSaveAction.addActionListener(new SaveTopNodeActionListener());
		mnFile.add(mntmSaveAction);
		
		JMenuItem mntmLoad = new JMenuItem("Load");
		mntmLoad.addActionListener(new LoadActionListener());
		mnFile.add(mntmLoad);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		menuBar.add(horizontalStrut_1);
		
		JButton btnClearActions = new JButton("Clear action");
		btnClearActions.addActionListener(new ClearActionsListener());
		menuBar.add(btnClearActions);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		menuBar.add(horizontalStrut);
		
		JButton btnApplyAction = new JButton("Apply action");
		btnApplyAction.addActionListener(new ApplyClickListener());
		menuBar.add(btnApplyAction);
		
		effectTree = new JTree();
		effectTree.setModel(new EffectTreeViewModel());
		effectTree.setDragEnabled(true);
		effectTree.setDropMode(DropMode.ON_OR_INSERT);
		effectTree.setTransferHandler(new TreeTransferHandler());
		effectTree.getSelectionModel().setSelectionMode(
                TreeSelectionModel.CONTIGUOUS_TREE_SELECTION);
		effectTree.addMouseListener(new EditItemListener());
		JScrollPane actionScroll = new JScrollPane(effectTree);
		getContentPane().add(actionScroll, BorderLayout.CENTER);

	}
	
	class EditItemListener extends MouseAdapter {
		private JPopupMenu popupMenu;
		
		@Override
		public void mouseClicked(MouseEvent event) {
			if (event.getButton() == MouseEvent.BUTTON3) {
				rightButtonClicked(event);
			} else if (event.getButton() == MouseEvent.BUTTON1 && event.getClickCount() == 2) {
				leftButtonClicked();
			}
		}
		
		private void leftButtonClicked() {
			if (effectTree.getSelectionPath() == null) {
				return;
			}
			
			Object[] path = effectTree.getSelectionPath().getPath();
			if (path != null) {
				ActionNode selectedNode = (ActionNode) path[path.length-1];
				selectedNode.editClicked();
			}
		}
		
		private void rightButtonClicked(MouseEvent event) {
			if (popupMenu != null) {
				popupMenu.setVisible(false);
			}
			popupMenu = getEditingMenu();
			popupMenu.show(ActionEditorDialog.this, event.getX(), event.getY());
		}
	}
	
	private JPopupMenu getEditingMenu() {
		JPopupMenu toReturn = new JPopupMenu();
			
			JMenuItem saveItem = new JMenuItem("Сохранить выбранную часть как...");
			saveItem.addActionListener(new SaveListener());
			toReturn.add(saveItem);
			
			JMenuItem deleteItem = new JMenuItem("Удалить");
			deleteItem.addActionListener(new DeleteItemClickListener());
			toReturn.add(deleteItem);
			
			toReturn.add(new JSeparator(JSeparator.HORIZONTAL));
			
			toReturn.add(getAddMenu());
			
		return toReturn;
	}
	
	class DeleteItemClickListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			if (getSelectedNode() != null) {
				getSelectedNode().removeFromParent();
			}
			effectTree.updateUI();
			updateProperties();
		}
	}
	
	class SaveListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			Preferences prefs = Preferences.userNodeForPackage(ActionEditorDialog.class);
			String lastDir = prefs.get("save-action-dir", "./actions");
			
			JFileChooser saveDialog = new JFileChooser() {
				private static final long serialVersionUID = -399110502751231912L;
				@Override
				public void approveSelection() {
					if (getSelectedFile().getName().endsWith(".action")) {
						super.approveSelection();
					} else {
						JOptionPane.showMessageDialog(null, "Name should ends by <.action>");
					}
				}
			};
			saveDialog.setCurrentDirectory(new File(lastDir));
			
			if (saveDialog.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				prefs.put("save-action-dir", saveDialog.getSelectedFile().getParent());
				try {
					prefs.flush();
				} catch (BackingStoreException e) {
				}
				
				if (getSelectedNode() != null) {
					ActionDescriptor.saveToFile(saveDialog.getSelectedFile(), getSelectedNode().getDescriptor());
				}
			}
		}
	}
	
	class AddNodeListener implements ActionListener {
		private ActionNode toAdd;
		public AddNodeListener(ActionType type) {
			toAdd = ActionNode.getByActionType(type);
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			effectTree.updateUI();
			if (getSelectedNode() != null) {
				if (!getSelectedNode().isLeaf()) {
					getSelectedNode().add(toAdd);
				} else {
					TreeNode parent = getSelectedNode().getParent();
					if (parent != null) {
						ActionNode parentActionNode = (ActionNode) parent;
						parentActionNode.insert(toAdd, parentActionNode.getIndex(getSelectedNode()) + 1);
					}
				}
			}
			effectTree.updateUI();
		}
		
	};
	
	private void updateProperties() {
		getActiveActor().clearActions();
		getActiveActor().addAction(getModel().getResultAction());
	}
	
	private EffectTreeViewModel getModel() {
		return (EffectTreeViewModel) effectTree.getModel();
	}
	
	private GameActor getActiveActor() {
		return actor;
	}
	
	private ActionNode getSelectedNode() {
		if (effectTree.getSelectionPath() == null) {
			return null;
		}
		Object[] path = effectTree.getSelectionPath().getPath();
		if (path != null) {
			ActionNode selectedNode = (ActionNode) path[path.length-1];
			return selectedNode;
		}
		
		return null;
	}
	
	private JMenu getAddMenu() {
		JMenu toReturn = new JMenu("Добавить...");
			for(ActionType effect : ActionType.values()) {
				JMenuItem rotateItem = new JMenuItem(effect.toString());
				rotateItem.addActionListener(new AddNodeListener(effect));
				toReturn.add(rotateItem);
			}
		return toReturn;
	}

	class ApplyClickListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			updateProperties();
		}
	}
	
	class LoadActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			Preferences prefs = Preferences.userNodeForPackage(ActionEditorDialog.class);
			String lastDir = prefs.get("save-action-dir", "./actions");
			
			JFileChooser saveDialog = new JFileChooser();
			saveDialog.setCurrentDirectory(new File(lastDir));
			
			if (saveDialog.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				prefs.put("save-action-dir", saveDialog.getSelectedFile().getParent());
				try {
					prefs.flush();
				} catch (BackingStoreException e) {
				}
				
				Scanner sc = null;
				try {
					sc = new Scanner(saveDialog.getSelectedFile());
				} catch (FileNotFoundException e) {
					JOptionPane.showMessageDialog(null, "Error during loading action!");
				}
				String jsonAction = sc.nextLine();
				ActionNode actionNode = ActionNode.getByDescriptor(ActionDescriptor.fromJson(jsonAction));
				effectTree.setModel(new EffectTreeViewModel(actionNode));
				effectTree.repaint();
			}
		}
	}
	
	class SaveTopNodeActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			Preferences prefs = Preferences.userNodeForPackage(ActionEditorDialog.class);
			String lastDir = prefs.get("save-action-dir", "./actions");
			
			JFileChooser saveDialog = new JFileChooser() {
				private static final long serialVersionUID = -399110502751231912L;
				@Override
				public void approveSelection() {
					if (getSelectedFile().getName().endsWith(".action")) {
						super.approveSelection();
					} else {
						JOptionPane.showMessageDialog(null, "Name should ends by <.action>");
					}
				}
			};
			saveDialog.setCurrentDirectory(new File(lastDir));
			
			if (saveDialog.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				prefs.put("save-action-dir", saveDialog.getSelectedFile().getParent());
				try {
					prefs.flush();
				} catch (BackingStoreException e) {
				}
				
				ActionNode node = (ActionNode) effectTree.getModel().getRoot();
				ActionDescriptor.saveToFile(saveDialog.getSelectedFile(), node.getDescriptor());
			}
		}
	}
	
	class ClearActionsListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			getActiveActor().clearActions();
		}
	}
}
