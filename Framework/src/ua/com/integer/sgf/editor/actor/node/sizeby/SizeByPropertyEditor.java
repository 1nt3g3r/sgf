package ua.com.integer.sgf.editor.actor.node.sizeby;

import ua.com.integer.sgf.dimension.Dimension;
import ua.com.integer.sgf.editor.actor.node.moveby.MoveByEditor;

public class SizeByPropertyEditor extends MoveByEditor {
	private static final long serialVersionUID = 4835758941554473857L;
	public SizeByPropertyEditor(Dimension oldDx, Dimension oldDy, float oldTime) {
		super(oldDx, oldDy, oldTime);
		setTitle("SizeByAction editor");
	}
}
