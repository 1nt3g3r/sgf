package ua.com.integer.sgf.editor.localize;

import javax.swing.AbstractListModel;

import ua.com.integer.sgf.localize.Book;
import ua.com.integer.sgf.localize.Word;

public class BookListModel extends AbstractListModel {
	private static final long serialVersionUID = 1381647401196587925L;
	private Book book;
	
	public BookListModel(Book book) {
		this.book = book;
	}
	
	@Override
	public Object getElementAt(int index) {
		return book.words.get(index).word + " -> " + book.words.get(index).translatedWord;
	}

	@Override
	public int getSize() {
		return book.words.size;
	}

	public Word wordAt(int index) {
		return book.words.get(index);
	}
}
