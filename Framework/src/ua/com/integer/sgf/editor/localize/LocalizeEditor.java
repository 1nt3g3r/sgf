package ua.com.integer.sgf.editor.localize;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ua.com.integer.sgf.localize.Book;
import ua.com.integer.sgf.localize.Word;

import com.google.gson.Gson;

public class LocalizeEditor extends JDialog {
	private static final long serialVersionUID = -9082836895633697415L;
	private final JPanel contentPanel = new JPanel();
	private JTextField textTranslate;
	private JTextField textNewWord;
	private JTextField textNewWordTranslation;
	private JButton btnSaveTranslation;
	private JButton btnAddWord;
	private JList wordList;
	private JButton btnRemoveWord;
	
	private BookListModel bookModel;
	private Book book;
	
	private Preferences prefs = Preferences.userNodeForPackage(LocalizeEditor.class);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			LocalizeEditor dialog = new LocalizeEditor();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public LocalizeEditor() {
		setTitle("Localize editor");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			wordList = new JList();
			JScrollPane wordScroll = new JScrollPane(wordList);
			wordList.addListSelectionListener(new WordSelectionListener());
			contentPanel.add(wordScroll, BorderLayout.CENTER);
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.SOUTH);
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			{
				JLabel lblTranslation = new JLabel("Translation");
				panel.add(lblTranslation);
			}
			{
				textTranslate = new JTextField();
				panel.add(textTranslate);
				textTranslate.setColumns(10);
			}
			{
				btnSaveTranslation = new JButton("Save translation");
				btnSaveTranslation.addActionListener(new SaveTranslatedWordListener());
				panel.add(btnSaveTranslation);
			}
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.NORTH);
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			{
				JLabel lblWord = new JLabel("Word");
				panel.add(lblWord);
			}
			{
				textNewWord = new JTextField();
				panel.add(textNewWord);
				textNewWord.setColumns(10);
			}
			{
				JLabel lblTranslation_1 = new JLabel("Translation");
				panel.add(lblTranslation_1);
			}
			{
				textNewWordTranslation = new JTextField();
				panel.add(textNewWordTranslation);
				textNewWordTranslation.setColumns(10);
			}
			{
				btnAddWord = new JButton("Add word");
				btnAddWord.addActionListener(new AddWordListener());
				panel.add(btnAddWord);
			}
			{
				Component horizontalStrut = Box.createHorizontalStrut(20);
				panel.add(horizontalStrut);
			}
			{
				btnRemoveWord = new JButton("Remove word");
				btnRemoveWord.addActionListener(new RemoveWordListener());
				panel.add(btnRemoveWord);
			}
		}
		{
			JMenuBar menuBar = new JMenuBar();
			setJMenuBar(menuBar);
			{
				JMenu mnFile = new JMenu("File");
				menuBar.add(mnFile);
				{
					JMenuItem mntmOpen = new JMenuItem("Open");
					mntmOpen.addActionListener(new LoadFromFileListener());
					mnFile.add(mntmOpen);
				}
				{
					JMenuItem mntmSave = new JMenuItem("Save");
					mntmSave.addActionListener(new SaveToFileListener());
					mnFile.add(mntmSave);
				}
			}
		}
		
		book = new Book();
		initModel();
		
		pack();
	}
	
	private void initModel() {
		bookModel = new BookListModel(book);
		wordList.setModel(bookModel);
		wordList.updateUI();
	}
	
	private Word getSelectedWord() {
		if (	book.words.size <= 0 || 
				wordList.getSelectedIndex() > book.words.size-1 ||
				wordList.getSelectedIndex() < 0) {
			return null;
		} else {
			return bookModel.wordAt(wordList.getSelectedIndex());
		}
	}

	class AddWordListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			Word word = new Word();
			word.word = textNewWord.getText();
			word.translatedWord = textNewWordTranslation.getText();
			
			book.words.add(word);
			wordList.updateUI();
			wordList.repaint();
		}
	}
	
	class RemoveWordListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			book.words.removeValue(getSelectedWord(), true);
			wordList.updateUI();
		}
	}
	
	class WordSelectionListener implements ListSelectionListener {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			if (getSelectedWord() != null) {
				textTranslate.setText(getSelectedWord().translatedWord);
			}
		}
	}
	
	class SaveTranslatedWordListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (getSelectedWord() != null) {
				getSelectedWord().translatedWord = textTranslate.getText();
				wordList.updateUI();
			}
		}
	}
	
	class SaveToFileListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String lastPath = prefs.get("last-file", "./");
			JFileChooser fileChooser = new JFileChooser(new File(lastPath));
			if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				prefs.put("last-file", fileChooser.getSelectedFile().getParentFile().toString());
				try {
					prefs.flush();
				} catch (BackingStoreException e2) {}
				
				try {
					book.saveToFile(fileChooser.getSelectedFile());
					JOptionPane.showMessageDialog(null, "File " + fileChooser.getSelectedFile() + " sucessfully saved.");
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(null, "Error during saving " + fileChooser.getSelectedFile() + " file!");
				}
			}
		}
	}
	
	class LoadFromFileListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String lastPath = prefs.get("last-file", "./");
			JFileChooser fileChooser = new JFileChooser(new File(lastPath));
			if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				prefs.put("last-file", fileChooser.getSelectedFile().getParentFile().toString());
				try {
					prefs.flush();
				} catch (BackingStoreException e2) {}
				
				try {
					Scanner sc = new Scanner(fileChooser.getSelectedFile());
					String stringBook = sc.nextLine();
					
					book = new Gson().fromJson(stringBook, Book.class);
					initModel();
					
					sc.close();
				} catch (FileNotFoundException e) {
					JOptionPane.showMessageDialog(null, "Error during loading file!");
				}
			}
		}
		
	}
	
}
