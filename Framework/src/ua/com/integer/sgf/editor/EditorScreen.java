package ua.com.integer.sgf.editor;

import ua.com.integer.sgf.actor.GameActor;
import ua.com.integer.sgf.screen.manager.AbstractScreen;
import ua.com.integer.sgf.screen.manager.ScreenConfig;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class EditorScreen extends AbstractScreen {
	private GameActor actionActor;
	
	class HidePopupMenuListener extends ClickListener {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			EditListener.hidePopupMenu();
		}
	}
	
	public EditorScreen() {
		super(new ScreenConfig());
		getConfig().needDrawBackgroundImage = false;
		getConfig().screenName = "editor-screen";
		getConfig().backgroundColor = Color.BLUE;
		
		getStage().addListener(new HidePopupMenuListener());
	}
	
	public void setActionActor(GameActor actionActor) {
		if (this.actionActor != null) {
			this.actionActor.remove();
		}
		
		this.actionActor = actionActor;
		this.actionActor.setPosition((Gdx.graphics.getWidth() - actionActor.getWidth())/2,
									 (Gdx.graphics.getHeight() - actionActor.getHeight())/2);
		
		addActor(actionActor);
	}
	
	public GameActor getRootActor() {
		return actionActor;
	}
	
	public GameActor getActiveActor() {
		for(Actor actor : getStage().getActors()) {
			GameActor gameActor = (GameActor) actor;
			GameActor activeActor = gameActor.getActiveActor();
			if (activeActor != null) {
				return activeActor;
			}
		}
		return null;
	}
	
	/**
	 * Makes all actor inactive.
	 */
	public void deactivateAllGameActors() {
		for(Actor actor : getStage().getActors()) {
			GameActor gameActor = (GameActor) actor;
			gameActor.deactivateAll();
		}
	}
	
}
