package ua.com.integer.sgf.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import ua.com.integer.sgf.editor.actor.menu.MenuCreator;
import ua.com.integer.sgf.editor.config.GameConfigEditor;
import ua.com.integer.sgf.editor.localize.LocalizeEditor;

import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class ControlFrame extends JDialog {
	private static final long serialVersionUID = -6032418821985029594L;
	private final JPanel contentPanel = new JPanel();
	private JTextField gameDirectoryField;
	
	private Preferences prefs = Preferences.userNodeForPackage(ControlFrame.class);
	private JTextField widthText;
	private JTextField heightText;
	private JCheckBox invertParamsBox;
	private JComboBox standardResolutionsBox;
	private JComboBox scaleBox;
	private JTextField langTextField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ControlFrame dialog = new ControlFrame();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ControlFrame() {
		setResizable(false);
		getContentPane().setBackground(Color.GRAY);
		setTitle("SGF Control Panel");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(Color.GRAY);
		contentPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
			
			JPanel gameDirectoryPanel = new JPanel();
			gameDirectoryPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			gameDirectoryPanel.setBackground(Color.LIGHT_GRAY);
			contentPanel.add(gameDirectoryPanel);
			gameDirectoryPanel.setLayout(new BoxLayout(gameDirectoryPanel, BoxLayout.X_AXIS));
			
			Component horizontalGlue_2 = Box.createHorizontalGlue();
			gameDirectoryPanel.add(horizontalGlue_2);
			
			JLabel lblGameDirectory = new JLabel("Game directory");
			gameDirectoryPanel.add(lblGameDirectory);
			
			Component horizontalStrut = Box.createHorizontalStrut(20);
			gameDirectoryPanel.add(horizontalStrut);
			
			gameDirectoryField = new JTextField();
			gameDirectoryField.setText(prefs.get("base-directory", ""));
			gameDirectoryPanel.add(gameDirectoryField);
			gameDirectoryField.setColumns(10);
		}
		
		Component verticalStrut = Box.createVerticalStrut(20);
		contentPanel.add(verticalStrut);
		
		JPanel panelStandardResolutions = new JPanel();
		panelStandardResolutions.setBorder(new TitledBorder(null, "Standard resolutions", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPanel.add(panelStandardResolutions);
		
		standardResolutionsBox = new JComboBox();
		standardResolutionsBox.addActionListener(new StandardResolutionSelectListener());
		standardResolutionsBox.setModel(new DefaultComboBoxModel(new String[] {"<custom>", "1280*800", "1024*768", "1024*600", "800*600", "800*480", "600*480", "480*320", "320*240"}));
		panelStandardResolutions.add(standardResolutionsBox);
		
		invertParamsBox = new JCheckBox("Swap width and height");
		invertParamsBox.addActionListener(new StandardResolutionSelectListener());
		panelStandardResolutions.add(invertParamsBox);
		
		JPanel resolutionPanel = new JPanel();
		resolutionPanel.setBackground(Color.LIGHT_GRAY);
		resolutionPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		contentPanel.add(resolutionPanel);
		resolutionPanel.setLayout(new BoxLayout(resolutionPanel, BoxLayout.X_AXIS));
		
		JLabel lblScreenResolution = new JLabel("Screen resolution:");
		resolutionPanel.add(lblScreenResolution);
		
		widthText = new JTextField(prefs.getInt("screen-width", 800) + "");
		resolutionPanel.add(widthText);
		widthText.setColumns(4);
		
		JLabel lblX = new JLabel("x");
		resolutionPanel.add(lblX);
		
		heightText = new JTextField(prefs.getInt("screen-height", 480) + "");
		resolutionPanel.add(heightText);
		heightText.setColumns(4);
		
		JLabel lblPixels = new JLabel(" pixels");
		resolutionPanel.add(lblPixels);
		
		JPanel panelScaleScreen = new JPanel();
		panelScaleScreen.setBorder(new TitledBorder(null, "Scale dimension", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelScaleScreen.setBackground(Color.LIGHT_GRAY);
		contentPanel.add(panelScaleScreen);
		panelScaleScreen.setLayout(new BoxLayout(panelScaleScreen, BoxLayout.X_AXIS));
		
		scaleBox = new JComboBox();
		scaleBox.setModel(new DefaultComboBoxModel(new String[] {"<not scale>", "0.9", "0.8", "0.7", "0.6", "0.5", "0.4", "0.3", "0.2", "0.1"}));
		scaleBox.setSelectedItem(prefs.get("scale", "<dont scale>"));
		panelScaleScreen.add(scaleBox);
		
		Component verticalStrut_1 = Box.createVerticalStrut(20);
		contentPanel.add(verticalStrut_1);
		
		JPanel langPrefixPanel = new JPanel();
		langPrefixPanel.setBackground(Color.LIGHT_GRAY);
		langPrefixPanel.setBorder(new TitledBorder(null, "Language prefix", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPanel.add(langPrefixPanel);
		
		langTextField = new JTextField(prefs.get("lang-prefix", "ru"));
		langPrefixPanel.add(langTextField);
		langTextField.setColumns(10);
		
		pack();
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenuItem mntmStartGame = new JMenuItem("Start game");
		mntmStartGame.addActionListener(new StartGameListener());
		mntmStartGame.setFont(new Font("Dialog", Font.BOLD, 16));
		mntmStartGame.setBackground(Color.LIGHT_GRAY);
		menuBar.add(mntmStartGame);
		
		JMenu resourceMenu = new JMenu("Resources");
		resourceMenu.setForeground(Color.BLACK);
		resourceMenu.setBackground(Color.DARK_GRAY);
		menuBar.add(resourceMenu);
		
		JMenuItem mntmActors = new JMenuItem("Actors");
		mntmActors.setBackground(Color.LIGHT_GRAY);
		mntmActors.addActionListener(new ActorsEditorListener());
		resourceMenu.add(mntmActors);
		
		JMenuItem mntmLocalization = new JMenuItem("Localization");
		mntmLocalization.setBackground(Color.LIGHT_GRAY);
		mntmLocalization.addActionListener(new LocalizeEditorListener());
		resourceMenu.add(mntmLocalization);
		
		JMenuItem mntmGameConfiguration = new JMenuItem("Configuration");
		mntmGameConfiguration.setBackground(Color.LIGHT_GRAY);
		mntmGameConfiguration.addActionListener(new GameConfigListener());
		menuBar.add(mntmGameConfiguration);
	}
	
	class GameConfigListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			GameConfigEditor dialog = new GameConfigEditor();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		}
	}
	
	class LocalizeEditorListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			LocalizeEditor editor = new LocalizeEditor();
			editor.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			editor.setVisible(true);
		}
	}
	
	class ActorsEditorListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				LwjglApplicationConfiguration config = getConfig("Editor");
				
				EditorFrame frame = new EditorFrame(EditorGame.getInstance(gameDirectoryField.getText()), config.title, config.width, config.height, false);
				EditorGame.getInstance().setEditorFrame(frame);
				frame.setJMenuBar(new JMenuBar());

				MenuCreator menuCreator = new MenuCreator();
				frame.getJMenuBar().add(menuCreator.getSimpleActorMenu());
				frame.getJMenuBar().add(menuCreator.getAdditionalMenu());
				
				frame.setVisible(true);
				EditorGame.getInstance().setLanguage(langTextField.getText());
			} catch (IllegalStateException ex) {
				JOptionPane.showMessageDialog(null, "Can't run editor because your run params incorrect!");
			}
		}
	}
	
	private LwjglApplicationConfiguration getConfig(String appName) {
		if (gameDirectoryField.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Select game directory!");
			throw new IllegalStateException();
		}
		
		float multiplier = 1;
		if (!scaleBox.getSelectedItem().equals("<not scale>")) {
			multiplier = Float.parseFloat(scaleBox.getSelectedItem().toString());
		}
		int gameWidth = 0;
		int gameHeight = 0;
		try {
			gameWidth = Integer.parseInt(widthText.getText());
			gameHeight = Integer.parseInt(heightText.getText());
		} catch (NumberFormatException ex) {
			JOptionPane.showMessageDialog(null, "Input correct width and height!");
			throw new IllegalStateException();
		}
		
		
		prefs.put("base-directory", gameDirectoryField.getText());
		prefs.putInt("screen-width", gameWidth);
		prefs.putInt("screen-height", gameHeight);
		prefs.put("scale", scaleBox.getSelectedItem().toString());
		prefs.put("lang-prefix", langTextField.getText());
		
		gameWidth = (int) (gameWidth * multiplier);
		gameHeight = (int) (gameHeight * multiplier);
		
		try {
			prefs.flush();
		} catch (BackingStoreException e1) {
			e1.printStackTrace();
		}
			
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = gameWidth;
		config.height = gameHeight;
		config.title = appName;
		
		return config;
	}
	
	class StandardResolutionSelectListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (!standardResolutionsBox.getSelectedItem().toString().equals("<custom>")) {
				String[] parts = standardResolutionsBox.getSelectedItem().toString().split("\\*");
				String width = parts[0];
				String height = parts[1];
				if (invertParamsBox.isSelected()) {
					widthText.setText(height);
					heightText.setText(width);
				} else {
					widthText.setText(width);
					heightText.setText(height);
				}
			} 
		}
	}
	
	class StartGameListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			try {
				LwjglApplicationConfiguration config = getConfig("Game");
				EditorGame.startDesktopGame(config);
			} catch (IllegalStateException e) {
				JOptionPane.showMessageDialog(null, "Can't run game because your run params incorrect!");
			}
		}
	}
	public JComboBox getScaleBox() {
		return scaleBox;
	}
	public JTextField getLangTextField() {
		return langTextField;
	}
}
