package ua.com.integer.sgf.editor.config;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;

import ua.com.integer.sgf.kernel.GameConfig;

import com.google.gson.Gson;

public class GameConfigEditor extends JDialog {
	private static final long serialVersionUID = -3913150744004876591L;
	private JTextField textPathToImages;
	private JTextField textPathToLocalizeDirectory;
	private JTextField textPathToFilesWithActions;
	private JTextField textPathToActors;
	private JTextField textPathToTTF;
	private JTextField textPathToSounds;
	private JTextField textPathToMusic;
	
	private Preferences prefs = Preferences.userNodeForPackage(GameConfigEditor.class);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			GameConfigEditor dialog = new GameConfigEditor();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public GameConfigEditor() {
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setResizable(false);
		setTitle("Game configuration editor");
		setBounds(100, 100, 347, 208);
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		{
			Component verticalStrut = Box.createVerticalStrut(20);
			getContentPane().add(verticalStrut);
		}
		{
			JPanel actorPathPanel = new JPanel();
			actorPathPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			getContentPane().add(actorPathPanel);
			actorPathPanel.setLayout(new BoxLayout(actorPathPanel, BoxLayout.X_AXIS));
			{
				JLabel labelPathToActors = new JLabel("Path to actors directory          ");
				actorPathPanel.add(labelPathToActors);
			}
			{
				textPathToActors = new JTextField();
				actorPathPanel.add(textPathToActors);
				textPathToActors.setColumns(10);
			}
		}
		{
			Component verticalStrut = Box.createVerticalStrut(20);
			getContentPane().add(verticalStrut);
		}
		{
			JPanel imagePathPanel = new JPanel();
			imagePathPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			getContentPane().add(imagePathPanel);
			imagePathPanel.setLayout(new BoxLayout(imagePathPanel, BoxLayout.X_AXIS));
			{
				JLabel labelPathToImages = new JLabel("Path to images directory         ");
				imagePathPanel.add(labelPathToImages);
			}
			{
				textPathToImages = new JTextField();
				imagePathPanel.add(textPathToImages);
				textPathToImages.setColumns(10);
			}
		}
		{
			Component verticalStrut = Box.createVerticalStrut(20);
			getContentPane().add(verticalStrut);
		}
		{
			JPanel soundPathPanel = new JPanel();
			soundPathPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			getContentPane().add(soundPathPanel);
			soundPathPanel.setLayout(new BoxLayout(soundPathPanel, BoxLayout.X_AXIS));
			{
				JLabel labelPathToSounds = new JLabel("Path to sounds directory         ");
				soundPathPanel.add(labelPathToSounds);
			}
			{
				textPathToSounds = new JTextField();
				textPathToSounds.setColumns(10);
				soundPathPanel.add(textPathToSounds);
			}
		}
		{
			Component verticalStrut = Box.createVerticalStrut(20);
			getContentPane().add(verticalStrut);
		}
		{
			JPanel musicPathPanel = new JPanel();
			musicPathPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			getContentPane().add(musicPathPanel);
			musicPathPanel.setLayout(new BoxLayout(musicPathPanel, BoxLayout.X_AXIS));
			{
				JLabel labelPathToMusic = new JLabel("Path to music files directory    ");
				musicPathPanel.add(labelPathToMusic);
			}
			{
				textPathToMusic = new JTextField();
				musicPathPanel.add(textPathToMusic);
				textPathToMusic.setColumns(10);
			}
		}
		{
			Component verticalStrut = Box.createVerticalStrut(20);
			getContentPane().add(verticalStrut);
		}
		{
			JPanel localizePathPanel = new JPanel();
			localizePathPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			getContentPane().add(localizePathPanel);
			localizePathPanel.setLayout(new BoxLayout(localizePathPanel, BoxLayout.X_AXIS));
			{
				JLabel labelPathToLocalize = new JLabel("Path to localize files directory");
				localizePathPanel.add(labelPathToLocalize);
			}
			{
				textPathToLocalizeDirectory = new JTextField();
				localizePathPanel.add(textPathToLocalizeDirectory);
				textPathToLocalizeDirectory.setColumns(10);
			}
		}
		{
			Component verticalStrut = Box.createVerticalStrut(20);
			getContentPane().add(verticalStrut);
		}
		{
			JPanel actionPathPanel = new JPanel();
			actionPathPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			getContentPane().add(actionPathPanel);
			actionPathPanel.setLayout(new BoxLayout(actionPathPanel, BoxLayout.X_AXIS));
			{
				JLabel lblPathToFilesWithActions = new JLabel("Path to files with actions         ");
				actionPathPanel.add(lblPathToFilesWithActions);
			}
			{
				textPathToFilesWithActions = new JTextField();
				actionPathPanel.add(textPathToFilesWithActions);
				textPathToFilesWithActions.setColumns(10);
			}
		}
		{
			Component verticalStrut = Box.createVerticalStrut(20);
			getContentPane().add(verticalStrut);
		}
		{
			JPanel fontPathPanel = new JPanel();
			fontPathPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			getContentPane().add(fontPathPanel);
			fontPathPanel.setLayout(new BoxLayout(fontPathPanel, BoxLayout.X_AXIS));
			{
				JLabel lblPathTottf = new JLabel("Path to \".ttf\" fonts                     ");
				fontPathPanel.add(lblPathTottf);
			}
			{
				textPathToTTF = new JTextField();
				fontPathPanel.add(textPathToTTF);
				textPathToTTF.setColumns(10);
			}
		}
		{
			Component verticalStrut = Box.createVerticalStrut(20);
			getContentPane().add(verticalStrut);
		}
		{
			JMenuBar menuBar = new JMenuBar();
			setJMenuBar(menuBar);
			{
				JMenu fileMenu = new JMenu("File");
				menuBar.add(fileMenu);
				{
					JMenuItem openConfig = new JMenuItem("Open");
					openConfig.addActionListener(new LoadConfigListener());
					fileMenu.add(openConfig);
				}
				{
					JMenuItem saveConfig = new JMenuItem("Save");
					saveConfig.addActionListener(new SaveConfigListener());
					fileMenu.add(saveConfig);
				}
				{
					JSeparator separator = new JSeparator();
					fileMenu.add(separator);
				}
				{
					JMenuItem exit = new JMenuItem("Exit");
					exit.addActionListener(new ExitListener());
					fileMenu.add(exit);
				}
			}
		}
		
		pack();
	}
	
	class SaveConfigListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			GameConfig config = new GameConfig();
			config.imageDirectory = textPathToImages.getText();
			config.soundDirectory = textPathToSounds.getText();
			config.musicDirectory = textPathToMusic.getText();
			config.localizeDirectory = textPathToLocalizeDirectory.getText();
			config.actionDirectory = textPathToFilesWithActions.getText();
			config.actorDirectory = textPathToActors.getText();
			config.ttfFontsDirectory = textPathToTTF.getText();
			
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(new File(getPref("config-dir")));
			if (fileChooser.showSaveDialog(GameConfigEditor.this) == JFileChooser.APPROVE_OPTION) {
				savePref("config-dir", fileChooser.getSelectedFile().getAbsolutePath());
				Gson gson = new Gson();
				String savedToJson = gson.toJson(config);
				
				FileWriter fWriter;
				try {
					fWriter = new FileWriter(fileChooser.getSelectedFile());
					fWriter.write(savedToJson);
					fWriter.flush();
					fWriter.close();
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "Error during saving game config!");
				}
			}
		}
	}
	
	private void savePref(String name, String value) {
		prefs.put(name, value);
		try {
			prefs.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}
	
	private String getPref(String name) {
		return prefs.get(name, "");
	}
	
	class LoadConfigListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(new File(getPref("config-dir")));
			if (fileChooser.showOpenDialog(GameConfigEditor.this) == JFileChooser.APPROVE_OPTION) {
				savePref("config-dir", fileChooser.getSelectedFile().getAbsolutePath());
				Gson gson = new Gson();
				GameConfig config;
				try {
					config = gson.fromJson(new FileReader(fileChooser.getSelectedFile()), GameConfig.class);
					textPathToImages.setText(config.imageDirectory);
					textPathToSounds.setText(config.soundDirectory);
					textPathToMusic.setText(config.musicDirectory);
					textPathToLocalizeDirectory.setText(config.localizeDirectory);
					textPathToFilesWithActions.setText(config.actionDirectory);
					textPathToActors.setText(config.actorDirectory);
					textPathToTTF.setText(config.ttfFontsDirectory);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Error during reading game config!");
				}
			}
		}
	}
	
	class ExitListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			GameConfigEditor.this.dispose();
		}
	}
	public JTextField getTextPathToTTF() {
		return textPathToTTF;
	}
}
