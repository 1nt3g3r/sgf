package ua.com.integer.sgf.editor;

import com.badlogic.gdx.graphics.Texture;

import ua.com.integer.sgf.actor.GameActor;
import ua.com.integer.sgf.actor.simple.SimpleActor;
import ua.com.integer.sgf.kernel.GameKernel;
import ua.com.integer.sgf.kernel.LoadingActor;
import ua.com.integer.sgf.kernel.screen.loading.LoadingScreen;

public class EditorGame extends GameKernel {
	private EditorFrame editorFrame;
	private static EditorGame instance;
	private String language = "en";
	
	public EditorGame(String baseDirectory) {
		super(baseDirectory);
		Texture.setEnforcePotImages(false);
	}

	public static EditorGame getInstance(String baseDirectory) {
		if (instance == null) {
			instance = new EditorGame(baseDirectory);
			
		}
		return instance;
	}
	
	public static EditorGame getInstance() {
		if (instance == null) {
			System.out.println("You should call getInstance(String baseDirectory) first!");
		}
		return instance;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}

	public void setActionActor(GameActor actionActor) {
		actionActor.debug();
		actionActor.addListener(new EditListener(actionActor));
		res().screens().getScreen(EditorScreen.class).setActionActor(actionActor);
		
		if (actionActor instanceof SimpleActor) {
			((SimpleActor) actionActor).deactivateAllGameActors();
			actionActor.activate();
		}
	}
	
	public void setEditorFrame(EditorFrame editorFrame) {
		this.editorFrame = editorFrame;
	}
	
	public EditorFrame getEditorFrame() {
		return editorFrame;
	}
	
	public GameActor getRootActor() {
		return res().screens().getScreen(EditorScreen.class).getRootActor();
	}

	public void resetActionActor() {
		if (getRootActor() != null) {
			getRootActor().reset();
			centerActionActor();
		}
	}
	public void centerActionActor() {
		setActionActor(getRootActor());
	}

	@Override
	public void created() {
		rm().loadAll();
		LoadingActor loadingActor = new LoadingActor(true) {
			@Override
			public void loadFinished() {
				res().screens().addScreen(new EditorScreen());
				setActionActor(new SimpleActor());
				res().screens().showScreen("editor-screen");
				created = true;
			}
		};
		
		LoadingScreen loadingScreen = new LoadingScreen(rm(), loadingActor);
		loadingScreen.addActor(loadingActor);
		
		res().screens().addScreen(loadingScreen);
		res().screens().showScreen("loading-screen");
		lc().setCurrentLanguage(language);
	}

	public GameActor getActiveActor() {
		return res().screens().getScreen(EditorScreen.class).getActiveActor();
	}
	
}
