package ua.com.integer.sgf.editor;

import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import ua.com.integer.sgf.actor.GameActor;
import ua.com.integer.sgf.actor.simple.SimpleActor;
import ua.com.integer.sgf.editor.actor.ActionEditorDialog;
import ua.com.integer.sgf.editor.util.SwingUtils;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class EditListener extends InputListener {
	private static JPopupMenu popupMenu;
	private ActionEditorDialog actionEditor;
	private GameActor actor;
	
	private Action lastAction;
	
	public EditListener(GameActor actor) {
		this.actor = actor;
		actionEditor = new ActionEditorDialog(actor);
	}
	
	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		if (!event.isHandled()) {
			createLastAction(keycode);
			
			if (lastAction != null) {
				actor.addAction(lastAction);
			}
		}
		return true;
	}
	
	private void createLastAction(int keycode) {
		actor.removeAction(lastAction);
		lastAction = null;
		switch(keycode) {
		case Keys.LEFT : lastAction = Actions.forever(Actions.moveBy(-30, 0, 1.0f)); break;
		case Keys.RIGHT : lastAction = Actions.forever(Actions.moveBy(30, 0, 1.0f)); break;
		case Keys.UP : lastAction = Actions.forever(Actions.moveBy(0, 30, 1.0f)); break;
		case Keys.DOWN : lastAction = Actions.forever(Actions.moveBy(0, -30, 1.0f)); break;
		case Keys.A : lastAction = Actions.forever(Actions.sizeBy(-30, 0, 1.0f)); break;
		case Keys.D : lastAction = Actions.forever(Actions.sizeBy(30, 0, 1.0f)); break;
		case Keys.S : lastAction = Actions.forever(Actions.sizeBy(0, -30, 1.0f)); break;
		case Keys.W : lastAction = Actions.forever(Actions.sizeBy(0, 30, 1.0f)); break;
		}
		
		if (actor instanceof SimpleActor) {
			SimpleActor simpleActor = (SimpleActor) actor;
			simpleActor.updateLabelPosition();
		}
	}
	
	@Override
	public boolean keyUp(InputEvent event, int keycode) {
		actor.removeAction(lastAction);
		return super.keyUp(event, keycode);
	}
	
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer,
			int button) {
		lastMouseButton = event.getButton();
		actor.activate();
		return true;
	}
	
	@Override
	public void touchUp(InputEvent event, float x, float y, int pointer,
			int button) {
		if (!event.isCancelled()) {
			hidePopupMenu();
			if (event.getButton() == 0) {
				actor.activate();
			} else if (event.getButton() == 1) {
				showEditingMenu();
			}
			event.cancel();
		}
	}
	
	private int lastMouseButton;
	@Override
	public void touchDragged(InputEvent event, float x, float y, int pointer) {
		if(!event.isCancelled() && lastMouseButton == 0) {
			event.cancel();
			EditorFrame frame = EditorGame.getInstance().getEditorFrame();
			Point mousePoint = frame.getMousePosition();
			if (mousePoint == null) {
				return;
			}
			int mouseX = mousePoint.x;
			int mouseY = frame.getHeight() - mousePoint.y;
			
			mouseX -= actor.getWidth()/2;
			mouseY -= actor.getHeight()/2;
			
			if (actor == EditorGame.getInstance().getRootActor()) {
				actor.setPosition(mouseX, mouseY);
			} else {
				Vector2 coords = new Vector2(mouseX, mouseY);
				Vector2 translated = actor.getParent().stageToLocalCoordinates(coords);
				actor.setPosition(translated.x, translated.y);
			}
		}
	}
	
	private void showEditingMenu() {
		popupMenu = new JPopupMenu();
			JMenuItem propertiesItem = new JMenuItem("Properties");
			propertiesItem.addActionListener(new PropertyClickedListener());
			popupMenu.add(propertiesItem);
			
			JMenuItem eventItem = new JMenuItem("Events");
			eventItem.addActionListener(new EventClickedListener());
			popupMenu.add(eventItem);
			
			JMenuItem actionItem = new JMenuItem("Actions");
			actionItem.addActionListener(new ActionClickedListener());
			popupMenu.add(actionItem);
			
			JMenuItem labelItem = new JMenuItem("Label");
			labelItem.addActionListener(new LabelClickedListener());
			popupMenu.add(labelItem);
			
			JMenuItem insertItem = new JMenuItem("Insert actor");
			insertItem.addActionListener(new InsertNewItemMenuListener());
			popupMenu.add(insertItem);
			
			JMenuItem removeItem = new JMenuItem("Remove");
			removeItem.addActionListener(new RemoveActorItemMenuListener());
			popupMenu.add(removeItem);
		Point mPos = MouseInfo.getPointerInfo().getLocation();
		Point winPos = EditorGame.getInstance().getEditorFrame().getLocation();
		popupMenu.show(EditorGame.getInstance().getEditorFrame(), mPos.x - winPos.x, mPos.y - winPos.y);
	}
	
	class PropertyClickedListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			hidePopupMenu();
			actor.editProperties();
		}
	}
	
	class EventClickedListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			hidePopupMenu();
			actor.editActionEvents();
		}
	}
	
	class ActionClickedListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			hidePopupMenu();
			SwingUtils.showInCenter(actionEditor);
		}
	}
	
	class LabelClickedListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			hidePopupMenu();
			actor.editLabel();
		}
	}
	
	class InsertNewItemMenuListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			GameActor childActor = new SimpleActor();
			childActor.addListener(new EditListener(childActor));
			childActor.debug();
			childActor.setSize(actor.getWidth()/2, actor.getHeight()/2);
			childActor.setPosition(actor.getWidth()/4, actor.getHeight()/4);
			actor.addActor(childActor);
			childActor.activate();
		}
	}
	
	class RemoveActorItemMenuListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (actor == EditorGame.getInstance().getRootActor()) {
				JOptionPane.showMessageDialog(null, "Can't remove root actor");
			} else {
				((GameActor) actor.getParent()).activate();
				actor.remove();
			}
		}
	}
	
	public static void hidePopupMenu() {
		if (popupMenu != null) {
			popupMenu.setVisible(false);
			popupMenu = null;
		}
	}
	
}
