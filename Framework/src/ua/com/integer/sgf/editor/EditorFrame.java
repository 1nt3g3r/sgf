package ua.com.integer.sgf.editor;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.lwjgl.LwjglFrame;

public class EditorFrame extends LwjglFrame {
	private static final long serialVersionUID = 5213885807225623257L;
	public static final int MENU_BAR_HEIGHT = 21;
	
	public EditorFrame(ApplicationListener listener, String title, int width,
			int height, boolean useGL2) {
		super(listener, title, width, height + MENU_BAR_HEIGHT, useGL2);
	}
}
