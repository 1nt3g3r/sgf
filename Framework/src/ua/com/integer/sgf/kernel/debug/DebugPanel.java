package ua.com.integer.sgf.kernel.debug;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

/**
 * Debug panel. Shows you different debug info: frames per second, memory usage, etc. 
 * By default it locates in top screen place.
 * 
 * @author <1nt3g3r>
 */
public class DebugPanel extends Group {
	private LabelStyle labelStyle;
	private Label memoryLabel;
	private Label fpsLabel;
	
	private BitmapFont font;
	
	class UpdateTask extends Task {
		@Override
		public void run() {
			memoryLabel.setText("Memory usage: " + Gdx.app.getNativeHeap()/1024 + " KBytes");
			fpsLabel.setText("FPS: " + Gdx.graphics.getFramesPerSecond());
		}
	}
	
	public DebugPanel() {
		setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()/4);
		setPosition(0, Gdx.graphics.getHeight()*3/4);
		
		font = new BitmapFont();
		
		labelStyle = new LabelStyle();
		labelStyle.font = font;
		
		initMemoryLabel();
		initFPSLabel();
		
		Timer.schedule(new UpdateTask(), 0, 1.0f);
	}
	
	private void initMemoryLabel() {
		memoryLabel = new Label("", labelStyle);
		memoryLabel.setPosition(0, getHeight() - memoryLabel.getHeight() - 10);
		addActor(memoryLabel);
	}
	
	private void initFPSLabel() {
		fpsLabel = new Label("", labelStyle);
		fpsLabel.setY(memoryLabel.getY() - fpsLabel.getHeight() - 10);
		addActor(fpsLabel);
	}
}
