package ua.com.integer.sgf.kernel;


import ua.com.integer.sgf.action.ActionManager;
import ua.com.integer.sgf.actor.simple.ActorManager;
import ua.com.integer.sgf.fontmanager.TTFFontManager;
import ua.com.integer.sgf.resource.ResourceManager;
import ua.com.integer.sgf.resource.TextureManager;
import ua.com.integer.sgf.screen.manager.ScreenManager;
import ua.com.integer.sgf.sound.manager.MusicManager;
import ua.com.integer.sgf.sound.manager.SoundManager;

public class Resources {
	private ResourceManager resourceManager;
	
	public Resources(ResourceManager resourceManager) {
		this.resourceManager = resourceManager;
	}
	
	public ActorManager actors() {
		return resourceManager.getManager(ActorManager.class);
	}
	
	public TextureManager textures() {
		return resourceManager.getManager(TextureManager.class);
	}
	
	public ActionManager actions() {
		return resourceManager.getManager(ActionManager.class);
	}
	
	public ScreenManager screens() {
		return resourceManager.getManager(ScreenManager.class);
	}
	
	public TTFFontManager fonts() {
		return resourceManager.getManager(TTFFontManager.class);
	}
	
	public MusicManager music() {
		return resourceManager.getManager(MusicManager.class);
	}
	
	public SoundManager sounds() {
		return resourceManager.getManager(SoundManager.class);
	}
}
