package ua.com.integer.sgf.kernel.screen.loading;

public interface LoadingListener {
	public void loadDescription(String loadDescription);
	public void loadProgress(float percent);
	public void loadingFinished();
}
