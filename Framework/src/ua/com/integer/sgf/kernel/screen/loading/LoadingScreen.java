package ua.com.integer.sgf.kernel.screen.loading;

import ua.com.integer.sgf.resource.ResourceManager;
import ua.com.integer.sgf.screen.manager.AbstractScreen;

public class LoadingScreen extends AbstractScreen {
	private ResourceManager rm;
	private LoadingListener loadingFinishedListener;
	private boolean firstRender = true;
	private Runnable firstRunnable;
	
	public LoadingScreen(ResourceManager rm, LoadingListener loadingFinishedListener) {
		super("loading-screen");
		this.rm = rm;
		this.loadingFinishedListener = loadingFinishedListener;
		getConfig().needDrawBackgroundImage = false;
		
		if (rm == null) {
			throw new IllegalArgumentException("Resourse Manager can't be null!");
		}
		
		if (loadingFinishedListener == null) {
			throw new IllegalArgumentException("Loading listener can't be null");
		}
	}
	
	public void setFirstRunnable(Runnable firstRunnable) {
		this.firstRunnable = firstRunnable;
	}
	
	@Override
	public void render(float delta) {
		if (firstRender) {
			super.render(delta);
			firstRender = false;
			return;
		} else {
			if (firstRunnable != null) {
				firstRunnable.run();
				firstRunnable = null;
				super.render(delta);
				return;
			}
			
			if (!rm.loadStep()) {
				loadingFinishedListener.loadProgress(rm.getLoadPercent());
				loadingFinishedListener.loadDescription(rm.getLoadDescription());
				super.render(delta);
			} else {
				loadingFinishedListener.loadingFinished();
			}
		}
	}
}
