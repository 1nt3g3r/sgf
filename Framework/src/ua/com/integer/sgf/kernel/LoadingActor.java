package ua.com.integer.sgf.kernel;

import ua.com.integer.sgf.actor.simple.SimpleActor;
import ua.com.integer.sgf.kernel.screen.loading.LoadingListener;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

public abstract class LoadingActor extends SimpleActor implements LoadingListener {
	private BitmapFont font;
	private Label progressLabel;
	private Label loadNowLabel;
	private boolean useDefaultLabel;
	
	public LoadingActor() {
		this(true);
	}
	
	public LoadingActor(boolean useDefaultLabel) {
		this.useDefaultLabel = useDefaultLabel;
		if (useDefaultLabel) {
			font = new BitmapFont();
			
			LabelStyle style = new LabelStyle();
			style.font = font;
			
			progressLabel = new Label("Loading... 0%", style);
			addActor(progressLabel);
			
			loadNowLabel = new Label("", style);
			addActor(loadNowLabel);
		}
		setSize(0, 0);
		setPosition(0, 0);
	}
	
	@Override
	public void loadDescription(String loadDescription) {
		if (useDefaultLabel) {
			loadNowLabel.setText(loadDescription);
			loadNowLabel.setPosition((Gdx.graphics.getWidth() - loadNowLabel.getPrefWidth())/2,
					progressLabel.getY() - loadNowLabel.getHeight()*2);
		}
	}
	
	@Override
	public void loadProgress(float percent) {
		if (useDefaultLabel) {
			progressLabel.setText("Loading... " + (int) (percent*100) + "%");
			progressLabel.setPosition((Gdx.graphics.getWidth() - progressLabel.getPrefWidth())/2,
					(Gdx.graphics.getHeight() - progressLabel.getPrefHeight())/2);
		}
	}

	@Override
	public void loadingFinished() {
		if (useDefaultLabel) {
			remove();
			font.dispose();
		}
		loadFinished();
	}
	
	public abstract void loadFinished();
}
