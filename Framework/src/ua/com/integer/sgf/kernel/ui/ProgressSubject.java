package ua.com.integer.sgf.kernel.ui;

public interface ProgressSubject {
	public void addProgressListener(ProgressListener listener);
	public void removeProgressListener(ProgressListener listener);
	public void notifyProgressListeners();
}
