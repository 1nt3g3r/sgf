package ua.com.integer.sgf.kernel.ui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Array;

public class ProgressBar extends Actor implements ProgressSubject {
	private Array<ProgressListener> progressListeners;
	
	private TextureRegion background;
	private TextureRegion foreground;
	private Texture foregroundTexture;
	private float percent = -1;
	
	private int maxValue, currentValue;
	
	private boolean needDrawProgressText;
	private BitmapFont progressFont;
	private float textX, textY;
	private String progressText;

	private boolean dragable = false;
	
	class DragMouseListener extends InputListener {
		@Override
		public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
			if (!dragable) {
				return false;
			}
			setProgress(x/getWidth());
			return true;
		}
		
		@Override
		public void touchDragged(InputEvent event, float x, float y, int pointer) {
			setProgress(x/getWidth());
		}
	}
	
	public ProgressBar() {
		setProgress(0);
		progressListeners = new Array<ProgressListener>();
		addListener(new DragMouseListener());
	}
	
	public void setDragable(boolean dragable) {
		this.dragable = dragable;
	}

	public void setForeground(TextureRegion foreground) {
		this.foreground = foreground;
		foregroundTexture = foreground.getTexture();
	}

	public void setBackground(TextureRegion background) {
		this.background = background;
	}
	
	public void setProgressFont(BitmapFont progressFont) {
		this.progressFont = progressFont;
		updateText();
	}
	
	public void needShowText(boolean needShowText) {
		this.needDrawProgressText = needShowText;
		updateText();
	}
	
	public void setTextFont(BitmapFont progressFont) {
		this.progressFont = progressFont;
		updateText();
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		batch.draw(background, getX(), getY(), getWidth(), getHeight());
		batch.draw(foregroundTexture, getX(), getY(),
				getWidth() * percent, getHeight(),
				foreground.getRegionX(), foreground.getRegionY(),
				(int) (foreground.getRegionWidth() * percent),
				foreground.getRegionHeight(), false, false);
		
		if (needDrawProgressText) {
			drawProgressText(batch);
		}
	}
	
	private void drawProgressText(SpriteBatch batch) {
		TextBounds textBounds = progressFont.getBounds(progressText);
		float textWidth = textBounds.height;
		float textHeight = textBounds.height;
		
		textX = (getWidth() - textWidth)/2 + getX();
		textY = (getHeight() - textHeight)/2 + getY() + textHeight;
		
		progressFont.draw(batch, progressText, textX, textY);
	}

	public void setProgress(float percent) {
		if (percent < 0) {
			percent = 0;
		}
		if (percent > 1) {
			percent = 1;
		}
		currentValue = (int) (maxValue*percent);
		if (this.percent != percent && needDrawProgressText) {
			this.percent = percent;
			notifyProgressListeners();
			updateText();
		} else {
			this.percent = percent;
		}
		
	}
	
	private void updateText() {
		progressText = currentValue+"";
	}
	
	public float getProgress() {
		return percent;
	}
	
	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}
	
	public void setCurrentValue(int currentValue) {
		this.currentValue = currentValue;
		setProgress((float) currentValue / (float) maxValue);
	}
	
	public int getCurrentValue() {
		return currentValue;
	}

	@Override
	public void addProgressListener(ProgressListener listener) {
		progressListeners.add(listener);
	}

	@Override
	public void removeProgressListener(ProgressListener listener) {
		progressListeners.removeValue(listener, true);
	}

	@Override
	public void notifyProgressListeners() {
		for(ProgressListener l : progressListeners) {
			l.progressChanged(maxValue, currentValue, percent);
		}
	}
};
