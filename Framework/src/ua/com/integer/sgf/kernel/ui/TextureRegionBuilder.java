package ua.com.integer.sgf.kernel.ui;

import ua.com.integer.sgf.kernel.GameKernel;
import ua.com.integer.sgf.resource.TextureManager;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Pools;

public class TextureRegionBuilder {
	private TextureManager tm;
	private TextureRegion region;
	private float x, y;
	private float width, height;
	
	public TextureRegionBuilder kernel(GameKernel kernel) {
		tm = kernel.res().textures();
		return this;
	}
	
	public TextureRegionBuilder region(String pack, String region) {
		this.region = tm.get(pack, region);
		return this;
	}
	
	public TextureRegionBuilder size(float width, float height) {
		this.width = width;
		this.height = height;
		return this;
	}
	
	public TextureRegionBuilder position(float x, float y) {
		this.x = x;
		this.y = y;
		return this;
	}
	
	public Actor result() {
		TextureRegionActor actor = new TextureRegionActor();
		actor.setRegion(region);
		actor.setPosition(x, y);
		actor.setSize(width, height);
		actor.setOrigin(width/2, height/2);
		
		Pools.free(this);
		
		return actor;
	}
	
	public class TextureRegionActor extends Actor {
		private TextureRegion region;
		private Color color;
		
		public void setRegion(TextureRegion region) {
			this.region = region;
		}
		
		@Override
		public void draw(SpriteBatch batch, float parentAlpha) {
			color = batch.getColor();
			
			batch.setColor(getColor());
			batch.draw(region, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
		
			batch.setColor(color);
		}
	}
	
	
}
