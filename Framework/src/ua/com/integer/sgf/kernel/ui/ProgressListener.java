package ua.com.integer.sgf.kernel.ui;

public interface ProgressListener {
	public void progressChanged(int maxValue, int currentValue, float percent);
}
