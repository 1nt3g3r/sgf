package ua.com.integer.sgf.kernel.ui;

import ua.com.integer.sgf.kernel.GameKernel;
import ua.com.integer.sgf.resource.TextureManager;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pools;

public class AnimationBuilder {
	private String packname;
	private String baseRegionName;
	private int count;
	private float frameDuration;
	private boolean loop = true;
	private GameKernel kernel;
	
	public AnimationBuilder kernel(GameKernel kernel) {
		this.kernel = kernel;
		return this;
	}
	
	public AnimationBuilder pack(String packname) {
		this.packname = packname;
		return this;
	}
	
	public AnimationBuilder basename(String basename) {
		this.baseRegionName = basename;
		return this;
	}
	
	public AnimationBuilder count(int count) {
		this.count = count;
		return this;
	}
	
	public AnimationBuilder frameDuration(float frameDuration) {
		this.frameDuration = frameDuration;
		return this;
	}
	
	public AnimationBuilder withLoop() {
		loop = true;
		return this;
	}
	
	public AnimationBuilder noLoop() {
		loop = false;
		return this;
	}
	
	
	public Animation result() {
		Array<TextureRegion> regions = new Array<TextureRegion>();
		TextureManager tm = kernel.res().textures();
		
		for(int i = 1; i <= count; i++) {
			regions.add(tm.get(packname, baseRegionName+"-"+i));
		}
		
		Animation result = new Animation(frameDuration, regions);
		if (loop) {
			result.setPlayMode(Animation.LOOP);
		}
		
		Pools.free(this);
		
		return result;
	}
}
