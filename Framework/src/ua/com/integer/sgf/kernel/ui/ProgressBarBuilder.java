package ua.com.integer.sgf.kernel.ui;

import ua.com.integer.sgf.fontmanager.TTFFontManager;
import ua.com.integer.sgf.kernel.GameKernel;
import ua.com.integer.sgf.resource.TextureManager;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Pools;

public class ProgressBarBuilder {
	private TextureManager tm;
	private TTFFontManager fm;
	private TextureRegion foreground;
	private TextureRegion background;
	private boolean needShowText;
	private BitmapFont textFont;
	private float width, height;
	private boolean dragable;
	
	public ProgressBarBuilder kernel(GameKernel kernel) {
		return setFontManager(kernel.res().fonts())
				.setTextureManager(kernel.res().textures());
	}

	public ProgressBarBuilder setFontManager(TTFFontManager fm) {
		this.fm = fm;
		return this;
	}
	
	public ProgressBarBuilder setTextureManager(TextureManager tm) {
		this.tm = tm;
		return this;
	}
	
	public ProgressBarBuilder foreground(String pack, String region) {
		foreground = tm.get(pack, region);
		return this;
	}
	
	public ProgressBarBuilder background(String pack, String region) {
		background = tm.get(pack, region);
		return this;
	}
	
	public ProgressBarBuilder showText(boolean needShowText) {
		this.needShowText = needShowText;
		return this;
	}
	
	public ProgressBarBuilder font(String font, int size) {
		textFont = fm.getFont(font, size);
		return this;
	}
	
	public ProgressBarBuilder size(float width, float height) {
		this.width = width;
		this.height = height;
		return this;
	}
	
	public ProgressBarBuilder dragable(boolean dragable) {
		this.dragable = dragable;
		return this;
	}
	
	public ProgressBar result() {
		ProgressBar toReturn = new ProgressBar();
		toReturn.setBackground(background);
		toReturn.setForeground(foreground);
		toReturn.needShowText(needShowText);
		toReturn.setProgressFont(textFont);
		toReturn.setSize(width, height);
		toReturn.setDragable(dragable);
		
		dragable = false;
		needShowText = false;
		
		Pools.free(this);
		
		return toReturn;
	}
	
}
