package ua.com.integer.sgf.kernel.ui;

import ua.com.integer.sgf.fontmanager.TTFFontManager;
import ua.com.integer.sgf.kernel.GameKernel;
import ua.com.integer.sgf.localize.Localize;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.Pools;

public class LabelBuilder {
	private Localize lc;
	private TTFFontManager fm;
	private String text;
	private BitmapFont font;
	private boolean needLocalize;
	
	public LabelBuilder kernel(GameKernel kernel) {
		lc = kernel.lc();
		fm = kernel.res().fonts();
		return this;
	}
	
	public LabelBuilder text(String text) {
		this.text = text;
		return this;
	}
	
	public LabelBuilder font(String name, int size) {
		font = fm.getFont(name, size);
		return this;
	}
	
	public LabelBuilder localize(boolean localize) {
		needLocalize = localize;
		return this;
	}
	
	public Label result() {
		LabelStyle style = new LabelStyle();
		style.font = font;

		if (needLocalize) {
			text = lc.translate(text);
		}
		
		Label toReturn = new Label(text, style);
		Pools.free(this);
		return toReturn;
	}
}
