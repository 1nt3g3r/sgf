package ua.com.integer.sgf.kernel;

import javax.swing.JDialog;
import javax.swing.SwingUtilities;

import ua.com.integer.sgf.GlobalSettings;
import ua.com.integer.sgf.action.ActionManager;
import ua.com.integer.sgf.actor.GameActor;
import ua.com.integer.sgf.actor.simple.ActorManager;
import ua.com.integer.sgf.editor.ControlFrame;
import ua.com.integer.sgf.fontmanager.TTFFontManager;
import ua.com.integer.sgf.localize.Localize;
import ua.com.integer.sgf.ncm.ClientCommandManager;
import ua.com.integer.sgf.resource.ResourceManager;
import ua.com.integer.sgf.resource.ResourceManagerConfig;
import ua.com.integer.sgf.screen.manager.ScreenManager;
import ua.com.integer.sgf.sound.manager.MusicManager;
import ua.com.integer.sgf.sound.manager.SoundManager;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public abstract class GameKernel extends Game {
	private ResourceManager resourceManager;
	private Resources resources;
	private ClientCommandManager networkCommandManager;
	private Localize localize;
	private static GameKernel lastGameKernel;

	protected String baseDirectory;
	protected GameConfig gameConfig;
	protected boolean created;
	
	public GameKernel(String baseDirectory) {
		this.baseDirectory = baseDirectory;
	}
	
	public GameKernel(String baseDirectory, boolean debug) {
		this(baseDirectory);
		if (debug) {
			debug();
		}
	}
	
	public void debug() {
		GlobalSettings.debug();
	}
	
	/**
	 * @return is game created - {@link #create()} method was called in this case. 
	 * You should manually set {@link #created} variable to true value
	 */
	public boolean isCreated() {
		return created;
	}
	
	@Override
	public void create() {
		gameConfig = GameConfig.loadFromFile(baseDirectory + "/gameConfig");

		ResourceManagerConfig resourceConfig = new ResourceManagerConfig();
		resourceConfig.imagesDirectory = gameConfig.imageDirectory;
		resourceManager = new ResourceManager(resourceConfig);
		
		resources = new Resources(resourceManager);
		
		networkCommandManager = new ClientCommandManager();
		localize = new Localize();
		localize.setDirectory(gameConfig.localizeDirectory);
		
		initManagers();
		
		res().screens().setGame(this);
		
		GameActor.setResourceManager(rm());
		GameActor.setLocalize(lc());
		
		created();
		if (GlobalSettings.needDebug()) {
			Gdx.app.setLogLevel(Application.LOG_DEBUG);
		}
	}
	
	private void initManagers() {
		rm().addManager(ActorManager.class);
		rm().getManager(ActorManager.class).setActorDirectory(gameConfig.actorDirectory);
		
		rm().addManager(TTFFontManager.class);
		rm().getManager(TTFFontManager.class).setFontDirectory(gameConfig.ttfFontsDirectory);
		
		rm().addManager(ActionManager.class);
		rm().getManager(ActionManager.class).setActionDirectory(gameConfig.actionDirectory);
		
		rm().addManager(SoundManager.class);
		rm().getManager(SoundManager.class).setSoundDirectory(gameConfig.soundDirectory);
		
		rm().addManager(MusicManager.class);
		rm().getManager(MusicManager.class).setMusicDirectory(gameConfig.musicDirectory);
		
		rm().addManager(ScreenManager.class);
	}
	
	/**
	 * This method calls after initialize all game engine parts. 
	 * Write your init code here.
	 */
	public abstract void created();
	
	/**
	 * @return {@link #resourceManager} instance
	 */
	public ResourceManager rm() {
		return resourceManager;
	}
	
	/**
	 * @return #{@link ClientCommandManager} instance
	 */
	public ClientCommandManager network() {
		return networkCommandManager;
	}
	
	/**
	 * @return #{@link Localize} instance
	 */
	public Localize lc() {
		return localize;
	}
	
	/**
	 * Shows editor control panel. You can edit 
	 * game configuration, actors, action, etc. in this panel.
	 */
	public static void showControlPanel() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				ControlFrame dialog = new ControlFrame();
				dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				dialog.setVisible(true);
			}
		});
	}
	
	/**
	 * Shows dialog where you choose - run game or game editor.
	 */
	public static void showDevelopmentDialog(GameKernel gameKernel) {
		if (gameKernel == null) {
			throw new IllegalArgumentException("Kernel can't be null!");
		}
		lastGameKernel = gameKernel;
		showControlPanel();
	}
	
	public static void startDesktopGame(final LwjglApplicationConfiguration config) {
		new LwjglApplication(lastGameKernel, config);
	}
	
	@Override
	public void dispose() {
		rm().dispose();
		network().close();
	}
	
	public Resources res() {
		return resources;
	}
}
