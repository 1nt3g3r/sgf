package ua.com.integer.sgf.kernel;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Json;

public class GameConfig {
	public String imageDirectory;
	public String musicDirectory;
	public String soundDirectory;
	public String localizeDirectory;
	public String actionDirectory;
	public String actorDirectory;
	public String ttfFontsDirectory;
	
	public static GameConfig loadFromFile(String filename) {
		GameConfig config = new Json().fromJson(GameConfig.class, Gdx.files.internal(filename));
		return config;
	}
	
	public void saveToFile(String filename) {
		new Json().toJson(this, Gdx.files.internal(filename));
	}
}
