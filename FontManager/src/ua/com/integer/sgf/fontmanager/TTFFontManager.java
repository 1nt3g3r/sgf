package ua.com.integer.sgf.fontmanager;

import java.util.HashMap;
import java.util.Map;

import ua.com.integer.sgf.GlobalSettings;
import ua.com.integer.sgf.resource.LoadManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Manager for fonts. Allows to generate bitmap fonts from .ttf files. 
 * Implements "lazy" loading - if font wasn't loaded, it will try load it 
 * on the fly.
 * 
 * @author integer
 */
public class TTFFontManager implements LoadManager {
	public static final String DEFAULT_CHARS = "АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯяabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789][_!$%#@|\\/?-+=()*&.;:,{}\"´`'<>’";
	
	private String charsToGenerate;
	private String fontDirectory;
	
	private ObjectMap<String, Map<Integer, BitmapFont>> fonts;
	private ObjectMap<String, FreeTypeFontGenerator> fGenerators;
	
	private Array<String> loadFontQueue;
	private int totalFontCount;
	
	private boolean debug;

	
	public TTFFontManager() {
		this(DEFAULT_CHARS);
	}
	
	public TTFFontManager(String charsToGenerate) {
		this.charsToGenerate = charsToGenerate;
		fonts = new ObjectMap<String, Map<Integer, BitmapFont>>();
		fGenerators = new ObjectMap<String, FreeTypeFontGenerator>();
	}
	
	public void setFontChars(String charsToGenerate) {
		this.charsToGenerate = charsToGenerate;
	}
	
	public void setFontDirectory(String fontDirectory) {
		this.fontDirectory = fontDirectory;
		debug = GlobalSettings.needDebug();
	}
	
	/**
	 * Returns the font specified size. If necessary, the font is created.
	 * @param size font size
	 * @return
	 */
	public BitmapFont getFont(String fontName, int size) {
		createFontGeneratorIfNeed(fontName);
		createFontMapIfNeed(fontName, size);
		createFontIfNeed(fontName, size);
		
		return fonts.get(fontName).get(size);
	}
	
	private void createFontGeneratorIfNeed(String fontName) {
		try {
			if (fGenerators.get(fontName) == null) {
				FreeTypeFontGenerator fGen = new FreeTypeFontGenerator(Gdx.files.internal(fontDirectory +"/" + fontName + ".ttf"));
				fGenerators.put(fontName, fGen);
				if (debug) {
					Gdx.app.debug(getClass().toString(), "Font generator for " + fontName + " created.");
				}
			}
		} catch (GdxRuntimeException ex) {
			Gdx.app.error(getClass().toString(), "Can't load font " + fontName + ex);
			System.exit(0);
		}
	}
	
	private void createFontMapIfNeed(String fontName, int fontSize) {
		if (fonts.get(fontName) == null) {
			fonts.put(fontName, new HashMap<Integer, BitmapFont>());
		}
	}
	
	private void createFontIfNeed(String fontName, int size) {
		if (fonts.get(fontName).get(size) == null) {
			BitmapFont font = fGenerators.get(fontName).generateFont(size, charsToGenerate, false);
			fonts.get(fontName).put(size, font);
			if (debug) {
				Gdx.app.debug(getClass().toString(), "Font " + fontName + " loaded.");
			}
		}
	}
	
	/**
	 * Dispose fonts and font generators.
	 */
	public void dispose() {
		disposeFonts();
		disposeFontGenerators();
		if (debug) {
			Gdx.app.debug(getClass().toString(), "Font generators and fonts destroyed.");
		}
	}
	
	private void disposeFonts() {
		for(Map<Integer, BitmapFont> fontMap : fonts.values()) {
			for(BitmapFont font : fontMap.values()) {
				font.dispose();
			}
			fontMap.clear();
		}
	}
	
	private void disposeFontGenerators() {
		for(FreeTypeFontGenerator fGen : fGenerators.values()) {
			fGen.dispose();
		}
		fGenerators.clear();
	}
	
	/**
	 * Reinit all fonts in font cache. It may be useful 
	 * if your game was closed and opened (on android, as example).
	 */
	public void reload() {
		disposeFonts();
		
		for(String fontName : fonts.keys()) {
			Map<Integer, BitmapFont> tmpFonts = fonts.get(fontName);
			FreeTypeFontGenerator fGen = fGenerators.get(fontName);
			for(int fontSize : tmpFonts.keySet()) {
				tmpFonts.put(fontSize, fGen.generateFont(fontSize, DEFAULT_CHARS, false));
			}
		}
	}

	/**
	 * This method loads TTF Generators only - but don't creaty any fonts. 
	 * Only after getFont(name, size) method font with selected name and size will be created 
	 * and added to the font cache.
	 */
	@Override
	public void loadAll() {
		if (fontDirectory == null) {
			Gdx.app.error(getClass().toString(), "Font directory isn't selected! Interrupt loading fonts");
			return;
		}
		clearLoadQueue();
		fillLoadQueue();
	}
	
	private void clearLoadQueue() {
		if (loadFontQueue == null) {
			loadFontQueue = new Array<String>();
		} else {
			loadFontQueue.clear();
		}
	}
	
	private void fillLoadQueue() {
		for(FileHandle fontHandle : Gdx.files.internal(fontDirectory).list()) {
			if (!fontHandle.isDirectory() && fontHandle.extension().equals("ttf")) {
				loadFontQueue.add(fontHandle.nameWithoutExtension());
			}
		}
		totalFontCount = loadFontQueue.size;
	}

	@Override
	public boolean loadStep() {
		if (loadFontQueue.size > 0) {
			createFontGeneratorIfNeed(loadFontQueue.pop());
			return false;
		} else {
			Gdx.app.debug(getClass().toString(), "All font generators created.");
			return true;
		}
	}

	@Override
	public float getLoadPercent() {
		if (loadFontQueue == null || loadFontQueue.size == 0) {
			return 1;
		} else {
			return (float) fGenerators.size / (float) totalFontCount;
		}
	}
	
	public String[] getFontNames() {
		String[] fontNames = new String[fGenerators.size];
		
		int fontIndex = -1;
		for(String fontName : fGenerators.keys()) {
			fontNames[++fontIndex] = fontName;
		}
		
		return fontNames;
	}

	@Override
	public String getLoadDescription() {
		return "Fonts";
	}
}
