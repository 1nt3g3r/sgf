package ua.com.integer.sgf.localize;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import ua.com.integer.sgf.JsonWorker;

import com.badlogic.gdx.utils.Array;

public class Book {
	public Array<Word> words = new Array<Word>();

	public String translate(String toTranslate) {
		for(Word word : words) {
			if (word.word.equals(toTranslate)) {
				return word.translatedWord;
			}
		}
		return null;
	}
	
	public void saveToFile(File file) throws IOException {
		words.shrink();
		String jsonBook = JsonWorker.GSON.toJson(this);
		
		FileWriter writer = new FileWriter(file);
		writer.write(jsonBook);
		writer.flush();
		writer.close();
	}
}
