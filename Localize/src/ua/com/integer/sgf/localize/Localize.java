package ua.com.integer.sgf.localize;

import java.util.HashMap;
import java.util.Map;

import ua.com.integer.sgf.JsonWorker;

import com.badlogic.gdx.Gdx;

public class Localize {
	private String localizeDirectory;
	
	private Map<String, Book> data;
	private String currentLanguage;
	
	public Localize() {
		data = new HashMap<String, Book>();
	}
	
	public Localize(String localizeDirectory) {
		this();
		setDirectory(localizeDirectory);
	}
	
	public void setDirectory(String directory) {
		this.localizeDirectory = directory;
		data.clear();
	}
	
	public void loadLanguage(String language) {
		String fullName = localizeDirectory + "/" + language +".lang";
		
		if (!Gdx.files.internal(fullName).exists()) {
			Gdx.app.error(Localize.class +"", "Language " + language + " not found!");
			return;
		}

		Book book = JsonWorker.GSON.fromJson(Gdx.files.internal(fullName).readString(), Book.class);
		data.put(language, book);
	}
	
	public void setCurrentLanguage(String currenLanguage) {
		this.currentLanguage = currenLanguage;
		if (data.get(currenLanguage) == null) {
			loadLanguage(currenLanguage);
		}
	}
	
	public String getCurrentLanguage() {
		return currentLanguage;
	}
	
	public String translate(String toTranslate) {
		if (currentLanguage == null) {
			return toTranslate;
		}
		
		Book langMap = data.get(currentLanguage);
		if (langMap == null) {
			return toTranslate;
		}
		
		String toReturn = langMap.translate(toTranslate);
		if (toReturn == null) {
			return toTranslate;
		} else {
			return toReturn;
		}
	}
	
	public static void main(String[] args) {
		Localize lc = new Localize("./");
		lc.setCurrentLanguage("en");
	}
}
