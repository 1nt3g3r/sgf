package ua.com.integer.sgf.action.descriptor;

import ua.com.integer.sgf.action.ActionType;

public class ScaleByDescriptor extends ActionDescriptor {
	public ScaleByDescriptor() {
	}
	
	public ScaleByDescriptor(float scaleByX, float scaleByY, float time) {
		params = scaleByX + ";" + scaleByY + ";" + time;
		type = ActionType.SCALE_BY;
	}
}
