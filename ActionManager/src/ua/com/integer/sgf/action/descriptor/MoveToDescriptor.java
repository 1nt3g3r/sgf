package ua.com.integer.sgf.action.descriptor;

import ua.com.integer.sgf.action.ActionType;
import ua.com.integer.sgf.dimension.Dimension;


public class MoveToDescriptor extends ActionDescriptor {
	public MoveToDescriptor() {
	}
	
	public MoveToDescriptor(Dimension x, Dimension y, float time) {
		type = ActionType.MOVE_TO;
		params = x + ";" + y + ";" + time;
	}
}
