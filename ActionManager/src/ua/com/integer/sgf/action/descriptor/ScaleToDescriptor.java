package ua.com.integer.sgf.action.descriptor;

import ua.com.integer.sgf.action.ActionType;

public class ScaleToDescriptor extends ActionDescriptor {
	public ScaleToDescriptor() {
	}
	
	public ScaleToDescriptor(float scaleToX, float scaleToY, float time) {
		params = scaleToX + ";" + scaleToY + ";" + time;
		type = ActionType.SCALE_TO;
	}
}
