package ua.com.integer.sgf.action.descriptor;

import ua.com.integer.sgf.action.ActionType;
import ua.com.integer.sgf.dimension.Dimension;


public class MoveByActionDescriptor extends ActionDescriptor {
	public MoveByActionDescriptor() {
	}
	
	public MoveByActionDescriptor(Dimension dx, Dimension dy, float time) {
		params = dx + ";" + dy + ";" + time;
		type = ActionType.MOVE_BY;
	}
}
