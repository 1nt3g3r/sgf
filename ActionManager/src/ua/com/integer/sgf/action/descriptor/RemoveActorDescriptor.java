package ua.com.integer.sgf.action.descriptor;

import ua.com.integer.sgf.action.ActionType;

public class RemoveActorDescriptor extends ActionDescriptor {
	public RemoveActorDescriptor() {
		type = ActionType.REMOVE_ACTOR;
	}
}
