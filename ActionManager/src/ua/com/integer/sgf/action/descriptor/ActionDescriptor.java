package ua.com.integer.sgf.action.descriptor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import ua.com.integer.sgf.JsonWorker;
import ua.com.integer.sgf.action.ActionType;
import ua.com.integer.sgf.action.creator.ActionCreator;
import ua.com.integer.sgf.dimension.Dimension;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.utils.Array;

public class ActionDescriptor {
	public ActionType type;
	public String params;
	public Array<ActionDescriptor> children = new Array<ActionDescriptor>();
	
	public Action getAction(){
		return ActionCreator.createAction(this);
	};
	
	public void addChild(ActionDescriptor child) {
		children.add(child);
	}
	
	//TODO не создавать каждый раз пачку gson
	public static String toJson(ActionDescriptor descriptor) {
		descriptor.children.shrink();
		return JsonWorker.GSON.toJson(descriptor);
	}
	
	public static ActionDescriptor fromJson(String json) {
		return JsonWorker.GSON.fromJson(json, ActionDescriptor.class);
	}
	
	public static ActionDescriptor fromJsonFile(FileHandle file) {
		return JsonWorker.GSON.fromJson(file.readString(), ActionDescriptor.class);
	}
	
	public static void saveToFile(File file, ActionDescriptor descriptor) {
		try {
			FileWriter writer = new FileWriter(file);
			writer.write(toJson(descriptor));
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//TODO не создавать каждый раз пачку строк - оптимизация
	public Dimension dParam(int position) {
		String[] parts = params.split(";");
		return new Dimension(parts[position]);
	}
	
	public float fParam(int position) {
		String[] parts = params.split(";");
		return Float.parseFloat(parts[position]);
	}
	
	public int iParam(int position) {
		String[] parts = params.split(";");
		return Integer.parseInt(parts[position]);
	}
	
	@Override
	public String toString() {
		return type + ", " + params;
	}
}
