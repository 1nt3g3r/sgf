package ua.com.integer.sgf.action.descriptor;

import ua.com.integer.sgf.action.ActionType;

public class RotateByActionDescriptor extends ActionDescriptor {
	public RotateByActionDescriptor() {
	}
	
	public RotateByActionDescriptor(float degrees, float time) {
		params = degrees + ";" + time;
		type = ActionType.ROTATE_BY;
	}
}
