package ua.com.integer.sgf.action.descriptor;

import ua.com.integer.sgf.action.ActionType;

public class ParallelActionDescriptor extends ActionDescriptor {
	public ParallelActionDescriptor() {
		params = "-1";
		type = ActionType.PARALLEL;
	}
	
	public ParallelActionDescriptor(ActionDescriptor ... descriptors) {
		type = ActionType.PARALLEL;
		for(ActionDescriptor descriptor : descriptors) {
			children.add(descriptor);
		}
	}
}
