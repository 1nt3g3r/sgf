package ua.com.integer.sgf.action.descriptor;

import ua.com.integer.sgf.action.ActionType;

public class AlphaDescriptor extends ActionDescriptor {
	public AlphaDescriptor() {
		this(0, 0);
	}
	
	public AlphaDescriptor(float alpha, float time) {
		type = ActionType.ALPHA;
		params = alpha + ";" + time;
	}
}
