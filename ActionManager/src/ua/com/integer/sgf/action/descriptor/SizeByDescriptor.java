package ua.com.integer.sgf.action.descriptor;

import ua.com.integer.sgf.action.ActionType;
import ua.com.integer.sgf.dimension.Dimension;

public class SizeByDescriptor extends ActionDescriptor {
	public SizeByDescriptor() {
	}
	
	public SizeByDescriptor(Dimension sizeByX, Dimension sizeByY, float time) {
		params = sizeByX + ";" + sizeByY + ";" + time;
		type = ActionType.SIZE_BY;
	}
}
