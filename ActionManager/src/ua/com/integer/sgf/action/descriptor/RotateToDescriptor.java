package ua.com.integer.sgf.action.descriptor;

import ua.com.integer.sgf.action.ActionType;

public class RotateToDescriptor extends ActionDescriptor {
	public RotateToDescriptor() {
	}
	
	public RotateToDescriptor(float angle, float time) {
		params = angle + ";" + time;
		type = ActionType.ROTATE_TO;
	}

}
