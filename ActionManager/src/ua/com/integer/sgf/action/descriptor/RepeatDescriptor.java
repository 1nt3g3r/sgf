package ua.com.integer.sgf.action.descriptor;

import ua.com.integer.sgf.action.ActionType;

public class RepeatDescriptor extends ActionDescriptor {
	public RepeatDescriptor() {
		type = ActionType.REPEAT;
	}
	
	public RepeatDescriptor(ActionDescriptor descriptor, int count) {
		type = ActionType.REPEAT;
		children.add(descriptor);
		params = count + "";
	}
}
