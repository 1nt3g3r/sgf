package ua.com.integer.sgf.action.descriptor;

import ua.com.integer.sgf.action.ActionType;
import ua.com.integer.sgf.dimension.Dimension;

public class SizeToDescriptor extends ActionDescriptor {
	public SizeToDescriptor(Dimension width, Dimension height, float time) {
		type = ActionType.SIZE_TO;
		params = width + ";" + height + ";" + time;
	}
}
