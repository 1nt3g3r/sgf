package ua.com.integer.sgf.action.creator;

import java.util.HashMap;
import java.util.Map;

import ua.com.integer.sgf.action.ActionType;
import ua.com.integer.sgf.action.descriptor.ActionDescriptor;

import com.badlogic.gdx.scenes.scene2d.Action;

public abstract class ActionCreator {
	private static Map<ActionType, ActionCreator> creators;
	static {
		creators = new HashMap<ActionType, ActionCreator>();
		creators.put(ActionType.DELAY, new DelayCreator());
		creators.put(ActionType.REPEAT, new RepeatCreator());
		creators.put(ActionType.PARALLEL, new ParallelCreator());
		creators.put(ActionType.SEQUENCE, new SequenceCreator());
		creators.put(ActionType.REMOVE_ACTOR, new RemoveActorCreator());
		creators.put(ActionType.ALPHA, new AlphaCreator());
		creators.put(ActionType.MOVE_TO, new MoveToCreator());
		creators.put(ActionType.MOVE_BY, new MoveByCreator());
		creators.put(ActionType.ROTATE_BY, new RotateByCreator());
		creators.put(ActionType.SCALE_BY, new ScaleByCreator());
		creators.put(ActionType.SIZE_BY, new SizeByCreator());
		creators.put(ActionType.ROTATE_TO, new RotateToCreator());
		creators.put(ActionType.SCALE_TO, new ScaleToCreator());
		creators.put(ActionType.SIZE_TO, new SizeToCreator());
	}
	
	protected abstract Action getAction(ActionDescriptor descriptor);
	
	public static Action createAction(ActionDescriptor descriptor) {
		ActionCreator creator = creators.get(descriptor.type);
		if (creator == null) {
			throw new IllegalArgumentException("No creator for " + descriptor.type + " type!");
		} else {
			return creator.getAction(descriptor);
		}
	}
}
