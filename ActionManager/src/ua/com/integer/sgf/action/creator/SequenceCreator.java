package ua.com.integer.sgf.action.creator;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

public class SequenceCreator extends ActionCreator {
	@Override
	protected Action getAction(ActionDescriptor descriptor) {
		SequenceAction toReturn = Actions.sequence();
		for(ActionDescriptor child : descriptor.children) {
			toReturn.addAction(child.getAction());
		}
		return toReturn;
	}
}
