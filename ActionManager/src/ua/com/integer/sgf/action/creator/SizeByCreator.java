package ua.com.integer.sgf.action.creator;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.dimension.Dimension;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class SizeByCreator extends ActionCreator {

	@Override
	protected Action getAction(ActionDescriptor descriptor) {
		String[] parts = descriptor.params.split(";");
		float sizeDx = Dimension.getValue(parts[0]);
		float sizeDy = Dimension.getValue(parts[1]);
		float time = Float.parseFloat(parts[2]);
		
		return Actions.sizeBy(sizeDx, sizeDy, time);
	}

}
