package ua.com.integer.sgf.action.creator;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.dimension.Dimension;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class MoveByCreator extends ActionCreator {
	@Override
	public Action getAction(ActionDescriptor descriptor) {
		String[] parts = descriptor.params.split(";");
		float dx = Dimension.getValue(parts[0]);
		float dy = Dimension.getValue(parts[1]);
		float time = Float.parseFloat(parts[2]);
		
		Action moveAction = Actions.moveBy(dx, dy, time);
		return moveAction;
	}

}
