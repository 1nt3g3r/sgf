package ua.com.integer.sgf.action.creator;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class RotateToCreator extends ActionCreator {

	@Override
	protected Action getAction(ActionDescriptor descriptor) {
		String[] cmdParts = descriptor.params.split(";");
		float angle = Float.parseFloat(cmdParts[0]);
		float time = Float.parseFloat(cmdParts[1]);
		return Actions.rotateTo(angle, time);
	}

}
