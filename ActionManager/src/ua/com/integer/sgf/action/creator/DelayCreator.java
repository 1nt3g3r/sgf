package ua.com.integer.sgf.action.creator;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class DelayCreator extends ActionCreator {
	@Override
	protected Action getAction(ActionDescriptor descriptor) {
		float time = Float.parseFloat(descriptor.params);
		return Actions.delay(time);
	}
}
