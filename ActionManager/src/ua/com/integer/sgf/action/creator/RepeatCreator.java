package ua.com.integer.sgf.action.creator;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class RepeatCreator extends ActionCreator {
	@Override
	protected Action getAction(ActionDescriptor descriptor) {
		if (descriptor.children.size > 0) {
			int count = Integer.parseInt(descriptor.params);
			return Actions.repeat(count, descriptor.children.get(0).getAction());
		} else {
			return Actions.repeat(-1, Actions.sequence());
		}
	}
}
