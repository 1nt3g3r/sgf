package ua.com.integer.sgf.action.creator;


import ua.com.integer.sgf.action.descriptor.ActionDescriptor;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;

public class ParallelCreator extends ActionCreator {
	@Override
	protected Action getAction(ActionDescriptor descriptor) {
		ParallelAction toReturn = Actions.parallel();
		for(ActionDescriptor child : descriptor.children) {
			toReturn.addAction(child.getAction());
		}
		return toReturn;
	}
}
