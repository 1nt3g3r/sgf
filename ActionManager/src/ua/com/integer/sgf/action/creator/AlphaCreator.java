package ua.com.integer.sgf.action.creator;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class AlphaCreator extends ActionCreator {
	@Override
	protected Action getAction(ActionDescriptor descriptor) {
		String[] parts = descriptor.params.split(";");
		float alpha = Float.parseFloat(parts[0]);
		float duration = Float.parseFloat(parts[1]);
		return Actions.alpha(alpha, duration);
	}

}
