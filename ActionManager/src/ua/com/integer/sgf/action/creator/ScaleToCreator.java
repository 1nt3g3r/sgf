package ua.com.integer.sgf.action.creator;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class ScaleToCreator extends ActionCreator {
	@Override
	protected Action getAction(ActionDescriptor descriptor) {
		String[] parts = descriptor.params.split(";");
		float scaleX = Float.parseFloat(parts[0]);
		float scaleY = Float.parseFloat(parts[1]);
		float time = Float.parseFloat(parts[2]);
		return Actions.scaleTo(scaleX, scaleY, time);
	}
}
