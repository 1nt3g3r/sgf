package ua.com.integer.sgf.action.creator;

import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.dimension.Dimension;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class SizeToCreator extends ActionCreator {
	public SizeToCreator() {
	}
	
	@Override
	protected Action getAction(ActionDescriptor descriptor) {
		String[] parts = descriptor.params.split(";");
		float width = Dimension.getValue(parts[0]);
		float height = Dimension.getValue(parts[1]);
		float time = Float.parseFloat(parts[2]);
		return Actions.sizeTo(width, height, time);
	}
}
