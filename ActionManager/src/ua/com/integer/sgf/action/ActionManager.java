package ua.com.integer.sgf.action;

import java.util.HashMap;
import java.util.Map;

import ua.com.integer.sgf.GlobalSettings;
import ua.com.integer.sgf.action.descriptor.ActionDescriptor;
import ua.com.integer.sgf.resource.LoadManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.utils.Array;

/**
 * Manager for working with #{@link Action}. 
 * Actions saves in ".json" files with ".action" extension. You 
 * can get some action by call getAction(String name) method. Name of 
 * action is the same as filename which contains this action. Implements "lazy loading" - 
 * if some action isn't loaded, manager try to load it "on the fly".
 * 
 * @author 1nt3g3r
 */
public class ActionManager implements LoadManager {
	private Map<String, ActionDescriptor> descriptors;
	private String actionDirectory;
	private Array<String> toLoad;
	private int totalCount;
	
	private boolean debug;
	
	public ActionManager() {
		toLoad = new Array<String>();
	}
	
	/**
	 * Sets directory which contains ".action" files.
	 */
	public void setActionDirectory(String actionDirectory) {
		this.actionDirectory = actionDirectory;
		descriptors = new HashMap<String, ActionDescriptor>();
		debug = GlobalSettings.needDebug();
	}
	
	public void loadAll() {
		if (actionDirectory == null || !Gdx.files.internal(actionDirectory).exists()) {
			Gdx.app.error(getClass()+"", "Action directory isn't selected or doesn't exists!");
			return;
		}
		
		for(FileHandle file : Gdx.files.internal(actionDirectory).list()) {
			if (!file.isDirectory() && file.extension().equals("action")) {
				toLoad.add(file.nameWithoutExtension());
			}
		}
		totalCount = toLoad.size;
	}
	
	public boolean loadStep() {
		if (toLoad.size == 0) {
			if (debug) {
				Gdx.app.debug(ActionManager.class+"", "End loading.");
			}
			return true;
		} else {
			tryToLoadAction(toLoad.pop());
			return false;
		}
	}
	
	public float getLoadPercent() {
		if (totalCount == 0 || totalCount == descriptors.size()) {
			return 1;
		}
		
		return (float) descriptors.size() / (float) totalCount;
	}
	
	/**
	 * Returns action which contains in ".action" file. If action descriptor  
	 * is not loaded, manager try to load it. If file with action doesn't exists
	 * @param name name of action
	 * @return action
	 */
	public Action getAction(String name) {
		ActionDescriptor descriptor = descriptors.get(name);
		if (descriptor == null) {
			tryToLoadAction(name);
			descriptor = descriptors.get(name);
		}
		return descriptor.getAction();
	}
	
	private void tryToLoadAction(String name) {
		FileHandle actionFile = Gdx.files.internal(actionDirectory + "/" + name + ".action");
		if (actionFile.exists()) {
			descriptors.put(name, ActionDescriptor.fromJsonFile(actionFile));
			if (debug) {
				Gdx.app.debug(ActionManager.class+"", "Actor " + actionFile.name() + " loaded.");
			}
		} else {
			Gdx.app.error("Action manager", "Error during loading action " + name);
			System.exit(0);
		}
	}

	@Override
	public String getLoadDescription() {
		return "Actions";
	}
	
	@Override
	public void dispose() {}
	
	public String[] getActionNames() {
		String[] toReturn = new String[descriptors.size()];
		
		int actionIndex = -1;
		for(String actionName : descriptors.keySet()) {
			toReturn[++actionIndex] = actionName;
		}
		
		return toReturn;
	}
}
