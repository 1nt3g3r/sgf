package ua.com.integer.sgf.action;

public enum ActionType {
	DELAY,
	REPEAT,
	PARALLEL,
	SEQUENCE,
	REMOVE_ACTOR,
	ALPHA,
	MOVE_TO,
	MOVE_BY,
	ROTATE_BY,
	SCALE_BY,
	SIZE_BY,
	SIZE_TO,
	ROTATE_TO,
	SCALE_TO,
}
