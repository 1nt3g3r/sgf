package ua.com.integer.sgf.resource;

import com.badlogic.gdx.utils.Disposable;

/**
 * Abstract interface for resource managers. All 
 * managers should implements this interface for correct work
 * in #{ResourseManager} class
 * 
 * @author 1nt3g3r
 */
public interface LoadManager extends Disposable {
	/**
	 * Adds all resources in this manager to the load queque, 
	 * but don't load them.
	 */
	public void loadAll();
	/**
	 * Load next resource part from load queue.
	 * If this method returns true it means all 
	 * resources were loaded.
	 */
	public boolean loadStep();
	/**
	 * Returns float value between 0..1. 1 means that 
	 * all resources were loaded.
	 */
	public float getLoadPercent();
	/**
	 * Returns description of item which loading now - as example - "Images..."
	 * @return
	 */
	public String getLoadDescription();
}
