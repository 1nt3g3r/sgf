package ua.com.integer.sgf.resource;

import ua.com.integer.sgf.GlobalSettings;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;

/**
 * TextureManager which loads textures from ".pack" files. You 
 * can create these files using GdxTexturePacker - Libgdx extension. 
 * Manager implements "lazy loading" - if you try get AtlasRegion from some 
 * pack and this pack isn't loaded - manager will try to load it "on the fly".
 * 
 * @author 1nt3g3r
 */
public class TextureManager implements LoadManager {
	private AssetManager assets;
	private Array<String> loadedPacks;
	
	private String packDirectory;
	private boolean debug;
	
	/**
	 * Creates manager, using {@link AssetManager} assets.
	 */
	public TextureManager(AssetManager assets) {
		Texture.setAssetManager(assets);
		if (assets == null) {
			throw new IllegalArgumentException("AssetManager instance can't be null!");
		}
		
		this.assets = assets;
		loadedPacks = new Array<String>();
	}
	
	/**
	 * Selects base directory which contains image directories in format 
	 * "height"-"images". As example, for 800*480 resolution you should create 
	 * 480-images directory and put pack files into it. Manager automatically try 
	 * to set the best for current resolution directory and loads packs from it.
	 * 
	 * @param packDirectory relative path - as exampe, "data"
	 */
	public void setPackDirectory(String packDirectory) {
		this.packDirectory = packDirectory;
		debug = GlobalSettings.needDebug();
		
		if (packDirectory == null || !Gdx.files.internal(packDirectory).exists()) {
			Gdx.app.error(getClass()+"", "Image directory isn't selected or doesn't exists!");
			return;
		}
		
		Array<Integer> possibleHeights = new Array<Integer>();
		for(FileHandle handle : Gdx.files.internal(packDirectory).list()) {
			if (handle.isDirectory() && handle.name().startsWith("images-")) {
				int height = Integer.parseInt(handle.name().split("-")[1]);
				possibleHeights.add(height);
			}
		}
		possibleHeights.sort();

		if (possibleHeights.size != 0) {
			int currentHeight = possibleHeights.peek();
			for (int h : possibleHeights) {
				if (h >= currentHeight) {
					this.packDirectory += ("/images-" + h);
					break;
				}
			}
		}
		
		if (debug) {
			Gdx.app.debug("TextureManager", "Current images pack directory is " + this.packDirectory);
		}
	}
	
	/**
	 * Set necessary list of packs. Only they will be loaded - if other packs were loaded, 
	 * they will be unload
	 */
	public void setNecessaryPacks(Array<String> necessaryPacks) {
		if (packDirectory == null) { 
			throw new IllegalStateException("You should set up packDirectory before use this method");
		}
		
		for(String needPack : necessaryPacks) {
			if (!loadedPacks.contains(needPack, false)) {
				loadPack(needPack);
			}
		}
		
		for(String loadedPack : loadedPacks) {
			if (!necessaryPacks.contains(loadedPack, false)) {
				unloadPack(loadedPack);
			}
		}
	}
	
	/**
	 * Searches all files with .pack extension in current pack directory and 
	 * loades it.
	 */
	@Override
	public void loadAll() {
		if (packDirectory == null || !Gdx.files.internal(packDirectory).exists()) {
			Gdx.app.error(getClass()+"", "Image directory isn't selected or doesn't exists!");
			return;
		}
		
		for(FileHandle file : Gdx.files.internal(packDirectory).list()) {
			if (!file.isDirectory() && file.extension().equals("pack")) {
				loadPack(file.name());
			}
		}
	}
	
	/**
	 * @return list of loaded packs
	 */
	public Array<String> getLoadedPacks() {
		return loadedPacks;
	}
	
	/**
	 * Returns list of {@link AtlasRegion} for selected pack
	 * @param packName name of pack
	 */
	public Array<AtlasRegion> getRegionsFromPack(String packName) {
		if (!isPackLoaded(packName)) {
			throw new IllegalStateException("Pack " + packName + " isn't loaded!");
		}
		
		TextureAtlas atlas = assets.get(packDirectory + "/" + packName, TextureAtlas.class);
		return atlas.getRegions();
	}
	
	/**
	 * Loads pack into memory. In general, you shouldn't call this method manually
	 * @param packName name of the pack
	 */
	public void loadPack(String packName) {
		assets.load(packDirectory + "/" + packName, TextureAtlas.class);
		loadedPacks.add(packName);
	}
	
	/**
	 * Unloads pack from the memory.
	 * @param packName name of the pack
	 */
	public void unloadPack(String packName) {
		if (!assets.isLoaded(packName)) {
			assets.unload(packDirectory + "/" + packName);
			assets.finishLoading();
			loadedPacks.removeValue(packName, false);
		}
	}
	
	/**
	 * Returns {@link AtlasRegion} from selected pack. If pack wasn't loaded previously, 
	 * it will be loaded.
	 * @param packName name of the pack which contains selected region
	 * @param regionName name of the region
	 */
	public AtlasRegion get(String packName, String regionName) {
		TextureAtlas atlas = getAtlas(packName);
		AtlasRegion region = atlas.findRegion(regionName);
		
		if (region == null) {
			throw new IllegalArgumentException("Region " + regionName + " not found in pack " + packName);
		}
		
		return region;
	}
	
	public TextureAtlas getAtlas(String packname) {
		if (!isPackLoaded(packname)) {
			loadPack(packname);
			assets.finishLoading();
		}
		return assets.get(packDirectory + "/" + packname, TextureAtlas.class);
	}
	
	private boolean isPackLoaded(String packName) {
		return assets.isLoaded(packDirectory + "/" + packName);
	}

	@Override
	public boolean loadStep() {
		return assets.update();
	}

	@Override
	public float getLoadPercent() {
		return assets.getProgress();
	}


	@Override
	public String getLoadDescription() {
		return "Textures";
	}
	@Override
	public void dispose() {
		for(String pack : loadedPacks) {
			unloadPack(pack);
		}
	}
}
