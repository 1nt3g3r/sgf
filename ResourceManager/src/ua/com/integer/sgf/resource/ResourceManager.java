package ua.com.integer.sgf.resource;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

/**
 * Resource manager. Contains set of {@link LoadManager} objects. 
 * You can dynamically add new loaders by his class. The most valuable loaders implemented 
 * and have short methods to access them.
 * 
 * @author 1nt3g3r
 */
public class ResourceManager implements Disposable, LoadManager {
	private AssetManager assets;
	private ResourceManagerConfig config;
	
	private Array<LoadManager> loadManagers;
	private String currentLoadingDescription;
	
	/**
	 * Creates new manager with null config
	 */
	public ResourceManager() {
		this(null);
	}
	
	/**
	 * Creates new manager and load settings from config
	 */
	public ResourceManager(ResourceManagerConfig config) {
		assets = new AssetManager();
		loadManagers = new Array<LoadManager>();

		loadManagers.add(new TextureManager(assets));
		
		this.config = config;
		if (config != null) {
			loadFromConfig();
		}
	}
	
	private void loadFromConfig() {
		textures().setPackDirectory(config.imagesDirectory);
	}
	
	/**
	 * Return {@link TextureManager} instance;
	 */
	public TextureManager textures() {
		return getManager(TextureManager.class);
	}
	
	/**
	 * Load all resources to load query. Just call {@link #loadStep()} 
	 * after it to load it.
	 */
	public void loadAll() {
		for(LoadManager loadManager : loadManagers) {
			loadManager.loadAll();
		}
	}
	
	/**
	 * Call this method while it returns true. If 
	 * returns false it means all assets loaded.
	 * @return
	 */
	public boolean loadStep() {
		for(LoadManager loadManager : loadManagers) {
			if (loadManager.getLoadPercent() < 1) {
				loadManager.loadStep();
				currentLoadingDescription = loadManager.getLoadDescription();
				return false;
			}	
		}
		return true;
	}
	
	public float getLoadPercent() {
		float totalProgress = 0;
		for(LoadManager loadManager : loadManagers) {
			totalProgress += loadManager.getLoadPercent();
		}
		return totalProgress / (float) loadManagers.size;
	}
	
	@Override
	public void dispose() {
		for(LoadManager loadManager : loadManagers) {
			loadManager.dispose();
		}
		if (assets != null) {
			assets.dispose();
		}
	}
	
	/**
	 */
	@SuppressWarnings("unchecked")
	public <T extends LoadManager> T getManager(Class<T> type) {
		for(LoadManager loadManager : loadManagers) {
			if (loadManager.getClass() == type) {
				return (T) loadManager;
			}
		}
		throw new IllegalArgumentException("No load manager for this class!");
	}
	
	/**
	 * Add {@link LoadManager} to this resource manager by his class. You can get it by {@link #getManager(Class)} call
	 */
	public <T extends LoadManager> void addManager(Class<T> type) {
		try {
			LoadManager loadManager = type.newInstance();
			loadManagers.add(loadManager);
		} catch (Exception e) {
			Gdx.app.error("ResourceManager", "Error during loading " + type + " manager");
		}
	}

	@Override
	public String getLoadDescription() {
		return currentLoadingDescription;
	}
	
	public AssetManager getAssets() {
		return assets;
	}
}
