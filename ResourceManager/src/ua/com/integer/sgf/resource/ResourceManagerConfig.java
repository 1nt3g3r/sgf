package ua.com.integer.sgf.resource;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Json;
/**
 * Configuration class for {@link #ResourceManager}
 * 
 * @author 1nt3g3r
 */
public class ResourceManagerConfig {
	/**
	 * Path to directory which contains folder with packed images - ".pack" files. 
	 * In this directory you can place directories for different screen resolutions.
	 */
	public String imagesDirectory;
	/**
	 * Path to directory which contains ".action" files - .json files with action descriptors.
	 */
	public String actionDirectory;
	/**
	 * Path to directory which contains ".actor" files - .json files with actor descriptors.
	 */
	public String actorDirectory;
	
	/**
	 * Loads config from ".json" file
	 */
	public static ResourceManagerConfig loadFromFile(String filename) {
		ResourceManagerConfig config = new Json().fromJson(ResourceManagerConfig.class, Gdx.files.internal(filename));
		return config;
	}
	
	/**
	 * Saves config to ".json" file
	 */
	public void saveToFile(String filename) {
		new Json().toJson(this, Gdx.files.internal(filename));
	}
}
