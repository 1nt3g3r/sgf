package ua.com.integer.sgf.ncm;

public interface IncomingCommandHandler {
	public void onCommand(String cmd);
}
