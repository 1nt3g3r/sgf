package ua.com.integer.sgf.ncm;

public interface OutcomingCommand {
	public String getCommand();
}
