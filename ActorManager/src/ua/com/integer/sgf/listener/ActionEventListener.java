package ua.com.integer.sgf.listener;

import ua.com.integer.sgf.action.ActionManager;
import ua.com.integer.sgf.actor.GameActor;
import ua.com.integer.sgf.actor.simple.SimpleActor;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class ActionEventListener extends InputListener {
	private ActionManager am;
	private Action currentAction;
	private ActionEventListenerConfig config;
	private SimpleActor gameActor;
	
	public ActionEventListener(ActionEventListenerConfig config, SimpleActor gameActor) {
		this.gameActor = gameActor;
		am = GameActor.rm().getManager(ActionManager.class);
		setConfig(config);
	}
	
	public ActionEventListenerConfig getConfig() {
		return config;
	}
	
	public void setConfig(ActionEventListenerConfig config) {
		this.config = config;
	}
	
	public static ActionEventListener loadFromConfig(ActionEventListenerConfig cfg, SimpleActor actor) {
		return new ActionEventListener(cfg, actor);
	}
	
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer,
			int button) {
		if (!config.touchDownAction.equals("")) {
			addAction(config.touchDownAction);
		}
		return true;
	}
	
	@Override
	public void touchUp(InputEvent event, float x, float y, int pointer,
			int button) {
		if (!config.touchUpAction.equals("")) {
			addAction(config.touchUpAction);
		}
		super.touchUp(event, x, y, pointer, button);
	}
	
	private void addAction(String actionName) {
		if(config.removePreviousAction) {
			gameActor.removeAction(currentAction);
		}
		currentAction = am.getAction(actionName);
		gameActor.addAction(currentAction);
	}
	
}
