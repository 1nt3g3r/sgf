package ua.com.integer.sgf.editor.event;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import ua.com.integer.sgf.action.ActionManager;
import ua.com.integer.sgf.actor.GameActor;
import ua.com.integer.sgf.actor.simple.SimpleActorConfig;

public class GameActorEventEditor extends JDialog {
	private static final long serialVersionUID = -2791240148581440355L;
	private final JPanel contentPanel = new JPanel();
	private JComboBox boxTouchUpAction;
	private JComboBox boxTouchDownAction;
	private JCheckBox chckbxRemovePreviousAction;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			GameActorEventEditor dialog = new GameActorEventEditor(null);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private SimpleActorConfig config;
	/**
	 * Create the dialog.
	 */
	public GameActorEventEditor(SimpleActorConfig config) {
		addWindowListener(new CloseWindowListener());
		setTitle("Event editor");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new CompoundBorder(new EmptyBorder(10, 10, 10, 10), new LineBorder(new Color(192, 192, 192))));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		{
			JPanel removePreviousActionPanel = new JPanel();
			removePreviousActionPanel.setBorder(null);
			contentPanel.add(removePreviousActionPanel);
			removePreviousActionPanel.setLayout(new BoxLayout(removePreviousActionPanel, BoxLayout.X_AXIS));
			{
				chckbxRemovePreviousAction = new JCheckBox("Need remove previous action");
				removePreviousActionPanel.add(chckbxRemovePreviousAction);
			}
		}
		{
			Component verticalStrut = Box.createVerticalStrut(20);
			contentPanel.add(verticalStrut);
		}
		{
			JPanel touchDownPanel = new JPanel();
			touchDownPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
			contentPanel.add(touchDownPanel);
			touchDownPanel.setLayout(new BoxLayout(touchDownPanel, BoxLayout.X_AXIS));
			{
				JLabel lblOntouchdown = new JLabel("onTouchDown");
				touchDownPanel.add(lblOntouchdown);
			}
			{
				boxTouchDownAction = new JComboBox();
				boxTouchDownAction.setEditable(true);
				touchDownPanel.add(boxTouchDownAction);
			}
		}
		{
			Component verticalStrut = Box.createVerticalStrut(20);
			contentPanel.add(verticalStrut);
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new LineBorder(new Color(0, 0, 0)));
			contentPanel.add(panel);
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			{
				JLabel lblOntouchup = new JLabel("onTouchUp     ");
				panel.add(lblOntouchup);
			}
			{
				boxTouchUpAction = new JComboBox();
				boxTouchUpAction.setEditable(true);
				panel.add(boxTouchUpAction);
			}
		}
		
		this.config = config;
		if (config != null) {
			updateActionBoxes();
			boxTouchDownAction.setSelectedItem(config.eventConfig.touchDownAction);
			boxTouchUpAction.setSelectedItem(config.eventConfig.touchUpAction);
			chckbxRemovePreviousAction.setSelected(config.eventConfig.removePreviousAction);
		}
		pack();
	}
	
	private void updateActionBoxes() {
		ActionManager am = GameActor.rm().getManager(ActionManager.class);
		am.loadAll();
		while(!am.loadStep());
		
		String[] actionNames = am.getActionNames();
		boxTouchDownAction.setModel(new DefaultComboBoxModel(actionNames));
		boxTouchUpAction.setModel(new DefaultComboBoxModel(actionNames));
	}
	
	public SimpleActorConfig getConfig() {
		config.eventConfig.removePreviousAction = chckbxRemovePreviousAction.isSelected();
		config.eventConfig.touchDownAction = boxTouchDownAction.getSelectedItem().toString();
		config.eventConfig.touchUpAction = boxTouchUpAction.getSelectedItem().toString();
		return config;
	}
	
	class CloseWindowListener extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			propertyChanged();
			dispose();
		}
	}
	
	public void propertyChanged() {
		
	}
}
