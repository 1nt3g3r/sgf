package ua.com.integer.sgf.editor.label;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ua.com.integer.sgf.actor.GameActor;
import ua.com.integer.sgf.actor.simple.LabelConfig;
import ua.com.integer.sgf.actor.simple.SimpleActorConfig;
import ua.com.integer.sgf.editor.DimensionPanel;
import ua.com.integer.sgf.editor.property.listener.PropertyChangedListener;
import ua.com.integer.sgf.fontmanager.TTFFontManager;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

public class LabelEditorDialog extends JDialog implements PropertyChangedListener {
	private static final long serialVersionUID = -672864539346903990L;
	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private SimpleActorConfig config;
	private DimensionPanel fontSize;
	private JComboBox fontBox;
	private JCheckBox chckbxShowLabel;
	private JSpinner blueSpinner;
	private JSpinner redSpinner;
	private JSpinner greenSpinner;
	private JCheckBox chckbxLocalizeThisLabel;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JRadioButton verticalCenterRButton;
	private JRadioButton bottomAlignRButton;
	private JRadioButton horizCenterAlignButton;
	private JRadioButton rightAlignRButton;
	private JRadioButton leftAlignRButton;
	private JRadioButton topAlignRButton;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			LabelEditorDialog dialog = new LabelEditorDialog(null);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	/**
	 * Create the dialog.
	 */
	public LabelEditorDialog(SimpleActorConfig config) {
		addWindowListener(new WindowCloseListener());
		this.config = config;
		setTitle("Label editor");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.X_AXIS));
		{
			JPanel panelProperties = new JPanel();
			panelProperties.setBorder(new TitledBorder(null, "Properties", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			contentPanel.add(panelProperties);
			panelProperties.setLayout(new BoxLayout(panelProperties, BoxLayout.Y_AXIS));
			{
				JPanel panel_1 = new JPanel();
				panelProperties.add(panel_1);
				panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
				{
					JButton updateButton = new JButton("Update recursively");
					updateButton.addActionListener(new UpdateListener());
					panel_1.add(updateButton);
				}
			}
			{
				JPanel showLabelPanel = new JPanel();
				panelProperties.add(showLabelPanel);
				showLabelPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
				{
					chckbxShowLabel = new JCheckBox("Show label");
					showLabelPanel.add(chckbxShowLabel);
				}
			}
			{
				JPanel panel_1 = new JPanel();
				panelProperties.add(panel_1);
				panel_1.setBorder(new TitledBorder(null, "Need localize", TitledBorder.LEADING, TitledBorder.TOP, null, null));
				panel_1.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
				{
					chckbxLocalizeThisLabel = new JCheckBox("Localize this label");
					panel_1.add(chckbxLocalizeThisLabel);
				}
			}
			{
				JPanel textPanel = new JPanel();
				panelProperties.add(textPanel);
				textPanel.setBorder(new TitledBorder(null, "Text", TitledBorder.LEADING, TitledBorder.TOP, null, null));
				textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.X_AXIS));
				{
					textField = new JTextField();
					textPanel.add(textField);
					textField.setColumns(10);
				}
			}
			{
				Component verticalStrut = Box.createVerticalStrut(20);
				panelProperties.add(verticalStrut);
			}
			{
				JPanel fontPanel = new JPanel();
				panelProperties.add(fontPanel);
				fontPanel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Font name", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
				fontPanel.setLayout(new BoxLayout(fontPanel, BoxLayout.X_AXIS));
				{
					fontBox = new JComboBox();
					fontPanel.add(fontBox);
				}
			}
			{
				JPanel fontSizePanel = new JPanel();
				panelProperties.add(fontSizePanel);
				fontSizePanel.setBorder(new TitledBorder(null, "Font size", TitledBorder.LEADING, TitledBorder.TOP, null, null));
				fontSizePanel.setLayout(new BoxLayout(fontSizePanel, BoxLayout.X_AXIS));
				{
					fontSize = new DimensionPanel((String) null);
					fontSizePanel.add(fontSize);
				}
				{
					Component horizontalGlue = Box.createHorizontalGlue();
					fontSizePanel.add(horizontalGlue);
				}
			}
			{
				JPanel colorPanel = new JPanel();
				panelProperties.add(colorPanel);
				colorPanel.setBorder(new TitledBorder(null, "Color", TitledBorder.LEADING, TitledBorder.TOP, null, null));
				colorPanel.setLayout(new BoxLayout(colorPanel, BoxLayout.X_AXIS));
				{
					JLabel lblR = new JLabel("R");
					lblR.setForeground(Color.RED);
					lblR.setBackground(Color.RED);
					colorPanel.add(lblR);
				}
				{
					redSpinner = new JSpinner();
					redSpinner.setModel(new SpinnerNumberModel(new Float(0.5f), new Float(0), new Float(1), new Float(0.01f)));
					colorPanel.add(redSpinner);
				}
				{
					JLabel lblG = new JLabel("G");
					lblG.setForeground(Color.GREEN);
					colorPanel.add(lblG);
				}
				{
					greenSpinner = new JSpinner();
					greenSpinner.setModel(new SpinnerNumberModel(new Float(0.5f), new Float(0), new Float(1), new Float(0.01f)));
					colorPanel.add(greenSpinner);
				}
				{
					JLabel lblB = new JLabel("B");
					lblB.setForeground(Color.BLUE);
					colorPanel.add(lblB);
				}
				{
					blueSpinner = new JSpinner();
					blueSpinner.setModel(new SpinnerNumberModel(new Float(0.5f), new Float(0), new Float(1), new Float(0.01f)));
					colorPanel.add(blueSpinner);
				}
			}
		}
		{
			Component horizontalStrut = Box.createHorizontalStrut(20);
			contentPanel.add(horizontalStrut);
		}
		{
			JPanel alignPanel = new JPanel();
			alignPanel.setBorder(new TitledBorder(null, "Label position", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			contentPanel.add(alignPanel);
			alignPanel.setLayout(new BoxLayout(alignPanel, BoxLayout.Y_AXIS));
			{
				JPanel horizontalAlignPanel = new JPanel();
				horizontalAlignPanel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Horizontal alignment", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
				alignPanel.add(horizontalAlignPanel);
				horizontalAlignPanel.setLayout(new BoxLayout(horizontalAlignPanel, BoxLayout.X_AXIS));
				{
					leftAlignRButton = new JRadioButton("Left  ");
					buttonGroup.add(leftAlignRButton);
					horizontalAlignPanel.add(leftAlignRButton);
				}
				{
					horizCenterAlignButton = new JRadioButton("Center");
					buttonGroup.add(horizCenterAlignButton);
					horizontalAlignPanel.add(horizCenterAlignButton);
				}
				{
					rightAlignRButton = new JRadioButton("Right");
					buttonGroup.add(rightAlignRButton);
					horizontalAlignPanel.add(rightAlignRButton);
				}
			}
			{
				JPanel verticalAlignPanel = new JPanel();
				verticalAlignPanel.setBorder(new TitledBorder(null, "Vertical alignment", TitledBorder.LEADING, TitledBorder.TOP, null, null));
				alignPanel.add(verticalAlignPanel);
				verticalAlignPanel.setLayout(new BoxLayout(verticalAlignPanel, BoxLayout.X_AXIS));
				{
					topAlignRButton = new JRadioButton("Top");
					buttonGroup_1.add(topAlignRButton);
					verticalAlignPanel.add(topAlignRButton);
				}
				{
					verticalCenterRButton = new JRadioButton("Center");
					buttonGroup_1.add(verticalCenterRButton);
					verticalAlignPanel.add(verticalCenterRButton);
				}
				{
					bottomAlignRButton = new JRadioButton("Bottom");
					buttonGroup_1.add(bottomAlignRButton);
					verticalAlignPanel.add(bottomAlignRButton);
				}
			}
			{
				JPanel panel = new JPanel();
				alignPanel.add(panel);
			}
		}
		
		pack();
		
		if (config != null) {
			updateFontNames();
			
			ActionListener propertyChangedListener = new PropertyChangeActionListener();
			
			chckbxShowLabel.setSelected(config.labelConfig.needText);
			chckbxShowLabel.addActionListener(propertyChangedListener);
			
			chckbxLocalizeThisLabel.setSelected(config.labelConfig.needLocalize);
			chckbxLocalizeThisLabel.addActionListener(propertyChangedListener);
			
			fontBox.setSelectedItem(config.labelConfig.fontName);
			fontBox.addActionListener(propertyChangedListener);
			
			fontSize.setDimension(config.labelConfig.fontSize);
			fontSize.addPropertyChangedListener(this);
			
			textField.setText(config.labelConfig.text);
			textField.addKeyListener(new TextEnteredListener());
			
			ColorChangedListener colorChangedListener = new ColorChangedListener();
			
			redSpinner.setValue(config.labelConfig.r);
			redSpinner.addChangeListener(colorChangedListener);
			
			greenSpinner.setValue(config.labelConfig.g);
			greenSpinner.addChangeListener(colorChangedListener);
			
			blueSpinner.setValue(config.labelConfig.b);
			blueSpinner.addChangeListener(colorChangedListener);
			
			switch (config.labelConfig.horizontalAlign) {
			case 0 : horizCenterAlignButton.setSelected(true); break;
			case 1 : leftAlignRButton.setSelected(true); break;
			case 2 : rightAlignRButton.setSelected(true); break;
			}
			
			horizCenterAlignButton.addActionListener(propertyChangedListener);
			leftAlignRButton.addActionListener(propertyChangedListener);
			rightAlignRButton.addActionListener(propertyChangedListener);
			
			switch(config.labelConfig.verticalAlign) {
			case 0 : verticalCenterRButton.setSelected(true); break;
			case 1 : topAlignRButton.setSelected(true); break;
			case 2 : bottomAlignRButton.setSelected(true); break;
			}
			
			verticalCenterRButton.addActionListener(propertyChangedListener);
			topAlignRButton.addActionListener(propertyChangedListener);
			bottomAlignRButton.addActionListener(propertyChangedListener);
		}
	}
	
	class TextEnteredListener extends KeyAdapter {
		@Override
		public void keyReleased(KeyEvent e) {
			if (chckbxShowLabel.isSelected()) {
				propertyChanged();
			}
		}
	}
	
	private void updateFontNames() {
		TTFFontManager fm = GameActor.rm().getManager(TTFFontManager.class);
		fontBox.setModel(new DefaultComboBoxModel(fm.getFontNames()));
	}

	public SimpleActorConfig getConfig() {
		LabelConfig labelConfig = config.labelConfig;
		labelConfig.needText = chckbxShowLabel.isSelected();
		labelConfig.needLocalize = chckbxLocalizeThisLabel.isSelected();
		labelConfig.text = textField.getText();
		
		labelConfig.r = Float.parseFloat(redSpinner.getValue().toString());
		labelConfig.g = Float.parseFloat(greenSpinner.getValue().toString());
		labelConfig.b = Float.parseFloat(blueSpinner.getValue().toString());
		
		if (fontBox.getSelectedItem() != null) {
			labelConfig.fontName = fontBox.getSelectedItem().toString();
		}
		labelConfig.fontSize = fontSize.getDimension();
		
		if (horizCenterAlignButton.isSelected()) {
			labelConfig.horizontalAlign = 0;
		} else if (leftAlignRButton.isSelected()) {
			labelConfig.horizontalAlign = 1;
		} else if (rightAlignRButton.isSelected()) {
			labelConfig.horizontalAlign = 2;
		}
		
		if (verticalCenterRButton.isSelected()) {
			labelConfig.verticalAlign = 0;
		} else if (topAlignRButton.isSelected()) {
			labelConfig.verticalAlign = 1;
		} else if (bottomAlignRButton.isSelected()) {
			labelConfig.verticalAlign = 2;
		}
		
		return config;
	}
	
	class WindowCloseListener extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			updateProperties();
		}
	}
	
	class PropertyChangeActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			propertyChanged();
		}
	}

	@Override
	public void propertyChanged() {
		if (chckbxShowLabel.isSelected()) {
			updateProperties();
		}
	}
	
	private void updateProperties() {
		
	}
	
	class UpdateListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			updatePressed();
		}
	};

	/**
	 * This method calls when "Update" button pressed
	 */
	public void updatePressed() {
		
	}
	
	class ColorChangedListener implements ChangeListener {
		@Override
		public void stateChanged(ChangeEvent e) {
			propertyChanged();
		}
	}

	public JCheckBox getChckbxLocalizeThisLabel() {
		return chckbxLocalizeThisLabel;
	}
	public JRadioButton getVerticalCenterRButton() {
		return verticalCenterRButton;
	}
	public JRadioButton getBottomAlignRButton() {
		return bottomAlignRButton;
	}
	public JRadioButton getHorizCenterAlignButton() {
		return horizCenterAlignButton;
	}
	public JRadioButton getRightAlignRButton() {
		return rightAlignRButton;
	}
	public JRadioButton getLeftAlignRButton() {
		return leftAlignRButton;
	}
	public JRadioButton getTopAlignRButton() {
		return topAlignRButton;
	}
}
