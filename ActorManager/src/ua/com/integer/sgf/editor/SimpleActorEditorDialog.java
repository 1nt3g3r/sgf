package ua.com.integer.sgf.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ua.com.integer.sgf.actor.GameActor;
import ua.com.integer.sgf.actor.simple.LabelConfig;
import ua.com.integer.sgf.actor.simple.SimpleActorConfig;
import ua.com.integer.sgf.dimension.Dimension.RelativeSize;
import ua.com.integer.sgf.editor.property.listener.PropertyChangedListener;
import ua.com.integer.sgf.listener.ActionEventListenerConfig;

import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;

public class SimpleActorEditorDialog extends JDialog {
	private static final long serialVersionUID = -2804869608938261042L;
	private DimensionPanel x;
	private final JPanel contentPanel = new JPanel();
	private JComboBox regionBox;
	private JComboBox packBox;
	private DimensionPanel y;
	private DimensionPanel width;
	private DimensionPanel height;
	private JSpinner rotationSpinner;
	private JCheckBox rotateAroundCenterCheckbox;
	private PropertyUpdateListener chandedListener;
	private JTextField textFieldUnicalName;
	private ActionEventListenerConfig eventConfig;
	private LabelConfig labelConfig;
	private JCheckBox chckbxCenterX;
	private JCheckBox chckbxCenterY;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			SimpleActorEditorDialog dialog = new SimpleActorEditorDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public SimpleActorEditorDialog() {
		addWindowListener(new EndEditListener());

		chandedListener = new PropertyUpdateListener();
		setTitle("Simple actor properties editor");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new CompoundBorder(new EmptyBorder(5, 5, 5, 5), new LineBorder(new Color(0, 0, 0))));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		
		JPanel namePanel = new JPanel();
		namePanel.setBorder(new TitledBorder(null, "Unical actor name", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPanel.add(namePanel);
		namePanel.setLayout(new BoxLayout(namePanel, BoxLayout.X_AXIS));
		
		textFieldUnicalName = new JTextField();
		textFieldUnicalName.addKeyListener(new EnterPressedListener());
		namePanel.add(textFieldUnicalName);
		textFieldUnicalName.setColumns(10);
		
		JPanel regionPanel = new JPanel();
		regionPanel.setBorder(new TitledBorder(null, "Region", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPanel.add(regionPanel);
		regionPanel.setLayout(new BoxLayout(regionPanel, BoxLayout.X_AXIS));
		
		JLabel labelPackName = new JLabel("Pack name:");
		regionPanel.add(labelPackName);
		
		packBox = new JComboBox();
		packBox.addActionListener(new ChoosePackBoxListener());
		regionPanel.add(packBox);
		
		JLabel labelRegionName = new JLabel("Region name:");
		regionPanel.add(labelRegionName);
		
		regionBox = new JComboBox();
		regionBox.addActionListener(new PropertyUpdateListener());
		regionPanel.add(regionBox);
		
		JPanel xPanel = new JPanel();
		contentPanel.add(xPanel);
		xPanel.setLayout(new BoxLayout(xPanel, BoxLayout.X_AXIS));
		
		x = new DimensionPanel("X");
		x.setAllowedValues(RelativeSize.SCREEN_WIDTH);
		x.addPropertyChangedListener(chandedListener);
		xPanel.add(x);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		xPanel.add(horizontalStrut);
		
		chckbxCenterX = new JCheckBox("Center X");
		chckbxCenterX.addActionListener(new CenterListener());
		xPanel.add(chckbxCenterX);
		
		Component horizontalGlue_4 = Box.createHorizontalGlue();
		xPanel.add(horizontalGlue_4);
		
		JPanel yPanel = new JPanel();
		contentPanel.add(yPanel);
		yPanel.setLayout(new BoxLayout(yPanel, BoxLayout.X_AXIS));
		
		y = new DimensionPanel("Y");
		y.setAllowedValues(RelativeSize.SCREEN_HEIGHT);
		y.addPropertyChangedListener(chandedListener);
		yPanel.add(y);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		yPanel.add(horizontalStrut_1);
		
		chckbxCenterY = new JCheckBox("Center Y");
		chckbxCenterY.addActionListener(new CenterListener());
		yPanel.add(chckbxCenterY);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		yPanel.add(horizontalGlue);
		
		JPanel widthPanel = new JPanel();
		contentPanel.add(widthPanel);
		widthPanel.setLayout(new BoxLayout(widthPanel, BoxLayout.X_AXIS));
		
		width = new DimensionPanel("Width");
		width.setAllowedValues(RelativeSize.SCREEN_WIDTH, RelativeSize.SCREEN_HEIGHT);
		width.addPropertyChangedListener(chandedListener);
		widthPanel.add(width);
		
		JLabel lblRotation = new JLabel("Rotation:");
		widthPanel.add(lblRotation);
		
		rotationSpinner = new JSpinner();
		widthPanel.add(rotationSpinner);
		rotationSpinner.addChangeListener(chandedListener);
		rotationSpinner.setMaximumSize(new Dimension(60, 25));
		rotationSpinner.setPreferredSize(new Dimension(50, 20));
		
		Component horizontalGlue_2 = Box.createHorizontalGlue();
		widthPanel.add(horizontalGlue_2);
		
		JPanel heightPanel = new JPanel();
		contentPanel.add(heightPanel);
		heightPanel.setLayout(new BoxLayout(heightPanel, BoxLayout.X_AXIS));
		
		height = new DimensionPanel("Height");
		height.setAllowedValues(RelativeSize.SCREEN_HEIGHT);
		height.addPropertyChangedListener(chandedListener);
		heightPanel.add(height);
		
		rotateAroundCenterCheckbox = new JCheckBox("Rotate around center");
		heightPanel.add(rotateAroundCenterCheckbox);
		rotateAroundCenterCheckbox.addActionListener(chandedListener);
		
		Component horizontalGlue_3 = Box.createHorizontalGlue();
		heightPanel.add(horizontalGlue_3);
		
		JButton buttonFullScreen = new JButton("To screen size");
		buttonFullScreen.addActionListener(new FullSizeListener());
		heightPanel.add(buttonFullScreen);
		
		pack();
	}
	
	class EnterPressedListener extends KeyAdapter {
		@Override
		public void keyReleased(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				propertyChanged();
			}
		}
	}
	
	public void setSimpleActorConfig(SimpleActorConfig config) {
		initChooseRegionPanel();
		labelConfig = config.labelConfig;
		eventConfig = config.eventConfig;
		
		textFieldUnicalName.setText(config.name);
		packBox.setSelectedItem(config.packName);
		regionBox.setSelectedItem(config.regionName);
		x.setDimension(config.x);
		y.setDimension(config.y);
		width.setDimension(config.width);
		height.setDimension(config.height);
		rotationSpinner.setValue(config.rotation);
		rotateAroundCenterCheckbox.setSelected(config.rotateAroundCenter);
		
		chckbxCenterX.setSelected(config.centerX);
		x.setVisible(!chckbxCenterX.isSelected());
		
		chckbxCenterY.setSelected(config.centerY);
		y.setVisible(!chckbxCenterY.isSelected());
		
		propertyChanged();
	}
	
	public SimpleActorConfig getConfig() {
		SimpleActorConfig toReturn = new SimpleActorConfig();
		
		if (packBox.getSelectedItem() != null) {
			toReturn.packName = packBox.getSelectedItem().toString();
			toReturn.regionName = regionBox.getSelectedItem().toString();
		}
		toReturn.name = textFieldUnicalName.getText();
		toReturn.allowHeightOnlyForSize = width.getDimension().getRelativeSize() == RelativeSize.SCREEN_HEIGHT;
		toReturn.x = x.getDimension();
		toReturn.y = y.getDimension();
		toReturn.width = width.getDimension();
		toReturn.height = height.getDimension();
		toReturn.rotation = Float.parseFloat(rotationSpinner.getValue().toString());
		toReturn.rotateAroundCenter = rotateAroundCenterCheckbox.isSelected();
		toReturn.eventConfig = eventConfig;
		toReturn.labelConfig = labelConfig;
		toReturn.centerX = chckbxCenterX.isSelected();
		toReturn.centerY = chckbxCenterY.isSelected();
		return toReturn;
	}
	
	class ChoosePackBoxListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			updateChooseRegionCombobox();
			propertyChanged();
		}
	}
	
	private void initChooseRegionPanel() {
		packBox.setModel(new DefaultComboBoxModel(GameActor.rm().textures().getLoadedPacks().items));
		updateChooseRegionCombobox();
	}
	
	private void updateChooseRegionCombobox() {
		if (packBox.getSelectedItem() == null) {
			return;
		}
		
		Array<AtlasRegion> regions = GameActor.rm().textures().getRegionsFromPack(packBox.getSelectedItem().toString());
		String[] regionNames = new String[regions.size];
		for(int i = 0; i < regionNames.length; i++) {
			regionNames[i] = regions.get(i).name;
		}
		regionBox.setModel(new DefaultComboBoxModel(regionNames));
	}
	
	
	class EndEditListener extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			onClosing();
		}
	}
	
	class PropertyUpdateListener implements ActionListener, PropertyChangedListener, ChangeListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			propertyChanged();
		}

		@Override
		public void propertyChanged() {
			SimpleActorEditorDialog.this.propertyChanged();
		}

		@Override
		public void stateChanged(ChangeEvent arg0) {
			propertyChanged();
		}
	}
	
	/**
	 * This method calls when window closing.
	 */
	public void onClosing() {
	}
	
	/**
	 * This method calls when some property was changed.
	 */
	public void propertyChanged() {
		
	}
	public JTextField getTextFieldUnicalName() {
		return textFieldUnicalName;
	}
	
	class FullSizeListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			width.setDimension(ua.com.integer.sgf.dimension.Dimension.getDimension(RelativeSize.SCREEN_WIDTH, 1));
			height.setDimension(ua.com.integer.sgf.dimension.Dimension.getDimension(RelativeSize.SCREEN_HEIGHT, 1));
			x.setDimension(ua.com.integer.sgf.dimension.Dimension.getDimension(RelativeSize.SCREEN_WIDTH, 0));
			y.setDimension(ua.com.integer.sgf.dimension.Dimension.getDimension(RelativeSize.SCREEN_HEIGHT, 0));
			propertyChanged();
		}
	}
	
	class CenterListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			x.setVisible(!chckbxCenterX.isSelected());
			y.setVisible(!chckbxCenterY.isSelected());

			propertyChanged();
		}
	}

}
