package ua.com.integer.sgf.actor.simple;

import ua.com.integer.sgf.dimension.Dimension;
import ua.com.integer.sgf.dimension.Dimension.RelativeSize;

public class LabelConfig {
	public float r, g, b;
	/**
	 * How to horizontally align label. 0 - center, 1 - left, 2 - right
	 */
	public int horizontalAlign;
	/**
	 * How to vertically align label. 0 - center, 1 - top, 2 - bottom
	 */
	public int verticalAlign;
	public boolean needText;
	public boolean needLocalize;
	public String text = "";
	public String fontName = "";
	public Dimension fontSize = Dimension.getDimension(RelativeSize.SCREEN_HEIGHT, 0.05f);
}
