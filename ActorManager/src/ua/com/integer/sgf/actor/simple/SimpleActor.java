package ua.com.integer.sgf.actor.simple;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import ua.com.integer.sgf.JsonWorker;
import ua.com.integer.sgf.actor.GameActor;
import ua.com.integer.sgf.actor.simple.listener.ActivateAndEditClickListener;
import ua.com.integer.sgf.dimension.Dimension;
import ua.com.integer.sgf.dimension.Dimension.RelativeSize;
import ua.com.integer.sgf.editor.SimpleActorEditorDialog;
import ua.com.integer.sgf.editor.event.GameActorEventEditor;
import ua.com.integer.sgf.editor.label.LabelEditorDialog;
import ua.com.integer.sgf.editor.util.SwingUtils;
import ua.com.integer.sgf.fontmanager.TTFFontManager;
import ua.com.integer.sgf.listener.ActionEventListener;
import ua.com.integer.sgf.listener.ActionEventListenerConfig;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

public class SimpleActor extends GameActor {
	private static int counter;

	private String packName;
	private String regionName;
	private boolean rotateAroundCenter;
	private TextureRegion region;
	private boolean allowUseHeightOnlyForSize;
	private boolean centerX, centerY;

	private Label label;
	private LabelConfig labelConfig;
	
	private InputListener eventListener;
	private ActionEventListenerConfig eventConfig;
	
	public SimpleActor(String packName, String regionName) {
		this(new SimpleActorConfig());
		setRegion(packName, regionName);
	}
	
	public SimpleActor() {
		this(new SimpleActorConfig());
		setName("actor" + ++counter);
	}
	
	public SimpleActor(SimpleActorConfig config) {
		setConfig(config);
	}
	
	public void setConfig(SimpleActorConfig config) {

		setName(config.name);
		setX(config.x.getValue());
		setY(config.y.getValue());
		
		setSize(config.width.getValue(), config.height.getValue());
		setRegion(config.packName, config.regionName);
		setRotation(config.rotation);
		rotateAroundCenter = config.rotateAroundCenter;
		allowUseHeightOnlyForSize = config.allowHeightOnlyForSize;

		updateLabelConfig(config.labelConfig);
		updateEventConfig(config.eventConfig);
		
		centerX = config.centerX;
		centerY = config.centerY;
		
		if (centerX) {
			centerToParentX();
		}
		if (centerY) {
			centerToParentY();
		}
	}
	
	public SimpleActorConfig getConfig() {
		SimpleActorConfig result = new SimpleActorConfig();
		result.name = getName();
		if (allowUseHeightOnlyForSize) {
			result.width = Dimension.getDimension(RelativeSize.SCREEN_HEIGHT, getWidth()/Gdx.graphics.getHeight());
		} else {
			result.width = Dimension.getDimension(RelativeSize.SCREEN_WIDTH, getWidth()/Gdx.graphics.getWidth());
		}
		result.height = Dimension.getDimension(RelativeSize.SCREEN_HEIGHT, getHeight()/Gdx.graphics.getHeight());

	
		result.x = Dimension.getDimension(RelativeSize.SCREEN_WIDTH, getX()/Gdx.graphics.getWidth());
		result.y = Dimension.getDimension(RelativeSize.SCREEN_HEIGHT, getY()/Gdx.graphics.getHeight());
		result.packName = packName;
		result.regionName = regionName;
		result.rotateAroundCenter = rotateAroundCenter;
		result.allowHeightOnlyForSize = allowUseHeightOnlyForSize;
		result.rotation = getRotation();
		
		result.eventConfig = eventConfig;
		result.labelConfig = labelConfig;
		
		result.children.shrink();
		for(Actor actor : getChildren()) {
			if (actor instanceof SimpleActor) {
				SimpleActor childActor = (SimpleActor) actor;
				result.children.add(childActor.getConfig());
			}
		}
		result.centerX = centerX;
		result.centerY = centerY;
		return result;
	}
	
	public void setRegion(String packName, String regionName) {
		this.packName = packName;
		this.regionName = regionName;
		if (packName != null && regionName != null) {
			region = rm().textures().get(packName, regionName);
		}
	}
	
	public void setRegion(TextureRegion region) {
		this.region = region;
	}

	@Override
	public void saveToFile(File file) {
		String jsonFile = JsonWorker.GSON.toJson(getConfig());
		try {
			FileWriter writer = new FileWriter(file);
			writer.write(jsonFile);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			Gdx.app.error("Actors", "Error during saving actor to file " + file);
			System.exit(0);
		}
	}

	@Override
	public void loadFromFile(File file) {
		setConfig(loadConfigFromFile(file));
	}
	
	public static SimpleActorConfig loadConfigFromFile(File file) {
		try {
			Scanner sc = new Scanner(file);
			String jsonConfig = sc.nextLine();
			SimpleActorConfig config = JsonWorker.GSON.fromJson(jsonConfig, SimpleActorConfig.class);
			return config;
		} catch (FileNotFoundException e) {
			Gdx.app.error("Actors", "Error during loading actor from file " + file);
			System.exit(0);
			return null;
		}
	}
	
	public static SimpleActor loadFromConfig(SimpleActorConfig config, boolean needDebug) {
		SimpleActor actor = new SimpleActor(config);
		actor.addListener(new ActivateAndEditClickListener(actor));
		for(SimpleActorConfig childConfig : config.children) {
			actor.addActor(loadFromConfig(childConfig));
		}
		return actor;
	}
	
	public static SimpleActor loadFromConfig(SimpleActorConfig config) {
		SimpleActor actor = new SimpleActor(config);
		for(SimpleActorConfig childConfig : config.children) {
			actor.addActor(loadFromConfig(childConfig));
		}
		return actor;
	}
	
	public static SimpleActor loadActorFromFile(File file, boolean needDebug) {
		SimpleActor toReturn = loadFromConfig(loadConfigFromFile(file));
		if (needDebug) {
			toReturn.debug();
		}
		return toReturn;
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (region != null) {
			if (rotateAroundCenter) {
				setOrigin(getWidth()/2, getHeight()/2);
			}
			batch.setColor(getColor());
			batch.draw(region, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
		}
		super.draw(batch, parentAlpha);
	}

	@Override
	public void editProperties() {
		SimpleActorEditorDialog propertyEditor = new SimpleActorEditorDialog() {
			private static final long serialVersionUID = 7553349539060493562L;
			@Override
			public void onClosing() {
				setConfig(getConfig());
			}
			@Override
			public void propertyChanged() {
				setConfig(getConfig());
			}
		};
		propertyEditor.setSimpleActorConfig(getConfig());
		SwingUtils.showInCenter(propertyEditor);
	}

	@Override
	public void editActionEvents() {
		GameActorEventEditor editor = new GameActorEventEditor(getConfig()) {
			private static final long serialVersionUID = 5344520833930652542L;
			@Override
			public void propertyChanged() {
				setConfig(this.getConfig());
			}
		};
		SwingUtils.showInCenter(editor);
	}
	
	private void updateEventConfig(ActionEventListenerConfig eventConfig) {
		this.eventConfig = eventConfig;
		removeListener(eventListener);
		eventListener = new ActionEventListener(eventConfig, this);
		addListener(eventListener);
	}
	
	private void updateLabelConfig(LabelConfig labelConfig) {
		this.labelConfig = labelConfig;
		if (label != null) {
			label.remove();
			label = null;
		}
		
		if (labelConfig.needText && !labelConfig.fontName.equals("") && labelConfig.fontSize.getValue() >= 1) {
			String fontName = labelConfig.fontName;
			int fontSize = (int) labelConfig.fontSize.getValue();
			
			LabelStyle style = new LabelStyle();
			style.font = rm().getManager(TTFFontManager.class).getFont(fontName, fontSize);
			style.fontColor = new Color(labelConfig.r, labelConfig.g, labelConfig.b, 1);
			
			String text = labelConfig.text;
			if (labelConfig.needLocalize) {
				text = lc().translate(text);
			}
			label = new Label(text, style);
			label.setHeight(style.font.getBounds("1").height);
			
			switch(labelConfig.verticalAlign) {
			case 0 : label.setY((getHeight() - label.getHeight())/2); break;
			case 1 : label.setY(getHeight() - label.getHeight()); break;
			case 2 : label.setY(0);
			}
			
			switch(labelConfig.horizontalAlign) {
			case 0 : label.setX((getWidth() - label.getPrefWidth())/2); break;
			case 1 : label.setX(0); break;
			case 2 : label.setX(getWidth() - label.getPrefWidth()); break;
			}
		
			addActor(label);
		}
	}

	@Override
	public void editLabel() {
		LabelEditorDialog editor = new LabelEditorDialog(getConfig()) {
			private static final long serialVersionUID = 6790663196997132900L;
			@Override
			public void propertyChanged() {
				setConfig(this.getConfig());
			}
			
			@Override
			public void updatePressed() {
				localize();
			}
		};
		SwingUtils.showInCenter(editor);
	}

	public void updateLabelPosition() {
		updateLabelConfig(labelConfig);
	}
	
	public void setText(String text) {
		labelConfig.text = text;
		updateLabelPosition();
	}
	
	public String getText() {
		return labelConfig.text;
	}

	public void localize() {
		if (labelConfig.needLocalize) {
			updateLabelPosition();
			for(Actor actor : getChildren()) {
				if (actor instanceof SimpleActor) {
					SimpleActor child = (SimpleActor) actor;
					child.localize();
				}
			}
		}
	}
	
	public void applyConfig(String configName) {
		setConfig(rm().getManager(ActorManager.class).getConfig(configName));
	}
	
	public void updatePosition() {
		if (centerX) {
			centerToParentX();
		}
		if (centerY) {
			centerToParentY();
		}
		
		for(Actor child : getChildren()) {
			if (child instanceof SimpleActor) {
				((SimpleActor) child).updatePosition();
			}
		}
	}
	

}
