package ua.com.integer.sgf.actor.simple;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;

import ua.com.integer.sgf.GlobalSettings;
import ua.com.integer.sgf.actor.GameActor;
import ua.com.integer.sgf.resource.LoadManager;

public class ActorManager implements LoadManager {
	private String actorDirectory;
	private FileHandle addDirectory;
	private Map<String, SimpleActorConfig> actorConfigs;
	private Array<String> loadQueue;
	private Array<String> additionalLoadQueue;
	private boolean debug;
	int totalActorCount;
	
	public ActorManager() {
		actorConfigs = new HashMap<String, SimpleActorConfig>();
		loadQueue = new Array<String>();
		additionalLoadQueue = new Array<String>();
	}
	
	public void setActorDirectory(String actorDirectory) {
		this.actorDirectory = actorDirectory;
		debug = GlobalSettings.needDebug();
		setupAddDirectory();
	}
	
	private void setupAddDirectory() {
		Array<FileHandle> availableAdditionalDirs = new Array<FileHandle>();
		for(FileHandle addDirHandle : Gdx.files.internal(actorDirectory).list()) {
			if (addDirHandle.name().startsWith("actors") && addDirHandle.isDirectory()) {
				availableAdditionalDirs.add(addDirHandle);
			}
		}
		
		float gameScreenSizeRelation = (float) Gdx.graphics.getWidth() / (float) Gdx.graphics.getHeight();
		float delta = Float.MAX_VALUE;

		for(FileHandle dir : availableAdditionalDirs) {
			String[] parts = dir.name().split("-");
			int width = Integer.parseInt(parts[1]);
			int height = Integer.parseInt(parts[2]);
			float relation = (float) width / (float) height;
			
			if (Math.abs(gameScreenSizeRelation - relation) <= delta) {
				delta = Math.abs(gameScreenSizeRelation - relation);
				addDirectory = dir;
			}
		}
		if (addDirectory != null) {
			Gdx.app.debug(ActorManager.class.toString(), "Additional directory is " + addDirectory.name());
		}
		
	}
	
	@Override
	public void loadAll() {
		loadQueue.clear();
		additionalLoadQueue.clear();
		

		if (actorDirectory == null || !Gdx.files.internal(actorDirectory).exists()) {
			Gdx.app.error(getClass().toString(), "Actor directory isn't selected or doesn't exists!");
			return;
		}
		
		for(FileHandle actorHandle : Gdx.files.internal(actorDirectory).list()) {
			if (!actorHandle.isDirectory() && actorHandle.extension().equals("actor")) {
				loadQueue.add(actorHandle.nameWithoutExtension());
			}
		}
		
		if (addDirectory != null) {
			for(FileHandle actorHandle : addDirectory.list()) {
				if (!actorHandle.isDirectory() && actorHandle.extension().equals("actor"));
				additionalLoadQueue.add(actorHandle.nameWithoutExtension());
				if (debug) {
					Gdx.app.debug(getClass().toString(), "File from additional directory: " + actorHandle.name());
				}
			}
		}
		
		loadQueue.removeAll(additionalLoadQueue, false);
		
		totalActorCount = loadQueue.size + additionalLoadQueue.size;
		
		if (debug) {
			Gdx.app.debug(getClass().toString(), "Total actor count to load is " + totalActorCount);
		}
	}

	@Override
	public boolean loadStep() {
		return loadDefaultDirStep() && loadAddDirStep();
	}
	
	private boolean loadDefaultDirStep() {
		if (loadQueue.size == 0) {
			return true;
		} else {
			loadActorConfig(loadQueue.pop());
			return false;
		}
	}
	
	private boolean loadAddDirStep() {
		if (additionalLoadQueue.size == 0) {
			return true;
		} else {
			loadActorConfigFromAdditionalDir(additionalLoadQueue.pop());
			return false;
		}
	}
	
	private void loadActorConfig(String filename) {
		if (actorConfigs.containsKey(filename)) {
			return;
		}
		SimpleActorConfig config = SimpleActorConfig.loadFromFileHandle(Gdx.files.internal(actorDirectory+"/"+filename+".actor"));
		actorConfigs.put(filename, config);
		if (debug) {
			Gdx.app.debug(ActorManager.class + " ", "Actor " + filename + " loaded from default dir.");
		}
	}
	
	private void loadActorConfigFromAdditionalDir(String filename) {
		if (actorConfigs.containsKey(filename)) {
			return;
		}
		SimpleActorConfig config = SimpleActorConfig.loadFromFileHandle(addDirectory.child(filename + ".actor"));
		actorConfigs.put(filename, config);
		if (debug) {
			Gdx.app.debug(getClass().toString(), "Actor " + filename + " loaded from additional dir.");
		}
	}

	@Override
	public float getLoadPercent() {
		if (loadQueue.size == 0) {
			return 1;
		}
		return (float) actorConfigs.size() / (float) totalActorCount;
	}
	
	public GameActor getActor(String name) {
		return SimpleActor.loadFromConfig(getConfig(name));
	}
	
	public SimpleActorConfig getConfig(String name) {
		if (actorConfigs.get(name) == null) {
			if (actorExistsInMainDir(name)) {
				loadActorConfig(name);
			}
			loadActorConfigFromAdditionalDir(name);
		}
		if (actorConfigs.get(name) == null) {
			throw new IllegalArgumentException("Actor " + name + "not found!");
		}
		return actorConfigs.get(name);
	}
	
	private boolean actorExistsInMainDir(String actorName) {
		return Gdx.files.internal(actorDirectory + "/" + actorName + ".actor").exists();
	}

	@Override
	public String getLoadDescription() {
		return "Actors";
	}

	@Override
	public void dispose() {}
}
