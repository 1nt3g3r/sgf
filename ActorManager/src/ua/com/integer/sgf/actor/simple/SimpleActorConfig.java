package ua.com.integer.sgf.actor.simple;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import ua.com.integer.sgf.JsonWorker;
import ua.com.integer.sgf.dimension.Dimension;
import ua.com.integer.sgf.dimension.Dimension.RelativeSize;
import ua.com.integer.sgf.listener.ActionEventListenerConfig;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;

public class SimpleActorConfig {
	public Array<SimpleActorConfig> children = new Array<SimpleActorConfig>();
	
	public ActionEventListenerConfig eventConfig = new ActionEventListenerConfig();
	public LabelConfig labelConfig = new LabelConfig();
	
	public String name;
	public boolean allowHeightOnlyForSize = true;
	public boolean centerX, centerY;
	public Dimension x = Dimension.getDimension(RelativeSize.SCREEN_WIDTH, 0.25f);
	public Dimension y = Dimension.getDimension(RelativeSize.SCREEN_HEIGHT, 0.25f);
	public Dimension width = Dimension.getDimension(RelativeSize.SCREEN_HEIGHT, 0.25f);
	public Dimension height = Dimension.getDimension(RelativeSize.SCREEN_HEIGHT, 0.25f);
	public float rotation = 0;
	public boolean rotateAroundCenter = true;
	public String packName;
	public String regionName;
	
	public static SimpleActorConfig loadFromFileHandle(FileHandle fileHandle) {
		return JsonWorker.GSON.fromJson(fileHandle.readString(), SimpleActorConfig.class);
	}
	
	public static SimpleActorConfig loadFromFile(File file) {
		try {
			Scanner sc = new Scanner(file);
			String jsonConfig = sc.nextLine();
			return JsonWorker.GSON.fromJson(jsonConfig, SimpleActorConfig.class);
		} catch (FileNotFoundException e) {
			Gdx.app.error("Actors", "Error during loading config from file " + file);
			System.exit(0);
		}
		return null;
	}
}
