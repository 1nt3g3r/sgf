package ua.com.integer.sgf.actor.simple.listener;

import ua.com.integer.sgf.actor.GameActor;
import ua.com.integer.sgf.actor.simple.SimpleActor;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class ActivateAndEditClickListener extends ClickListener {
	private GameActor actor;
	
	public ActivateAndEditClickListener(GameActor actor) {
		this.actor = actor;
	}
	
	public void clicked(InputEvent event, float x, float y) {
		if (!event.isCancelled()) {
			event.cancel();
			if (actor instanceof SimpleActor) {
				((SimpleActor) actor).deactivateAllGameActors();
			}
			actor.activate();
			actor.editProperties();
		}
	};
}
