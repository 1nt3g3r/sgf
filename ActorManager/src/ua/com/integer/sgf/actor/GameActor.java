package ua.com.integer.sgf.actor;

import java.io.File;

import ua.com.integer.sgf.localize.Localize;
import ua.com.integer.sgf.resource.ResourceManager;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

public abstract class GameActor extends Group {
	private static ResourceManager rm;
	private static Localize lc;
	private boolean debug;
	private boolean active = false;
	private Color activeColor = Color.GREEN;
	
	private static ShapeRenderer debugRenderer = new ShapeRenderer(); 
	static {
		debugRenderer.setColor(Color.GREEN);
	}
	
	public static void setResourceManager(ResourceManager resManager) {
		rm = resManager;
	}
	
	public static void setLocalize(Localize localize) {
		lc = localize;
	}
	
	public static ResourceManager rm() {
		if (rm == null) {
			throw new IllegalStateException("Call setResourceManager before call this method!");
		}
		return rm;
	}
	
	public static Localize lc() {
		if (lc == null) {
			throw new IllegalStateException("Call setLocalize before call this method!");
		}
		return lc;
	}
	
	public void debug() {
		debug = true;
		for(Actor actor : getChildren()) {
			if (actor instanceof GameActor) {
				((GameActor) actor).debug();
			}
		}
	}
	
	public boolean isActive() {
		return active;
	}
	
	public void activate() {
		deactivateAllGameActors();
		activateActor(this);
		getStage().setKeyboardFocus(this);
	}
	
	public GameActor getActiveActor() {
		if (active == true) {
			return this;
		}
		
		for(Actor child : getChildren()) {
			GameActor childGameActor = (GameActor) child;
			GameActor childActive = childGameActor.getActiveActor();
			if (childActive != null) {
				return childActive;
			} 
		}
		
		return null;
	}

	private void activateActor(GameActor toActivate) {
		if (toActivate == this) {
			toActivate.active = true;
			return;
		}

		for(Actor actor : toActivate.getChildren()) {
			GameActor gameActor = (GameActor) actor;
			if (gameActor == toActivate) {
				gameActor.active = true;
				return;
			}
			gameActor.active = false;
			for(Actor childActor : gameActor.getChildren()) {
				GameActor childGameActor = (GameActor) childActor;
				childGameActor.activateActor(toActivate);
			}
		}
	}
	
	public void deactivateAll() {
		active = false;
		for(Actor actor : getChildren()) {
			if (actor instanceof GameActor) {
				GameActor gameActor = (GameActor) actor;
				gameActor.active = false;
				for(Actor childActor : gameActor.getChildren()) {
					if (childActor instanceof GameActor) {
						GameActor childGameActor = (GameActor) childActor;
						childGameActor.deactivateAll();
					}
				}
			}
		}
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		
		if (debug) {
			batch.end();
			
			debugRenderer.setColor(activeColor);
			
			debugRenderer.setProjectionMatrix(batch.getProjectionMatrix());
			debugRenderer.setTransformMatrix(batch.getTransformMatrix());
			debugRenderer.begin(ShapeType.Line);
			debugRenderer.rect(getX(), getY(), getWidth(), getHeight());
			debugRenderer.end();
			
			batch.begin();
		}
	}
	
	private float elapsedTime;
	@Override
	public void act(float delta) {
		if (debug) {
			elapsedTime += delta;
			if (elapsedTime >= 0.5f) {
				swapActiveColors();
				elapsedTime = 0;
			}
		}
		super.act(delta);
	}
	
	private void swapActiveColors() {
		if (active) {
			if (activeColor == Color.BLACK) {
				activeColor = Color.WHITE;
			} else {
				activeColor = Color.BLACK;
			}
		} else {
			activeColor = Color.GREEN;
		}
	}
	
	/**
	 * Сбрасывает состояние актера
	 */
	public void reset() {
		clearActions();
		setRotation(0);
		setColor(Color.WHITE);
	}
	
	/**
	 * Makes all actor inactive.
	 */
	public void deactivateAllGameActors() {
		for(Actor actor : getStage().getActors()) {
			GameActor gameActor = (GameActor) actor;
			gameActor.deactivateAll();
		}
	}
	
	public void centerToParentX() {
		if (getParentWidth() != -1) {
			setX((getParentWidth() - getWidth())/2);
		}
//		if (getParent() == null) {
//			if (getStage() != null) {
//				setX((getStage().getWidth() - getWidth())/2);
//			}
//		} else {
//			if(getParent() == getStage().getRoot()) {
//				setX((getStage().getWidth() - getWidth())/2);
//			} else {
//				setX((getParent().getWidth() - getWidth())/2);
//			}
//		}
	}
	
	public float getParentWidth() {
		if (getParent() == null) {
			if (getStage() != null) {
				return getStage().getWidth();
			}
		} else {
			if (getParent() == getStage().getRoot()) {
				return getStage().getWidth();
			} else {
				return getParent().getWidth();
			}
		}
		return -1;
	}
	
	public void centerToParentY() {
		if (getParent() == null) {
			if (getStage() != null) {
				setY((getStage().getHeight() - getHeight())/2);
			}
		} else {
			if(getParent() == getStage().getRoot()) {
				setY((getStage().getHeight() - getHeight())/2);
			} else {
				setY((getParent().getHeight() - getHeight())/2);
			}
		}
	}
	
	
	public abstract void editLabel();
	public abstract void editActionEvents();
	public abstract void editProperties();
	public abstract void saveToFile(File file);
	public abstract void loadFromFile(File file);
}
