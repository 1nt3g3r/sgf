package ua.com.integer.sgf;

public class GlobalSettings {
	private static boolean debug;
	
	public static void debug() {
		debug = true;
	}
	
	public static boolean needDebug() {
		return debug;
	}
}
