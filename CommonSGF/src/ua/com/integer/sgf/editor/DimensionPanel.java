package ua.com.integer.sgf.editor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ua.com.integer.sgf.dimension.Dimension;
import ua.com.integer.sgf.dimension.Dimension.RelativeSize;
import ua.com.integer.sgf.editor.property.listener.PropertyChangedListener;
import ua.com.integer.sgf.editor.property.listener.PropertyChangedSubject;
import javax.swing.SpinnerNumberModel;

public class DimensionPanel extends JPanel implements PropertyChangedSubject {
	private static final long serialVersionUID = -2481446133001569879L;
	private JSpinner valueSpinner;
	private JComboBox relativeSizeBox;
	
	private List<PropertyChangedListener> listeners;
	
	/**
	 * Create the panel.
	 */
	public DimensionPanel(String title) {
		setPreferredSize(new java.awt.Dimension(250, 50));
		setMinimumSize(new java.awt.Dimension(250, 50));
		setMaximumSize(new java.awt.Dimension(250, 50));
		listeners = new ArrayList<PropertyChangedListener>();
		
		setBorder(new TitledBorder(null, title, TitledBorder.LEADING, TitledBorder.TOP, null, null));
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		relativeSizeBox = new JComboBox();
		//relativeSizeBox.setEditable(true);
		relativeSizeBox.addActionListener(new PropertyChangeNotifyListneer());
		relativeSizeBox.setModel(new DefaultComboBoxModel(RelativeSize.values()));
		add(relativeSizeBox);
		
		JLabel label = new JLabel("*");
		add(label);
		
		valueSpinner = new JSpinner();
		valueSpinner.setModel(new SpinnerNumberModel(new Float(0), null, null, new Float(0.01f)));
		valueSpinner.addChangeListener(new PropertyChangeNotifyListneer());
		add(valueSpinner);
		
		setSize(250, 50);
	}
	
	public void setAllowedValues(RelativeSize ... values) {
		relativeSizeBox.setModel(new DefaultComboBoxModel(values));
	}
	
	class EnterPressedListener extends KeyAdapter {
		@Override
		public void keyReleased(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				notifyPropertyChangedListeners();
			}
		}
	}
	
	class PropertyChangeNotifyListneer implements ActionListener, ChangeListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			notifyPropertyChangedListeners();
		}

		@Override
		public void stateChanged(ChangeEvent e) {
			notifyPropertyChangedListeners();
		}
	}
	
	public void setDimension(Dimension d) {
		if (d == null) {
			return;
		}
		
		relativeSizeBox.setSelectedItem(d.getRelativeSize());
		valueSpinner.setValue(d.getMultiplier());
	}
	
	public Dimension getDimension() {
		return Dimension.getDimension((RelativeSize) relativeSizeBox.getSelectedItem(), Float.parseFloat(valueSpinner.getValue().toString()));
	}
	
	@Override
	public void addPropertyChangedListener(PropertyChangedListener l) {
		listeners.add(l);
	}

	@Override
	public void removePropertyChangedListener(PropertyChangedListener l) {
		listeners.remove(l);
	}

	@Override
	public void notifyPropertyChangedListeners() {
		for(PropertyChangedListener l : listeners) {
			l.propertyChanged();
		}
	}
}
