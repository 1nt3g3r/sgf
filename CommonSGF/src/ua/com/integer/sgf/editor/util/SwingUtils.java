package ua.com.integer.sgf.editor.util;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JDialog;

public class SwingUtils {
	public static void showModalInCenter(JDialog dialog) {
		dialog.setModal(true);
		showInCenter(dialog);
	}
	
	public static void showInCenter(JDialog dialog) {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		
		int sWidth = screenSize.width;
		int sHeight = screenSize.height;
		
		int wWidth = dialog.getWidth();
		int wHeight = dialog.getHeight();
		
		int dialogX = (sWidth - wWidth)/2;
		int dialogY = (sHeight - wHeight)/2;
		
		dialog.setLocation(dialogX, dialogY);
		dialog.setVisible(true);
	}
}
