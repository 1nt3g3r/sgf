package ua.com.integer.sgf.editor.property.listener;

public interface PropertyChangedListener {
	public void propertyChanged();
}
