package ua.com.integer.sgf.editor.property.listener;


public interface PropertyChangedSubject {
	public void addPropertyChangedListener(PropertyChangedListener l);
	public void removePropertyChangedListener(PropertyChangedListener l);
	public void notifyPropertyChangedListeners();
}
