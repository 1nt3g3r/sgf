package ua.com.integer.sgf;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import com.badlogic.gdx.Gdx;

public class ImageDownloader {
	private String siteUrl = "http://webprestige.com.ua/smash_stickers/stickers/";
	private String imageCachePath = "img_cache/";
	
	private String currentImageName;
	private boolean finished;
	private String error;
	private int totalLength, downloadedLength;
	
	public void setSiteURL(String siteURL) {
		this.siteUrl = siteURL;
	}
	
	public void setImageCachePath(String imageCachePath) {
		this.imageCachePath = imageCachePath;
	}
	
	public void setImageName(String currentImageName) {
		this.currentImageName = currentImageName;
	}
	
	public boolean isFinished() {
		return finished;
	}
	
	public String error() {
		return error;
	}
	
	class LoadingThread extends Thread {
		String toSaveName = currentImageName.substring(0, currentImageName.length()-4) + ".xyz";
		@Override
		public void run() {
			if (Gdx.files.external(imageCachePath + toSaveName).file().exists()) {
				finished = true;
				error = "ok";
				return;
			}
			URL url;
			InputStream is;
			OutputStream os;
			URLConnection connection;
			try {
				url = new URL(siteUrl + currentImageName);
				connection = url.openConnection();
				totalLength = connection.getContentLength();
				is = new BufferedInputStream(url.openStream());
				os = new FileOutputStream(Gdx.files.external(imageCachePath + toSaveName).file());

				byte[] b = new byte[2048];
				int length;

				while ((length = is.read(b)) != -1) {
					downloadedLength += length;
					os.write(b, 0, length);
				}
				is.close();
				os.flush();
				os.close();
				finished = true;
				error = "ok";
			} catch (Exception e) {
				finished = true;
				error = e + "";
			}
		}
	};
	
	public void startLoading() {
		downloadedLength = 0;
		finished = false;
		error = "error";
		new LoadingThread().start();
	}
	
	public int getDownloadPercent() {
		if (downloadedLength != 0) {
			return 100*downloadedLength/totalLength;
		} else {
			return 0;
		}
	}
}
