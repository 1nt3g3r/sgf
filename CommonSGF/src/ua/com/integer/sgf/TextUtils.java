package ua.com.integer.sgf;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

public class TextUtils {
	public static void setFont(Label label, BitmapFont font) {
		LabelStyle lStyle = new LabelStyle(label.getStyle());
		lStyle.font = font;
		label.setHeight(lStyle.font.getCapHeight());
		label.setStyle(lStyle);
	}
	
	public static void setTextColor(Label label, Color color) {
		LabelStyle lStyle = new LabelStyle(label.getStyle());
		lStyle.fontColor = color;
		label.setStyle(lStyle);
	}
	
	public static void setTextureRegionAsBackground(BitmapFont font, final TextField textField, TextureRegion background, final int addMultiplier) {

		TextFieldStyle style = new TextFieldStyle(textField.getStyle());
		style.font = font;
		style.fontColor = Color.BLACK;
		
		
		style.background = new TextureRegionDrawable(background) {
			@Override
			public float getLeftWidth() {
				float half = textField.getWidth()/2;
				float textSize = textField.getStyle().font.getBounds(textField.getText()).width;
				if (textField.getText().equals("")) {
					textSize = textField.getStyle().font.getBounds(textField.getMessageText()).width;
				}
				return half-textSize/2;
			}
			
			@Override
			public float getBottomHeight() {
				float half = textField.getHeight()/4*addMultiplier;
				float textSize = (textField.getStyle().font.getBounds("a").height + textField.getStyle().font.getBounds("A").height)/2;
				return half-textSize;
			}
			
		};
		
		textField.setStyle(style);
	}
	
	public static String transformText(String text, BitmapFont font) {
		return transformText(text, font, Gdx.graphics.getWidth()*0.65f);
	}
	
	public static String transformText(String text, BitmapFont font, float maxWidth) {
		return transformText2(text, font, maxWidth);
	}
	
	public static String transformText2(String text, BitmapFont font, float maxWidth) {
		return transformText2(text, font, maxWidth, false);
	}
	
	public static String transformText2(String text, BitmapFont font, float maxWidth, boolean needAlign) {
		String[] tmp = text.split(" ");
		Array<Array<String>> lines = new Array<Array<String>>();
		int index = 0;
		while(index < tmp.length-1) {
			StringBuilder txt = new StringBuilder();
			for(int i = index; i < tmp.length; i++) {
				if ((font.getBounds(txt).width + font.getBounds(tmp[i] + " ").width >= maxWidth) || i == tmp.length-1) {
					Array<String> line = new Array<String>();
					for(int j = index; j < i; j++) {
						line.add(tmp[j]);
					}
					if (i == tmp.length-1) {
						txt.append(tmp[tmp.length-1]+" ");
						line.add(tmp[tmp.length-1]);
					}
					lines.add(line);
					index = i;
					
					if (needAlign && i != tmp.length-1) {
						int spaceCount = (int) ((maxWidth - font.getBounds(txt).width)/font.getBounds(" ").width);
						insertSpaces(line, spaceCount);
					}
					break;
				} else {
					txt.append(tmp[i]);
					txt.append(" ");
				}
			}
		}
		
		StringBuffer result = new StringBuffer("");
		for(Array<String> line : lines) {
			StringBuffer tLine = new StringBuffer();
			for(int k = 0; k < line.size; k++) {
				tLine.append(line.get(k));
				if (k < line.size - 1) {
					tLine.append(" ");
				}
			}
			result.append(tLine);
			result.append("\n");
		}
		
		return result.toString();
	}
	
	private static void insertSpaces(Array<String> line, int spaceCount) {
		int wordIndex = 0;
		while(spaceCount > 0) {
			if (wordIndex < line.size - 1) {
				line.set(wordIndex, line.get(wordIndex) + " ");// = line.items[wordIndex] + " ";
			}
			wordIndex++;
			if (wordIndex >= line.size) {
				wordIndex = 0;
			}
			spaceCount--;
		}
	}
	
	/**
	 * Установить ограничение на максимальный размер текста
	 * @param field поле
	 * @param maxCount максимальное количество символов в поле
	 */
	public static void setMaximumInputText(TextField field, int maxCount) {
		class MaximumTextInputListener extends InputListener {
			private TextField textField;
			private int maxCount;
			
			public MaximumTextInputListener(TextField textField, int maxCount) {
				this.textField = textField;
				this.maxCount = maxCount;
			}
			
			@Override
			public boolean keyTyped(InputEvent event, char character) {
				if (textField.getText() == null) {
					return true;
				} else if (textField.getText().length() >= maxCount) {
					textField.setText(textField.getText().substring(0, maxCount));
					textField.setCursorPosition(maxCount);
					return true;
				}
				return true;
			}
		};
		field.addListener(new MaximumTextInputListener(field, maxCount));
	}
}
