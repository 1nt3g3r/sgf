package ua.com.integer.sgf.dimension;

import com.badlogic.gdx.Gdx;

public class Dimension {
	public enum RelativeSize {
		ONE,
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		SCREEN_CENTER_X,
		SCREEN_CENTER_Y;
		
		public float getValue() {
			switch(this) {
			case ONE : return 1;
			case SCREEN_CENTER_X: return Gdx.graphics.getWidth()/2;
			case SCREEN_CENTER_Y: return Gdx.graphics.getHeight()/2;
			case SCREEN_HEIGHT: return Gdx.graphics.getHeight();
			case SCREEN_WIDTH: return Gdx.graphics.getWidth();
			}
			throw new IllegalArgumentException("No value for " + this + " dimension!");
		}
	}

	private float multiplier;
	private RelativeSize relativeSize;
	private String value;
	
	public Dimension(String value) {
		this.value = value;
		relativeSize = getRelativeSize();
		multiplier = getMultiplier();
	}
	
	public Dimension(float value) {
		this.relativeSize = RelativeSize.ONE;
		this.multiplier = value;
		this.value = RelativeSize.ONE + "*" + value;
	}
	
	public Dimension(RelativeSize value) {
		this(value, 1);
	}
	
	public Dimension(RelativeSize value, float multiplier) {
		this.relativeSize = value;
		this.multiplier = multiplier;
		this.value = value + "*" + multiplier;
	}
	
	@Override
	public String toString() {
		return value;
	}
	
	public RelativeSize getRelativeSize() {
		return relativeSize;
	}
	
	public static float getValue(String dimensionValue) {
		if (dimensionValue.matches("^[0-9]+")) {
			return Float.parseFloat(dimensionValue);
		} else {
			String[] parts = dimensionValue.split("\\*");
			float relativeValue = RelativeSize.valueOf(parts[0]).getValue();
			float multiplier = Float.parseFloat(parts[1]);
			
			return relativeValue * multiplier;
		}
	}
	
	public float getValue() {
		return getValue(value);
	}
	
	public float getMultiplier() {
		return multiplier;
	}
	
	public static Dimension getDimension(float value) {
		return new Dimension(value);
	}
	
	public static Dimension getDimension(RelativeSize size, float multiplier) {
		return new Dimension(size, multiplier);
	}
}
